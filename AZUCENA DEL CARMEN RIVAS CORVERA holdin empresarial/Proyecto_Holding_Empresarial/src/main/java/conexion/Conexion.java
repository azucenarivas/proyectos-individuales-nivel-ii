package conexion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author azucena.rivasusam
 */
public class Conexion {

    static String bd = "holding_empresarial";
    static String user = "root";
    static String pass = "root";
    static String url = "jdbc:mysql://localhost/" + bd + "?useSSL=false";

    Connection conn = null;

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);

            if (conn != null) {
                System.out.println("Conectado correctamente");

            }

        } catch (Exception e) {
            System.out.println("Error de Conexion" + e);
        }

    }

    public Connection conectar() {
        return conn;

    }

    public void Desconectar() throws Exception {
        conn.close();

    }
}
