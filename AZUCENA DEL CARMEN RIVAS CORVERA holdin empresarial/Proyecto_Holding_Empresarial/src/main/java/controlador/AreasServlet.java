/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.AreasDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AreasBean;

@WebServlet(name = "AreasServlet", urlPatterns = {"/AreasServlet"})
public class AreasServlet extends HttpServlet {

    Conexion conn = new Conexion();
    AreasDao areasdao = new AreasDao(conn);
    AreasBean areasb = new AreasBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("area")) {
            ruta = "/Vistas/vista_areas/mostrar.jsp";
            try {
                request.setAttribute("area", areasdao.consultar_areas());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_areas/insertar.jsp";

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/vista_areas/mostrar.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                areasdao.eliminar(idEliminar);
                request.setAttribute("area", areasdao.consultar_areas());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/vista_areas/actualizar.jsp";
            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("area", areasdao.consultarById_areas(idActual));

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        areasb.setNombre_area(request.getParameter("txtnombre"));
        areasb.setDescripcion_area(request.getParameter("txtdescripcion"));

        if (ID == null || ID.isEmpty()) {
            try {
                areasdao.insertar(areasb);
                ruta = "/Vistas/vista_areas/mostrar.jsp";
                request.setAttribute("area", areasdao.consultar_areas());
            } catch (Exception e) {
            }
        } else {
            try {

                areasb.setCodigo_areas(Integer.parseInt(ID));
                areasdao.actualizar(areasb);

                ruta = "/Vistas/vista_areas/mostrar.jsp";
                request.setAttribute("area", areasdao.consultar_areas());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }
}
