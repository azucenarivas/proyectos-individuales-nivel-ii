/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.AreasDao;
import dao.Areas_cubiertasDao;
import dao.EmpresaDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AreasBean;
import modelo.Areas_cubiertasBean;
import modelo.EmpresaBean;

/**
 *
 * @author Azucena
 */
@WebServlet(name = "Areas_cubiertasServlet", urlPatterns = {"/Areas_cubiertasServlet"})
public class Areas_cubiertasServlet extends HttpServlet {

    Conexion conn = new Conexion();

    Areas_cubiertasDao areas_cubiertasdao = new Areas_cubiertasDao(conn);
    EmpresaDao empresadao = new EmpresaDao(conn);
    AreasDao areasdao = new AreasDao(conn);

    Areas_cubiertasBean areas_cubib = new Areas_cubiertasBean(0);
    EmpresaBean empresab = new EmpresaBean(0);
    AreasBean areasb = new AreasBean(0);

    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("area_cubiertas")) {
            ruta = "/Vistas/vista_areas_cubiertas/mostrar.jsp";

            try {
                request.setAttribute("areas_cubiertas", areas_cubiertasdao.Consultar_Areas_cubiertas());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_areas_cubiertas/insertar.jsp";

            try {

                request.setAttribute("empresa", empresadao.consultar_empresa());
                request.setAttribute("areas", areasdao.consultar_areas());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/vista_areas_cubiertas/mostrar.jsp";

            try {

                int idEliminar = Integer.parseInt(request.getParameter("id"));
                areas_cubiertasdao.eliminar_ares_cubiertas(idEliminar);
                request.setAttribute("areas_cubiertas", areas_cubiertasdao.Consultar_Areas_cubiertas());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/vista_areas_cubiertas/actualizar.jsp";

            try {

                int IdActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("areas_cubiertas", areas_cubiertasdao.consultarById_areaCubiertas(IdActual));
                request.setAttribute("empresa", empresadao.consultar_empresa());
                request.setAttribute("areas", areasdao.consultar_areas());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("lista_areas")) {
            ruta = "/Vistas/vista_areas_cubiertas/lista_areas.jsp";

            try {
                request.setAttribute("areas_cubiertas", areas_cubiertasdao.Ver_todas_Areas_cubiertas());
            } catch (Exception e) {
            }

        }else if (accion.equalsIgnoreCase("lista")) {
            ruta = "/Vistas/vista_areas_cubiertas/lista.jsp";

            try {
                request.setAttribute("areas_cubiertas", areas_cubiertasdao.Ver_Areas_cubiertas());
            } catch (Exception e) {
            }

        }


        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        empresab.setCodigo_empresa(Integer.parseInt(request.getParameter("cmbempresa")));
        areas_cubib.setEmpresa(empresab);

        areasb.setCodigo_areas(Integer.parseInt(request.getParameter("cmbarea")));
        areas_cubib.setAreas(areasb);

        if (ID == null || ID.isEmpty()) {
            try {
                areas_cubiertasdao.insertarAreas_cubiertas(areas_cubib);

                if (respuesta) {
                    msg = "registro guardado";
                } else {
                    msg = "registro no guardado";
                }
                ruta = "/Vistas/vista_areas_cubiertas/mostrar.jsp";
                request.setAttribute("areas_cubiertas", areas_cubiertasdao.Consultar_Areas_cubiertas());
            } catch (Exception e) {
            }
        } else {
            try {

                areas_cubib.setCodigo_areas_cubiertas(Integer.parseInt(ID));
                areas_cubiertasdao.actualizar_Areas_cubiertas(areas_cubib);

                if (respuesta) {
                    msg = "registro actualizado";
                } else {
                    msg = "registro no actualizado";
                }
                ruta = "/Vistas/vista_areas_cubiertas/mostrar.jsp";
                request.setAttribute("areas_cubiertas", areas_cubiertasdao.Consultar_Areas_cubiertas());

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }
}
