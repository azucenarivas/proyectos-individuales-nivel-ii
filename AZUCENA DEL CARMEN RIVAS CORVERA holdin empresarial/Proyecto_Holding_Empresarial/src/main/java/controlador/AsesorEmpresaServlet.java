/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.AsesorDao;
import dao.Asesor_empresaDao;
import dao.EmpresaDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AsesorBean;
import modelo.Asesor_empresaBean;
import modelo.EmpresaBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "AsesorEmpresaServlet", urlPatterns = {"/AsesorEmpresaServlet"})
public class AsesorEmpresaServlet extends HttpServlet {

    Conexion conn = new Conexion();

    Asesor_empresaDao asesorempresadao = new Asesor_empresaDao(conn);
    EmpresaDao empresadao = new EmpresaDao(conn);
    AsesorDao asesordao = new AsesorDao(conn);

    Asesor_empresaBean asesor_empresab = new Asesor_empresaBean(0);
    EmpresaBean empresab = new EmpresaBean(0);
    AsesorBean asesorb = new AsesorBean(0);

    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("asesorEmpresa")) {
            ruta = "/Vistas/vista_asesorEmpresa/mostrar.jsp";

            try {
                request.setAttribute("asesorempresa", asesorempresadao.ConsultarAsesorEmpresa());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_asesorEmpresa/insertar.jsp";
            try {

                request.setAttribute("empresa", empresadao.consultar_empresa());
                request.setAttribute("asesor", asesordao.consultar_asesor());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/vista_asesorEmpresa/actualizar.jsp";
            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("asesorempresa", asesorempresadao.consultarById_Asesor_empresa(idActual));
                request.setAttribute("empresa", empresadao.consultar_empresa());
                request.setAttribute("asesor", asesordao.consultar_asesor());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/vista_asesorEmpresa/mostrar.jsp";

            try {
                int IdEliminar = Integer.parseInt(request.getParameter("id"));
                asesorempresadao.eliminar(IdEliminar);
                request.setAttribute("asesorempresa", asesorempresadao.ConsultarAsesorEmpresa());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        empresab.setCodigo_empresa(Integer.parseInt(request.getParameter("cmbempresa")));
        asesor_empresab.setEmpresa(empresab);

        asesorb.setCodigo_asesor(Integer.parseInt(request.getParameter("cmbasesor")));
        asesor_empresab.setAsesor(asesorb);

        if (ID == null || ID.isEmpty()) {
            try {
                asesorempresadao.insertar(asesor_empresab);

                ruta = "/Vistas/vista_asesorEmpresa/mostrar.jsp";
                request.setAttribute("asesorempresa", asesorempresadao.ConsultarAsesorEmpresa());
            } catch (Exception e) {
            }
        } else {
            try {

                asesor_empresab.setCodigo_asesor_empresa(Integer.parseInt(ID));
                asesorempresadao.actualizar(asesor_empresab);

                if (respuesta) {
                    msg = "registro actualizado";
                } else {
                    msg = "registro no actualizado";
                }
                ruta = "/Vistas/vista_asesorEmpresa/mostrar.jsp";
                request.setAttribute("asesorempresa", asesorempresadao.ConsultarAsesorEmpresa());

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
