/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.AsesorDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AsesorBean;

/**
 *
 * @author Azucena
 */
@WebServlet(name = "AsesorServlet", urlPatterns = {"/AsesorServlet"})
public class AsesorServlet extends HttpServlet {

    Conexion conn = new Conexion();
    AsesorDao asesordao = new AsesorDao(conn);
    AsesorBean asesorb = new AsesorBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("asesor")) {
            ruta = "/Vistas/vista_asesor/mostrar.jsp";

            try {
                request.setAttribute("asesor", asesordao.consultar_asesor());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_asesor/insertar.jsp";;

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/vista_asesor/mostrar.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                asesordao.eliminar(idEliminar);
                request.setAttribute("asesor", asesordao.consultar_asesor());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/vista_asesor/actualizar.jsp";
            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("asesor", asesordao.consultarById_asesor(idActual));

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        asesorb.setNombre_a(request.getParameter("txtnombre"));
        asesorb.setDireccion_a(request.getParameter("txtdireccion"));
        asesorb.setTitulacion(request.getParameter("txttitulacion"));

        if (ID == null || ID.isEmpty()) {
            try {
                asesordao.insertar(asesorb);
                if (respuesta) {
                    msg = "registro guardado";
                } else {
                    msg = "registro no guardado";
                }
                ruta = "/Vistas/vista_asesor/mostrar.jsp";
                request.setAttribute("asesor", asesordao.consultar_asesor());
            } catch (Exception e) {
            }
        } else {
            try {

                asesorb.setCodigo_asesor(Integer.parseInt(ID));
                asesordao.actualizar(asesorb);
                if (respuesta) {
                    msg = "registro actualizado";
                } else {
                    msg = "registro no actualizado";
                }
                ruta = "/Vistas/vista_asesor/mostrar.jsp";
                request.setAttribute("asesor", asesordao.consultar_asesor());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }
}
