/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.AreasDao;
import dao.AsesorDao;
import dao.Cobertura_asesorDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AreasBean;
import modelo.AsesorBean;
import modelo.Cobertura_asesorBean;

/**
 *
 * @author Azucena
 */
@WebServlet(name = "Cobertura_asesorServlet", urlPatterns = {"/Cobertura_asesorServlet"})
public class Cobertura_asesorServlet extends HttpServlet {

    Conexion conn = new Conexion();

    Cobertura_asesorDao cober_asesordao = new Cobertura_asesorDao(conn);
    AreasDao areadao = new AreasDao(conn);
    AsesorDao asesordao = new AsesorDao(conn);

    Cobertura_asesorBean cobertirab = new Cobertura_asesorBean(0);
    AreasBean areasb = new AreasBean(0);
    AsesorBean asesorb = new AsesorBean(0);

    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("cobertura_asesor")) {
            ruta = "/Vistas/vista_cobertura_asesor/mostrar.jsp";

            try {
                request.setAttribute("cobertura_asesor", cober_asesordao.ConsultarCobertura_asesor());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_cobertura_asesor/insertar.jsp";

            try {

                request.setAttribute("areas", areadao.consultar_areas());
                request.setAttribute("asesor", asesordao.consultar_asesor());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/vista_cobertura_asesor/mostrar.jsp";

            try {

                int idEliminar = Integer.parseInt(request.getParameter("id"));
                cober_asesordao.eliminar(idEliminar);

                request.setAttribute("cobertura_asesor", cober_asesordao.ConsultarCobertura_asesor());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/vista_cobertura_asesor/actualizar.jsp";

            try {

                int IdActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("cobertura_asesor", cober_asesordao.consultarById_covertura_asesor(IdActual));
                request.setAttribute("areas", areadao.consultar_areas());
                request.setAttribute("asesor", asesordao.consultar_asesor());

            } catch (Exception e) {
            }

        }else if(accion.equalsIgnoreCase("listado_cobertura")){
            ruta = "/Vistas/vista_cobertura_asesor/lista_cobertura.jsp";
            
            try {
                 request.setAttribute("cobertura_asesor", cober_asesordao.VerCobertura_asesor());
            } catch (Exception e) {
            }
            
            
        
        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        areasb.setCodigo_areas(Integer.parseInt(request.getParameter("cmbarea")));
        cobertirab.setAreas(areasb);
        asesorb.setCodigo_asesor(Integer.parseInt(request.getParameter("cmbasesor")));
        cobertirab.setAsesor(asesorb);

        if (ID == null || ID.isEmpty()) {
            try {
                cober_asesordao.insertar(cobertirab);

                if (respuesta) {
                    msg = "registro guardado";
                } else {
                    msg = "registro no guardado";
                }
                ruta = "/Vistas/vista_cobertura_asesor/mostrar.jsp";
                request.setAttribute("cobertura_asesor", cober_asesordao.ConsultarCobertura_asesor());
            } catch (Exception e) {
            }
        } else {
            try {

                cobertirab.setCodigo_conbertura_asesor(Integer.parseInt(ID));
                cober_asesordao.actualizar(cobertirab);

                if (respuesta) {
                    msg = "registro actualizado";
                } else {
                    msg = "registro no actualizado";
                }
                ruta = "/Vistas/vista_cobertura_asesor/mostrar.jsp";
                request.setAttribute("cobertura_asesor", cober_asesordao.ConsultarCobertura_asesor());

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }
}
