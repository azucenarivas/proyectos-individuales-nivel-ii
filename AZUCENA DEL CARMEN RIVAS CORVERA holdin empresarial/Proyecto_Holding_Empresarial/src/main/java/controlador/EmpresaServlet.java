/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.EmpresaDao;
import dao.PaisDao;
import dao.SedeDao;
import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.EmpresaBean;
import modelo.PaisBean;
import modelo.SedeBean;

/**
 *
 * @author Azucena
 */
@WebServlet(name = "EmpresaServlet", urlPatterns = {"/EmpresaServlet"})
public class EmpresaServlet extends HttpServlet {

    Conexion conn = new Conexion();

    EmpresaDao empresadao = new EmpresaDao(conn);
    SedeDao sededao = new SedeDao(conn);
    PaisDao paisdao = new PaisDao(conn);

    EmpresaBean empresab = new EmpresaBean(0);
    PaisBean paisb = new PaisBean(0);
    SedeBean sedeb = new SedeBean(0);

    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("empresa")) {
            ruta = "/Vistas/vista_empresa/mostrar.jsp";
            try {
                request.setAttribute("empresas", empresadao.consultar_empresa());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_empresa/insertar.jsp";
            try {
                request.setAttribute("sede", sededao.consultar_sede());
                request.setAttribute("pais", paisdao.consultar_pais());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/vista_empresa/mostrar.jsp";
            try {
                int codigoelimar = Integer.parseInt(request.getParameter("id"));
                empresadao.eliminar(codigoelimar);
                request.setAttribute("empresas", empresadao.consultar_empresa());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("actualizar")) {
            ruta = "/Vistas/vista_empresa/actualizar.jsp";
            try {
                int idactual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("empresas", empresadao.consultarById_empresa(idactual));
                request.setAttribute("sede", sededao.consultar_sede());
                request.setAttribute("pais", paisdao.consultar_pais());

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        empresab.setNombre_e(request.getParameter("txtnombre"));
        //  empresab.setFecha_entrada_holdind(request.getParameter("txtfecha"));
        Date fentrada_holding = Date.valueOf(request.getParameter("txtfecha"));
        empresab.setFecha_entrada_holdind(fentrada_holding);
        empresab.setFacturacion_anual(request.getParameter("txtfacturacion"));
        empresab.setNumero_vendedores(Integer.parseInt(request.getParameter("txtvendedores")));
        sedeb.setCodigo_sede(Integer.parseInt(request.getParameter("cmbsede")));
        empresab.setSede(sedeb);
        paisb.setCodigo_pais(Integer.parseInt(request.getParameter("cmbpais")));
        empresab.setPais(paisb);

        if (ID == null || ID.isEmpty()) {
            try {
                empresadao.insertar(empresab);
                if (respuesta) {
                    msg = "registro guardado";
                } else {
                    msg = "registro no guardado";
                }
                ruta = "/Vistas/vista_empresa/mostrar.jsp";
                request.setAttribute("empresas", empresadao.consultar_empresa());
            } catch (Exception e) {
            }
        } else {
            try {

                empresab.setCodigo_empresa(Integer.parseInt(ID));
                empresadao.actualizar(empresab);
                if (respuesta) {
                    msg = "registro actualizado";
                } else {
                    msg = "registro no actualizado";
                }
                ruta = "/Vistas/vista_empresa/mostrar.jsp";
                request.setAttribute("empresas", empresadao.consultar_empresa());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }
}
