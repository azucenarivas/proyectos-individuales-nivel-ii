/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.PaisDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.PaisBean;

/**
 *
 * @author Azucena
 */
@WebServlet(name = "PaisServlet", urlPatterns = {"/PaisServlet"})
public class PaisServlet extends HttpServlet {

    Conexion conn = new Conexion();
    PaisDao paisdao = new PaisDao(conn);

    PaisBean paisb = new PaisBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("pais")) {
            ruta = "/Vistas/vista_pais/mostrar.jsp";

            try {
                request.setAttribute("pais", paisdao.consultar_pais());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_pais/insertar.jsp";

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/vista_pais/mostrar.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                paisdao.eliminar(idEliminar);
                request.setAttribute("pais", paisdao.consultar_pais());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/vista_pais/actualizar.jsp";
            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("pais", paisdao.consultarById_pais(idActual));

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        paisb.setNombre_p(request.getParameter("txtnombre_p"));
        paisb.setPIB(Double.parseDouble(request.getParameter("txtPIB")));
        paisb.setNumero_habitantes(Integer.parseInt(request.getParameter("txthabitantes")));
        paisb.setCapital(request.getParameter("txtcapital"));

        if (ID == null || ID.isEmpty()) {
            try {
                paisdao.insertar(paisb);
                if (respuesta) {
                    msg = "registro guardado";
                } else {
                    msg = "registro no guardado";
                }
                ruta = "/Vistas/vista_pais/mostrar.jsp";
                request.setAttribute("pais", paisdao.consultar_pais());
            } catch (Exception e) {
            }
        } else {
            try {

                paisb.setCodigo_pais(Integer.parseInt(ID));
                paisdao.actualizar(paisb);
                if (respuesta) {
                    msg = "registro actualizado";
                } else {
                    msg = "registro no actualizado";
                }
                ruta = "/Vistas/vista_pais/mostrar.jsp";
                request.setAttribute("pais", paisdao.consultar_pais());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }



}
