/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.EmpresaDao;
import dao.PaisDao;
import dao.SedeDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.EmpresaBean;
import modelo.PaisBean;
import modelo.SedeBean;

/**
 *
 * @author Azucena
 */
@WebServlet(name = "SedeServlet", urlPatterns = {"/SedeServlet"})
public class SedeServlet extends HttpServlet {

    Conexion conn = new Conexion();
    SedeDao sededao = new SedeDao(conn);


    SedeBean sedeb = new SedeBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("sede")) {
            ruta = "/Vistas/vista_sede/mostrar.jsp";

            try {
                request.setAttribute("sede", sededao.consultar_sede());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_sede/insertar.jsp";
           

        }else if(accion.equalsIgnoreCase("editar")){
             ruta = "/Vistas/vista_sede/actualizar.jsp";
             try {
                 int idActual=Integer.parseInt(request.getParameter("id"));
                   request.setAttribute("sede", sededao.consultarById_sedes(idActual));
            } catch (Exception e) {
            }
        
        }else if(accion.equalsIgnoreCase("eliminar")){
            ruta = "/Vistas/vista_sede/mostrar.jsp";
            try {
                int idEliminar=Integer.parseInt(request.getParameter("id"));
                sededao.eliminar(idEliminar);
                request.setAttribute("sede", sededao.consultar_sede());
            } catch (Exception e) {
            }
            
        
        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String ID = request.getParameter("txtID");

        sedeb.setCiudad(request.getParameter("txtciudad"));

        if (ID == null || ID.isEmpty()) {
            try {
                sededao.insertar(sedeb);

                if (respuesta) {
                    msg = "registro guardado";
                } else {
                    msg = "registro no guardado";
                }
                ruta = "/Vistas/vista_sede/mostrar.jsp";
                request.setAttribute("sede", sededao.consultar_sede());
            } catch (Exception e) {
            }
        } else {
            try {

                sedeb.setCodigo_sede(Integer.parseInt(ID));
                sededao.actualizar(sedeb);

                if (respuesta) {
                    msg = "registro actualizado";
                } else {
                    msg = "registro no actualizado";
                }
                ruta = "/Vistas/vista_sede/mostrar.jsp";
                request.setAttribute("sede", sededao.consultar_sede());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

}
