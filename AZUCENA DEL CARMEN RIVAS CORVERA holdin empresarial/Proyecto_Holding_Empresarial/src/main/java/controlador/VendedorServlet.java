/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.VendedorDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.VendedorBean;

/**
 *
 * @author Azucena
 */
@WebServlet(name = "VendedorServlet", urlPatterns = {"/VendedorServlet"})
public class VendedorServlet extends HttpServlet {

    Conexion conn = new Conexion();
    VendedorDao vendedordao = new VendedorDao(conn);

    VendedorBean vendedorb = new VendedorBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("vendedor")) {
            ruta = "/Vistas/vista_vendedor/mostrar.jsp";

            try {
                request.setAttribute("vendedor", vendedordao.consultar_vendedor());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_vendedor/insertar.jsp";

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/vista_vendedor/mostrar.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                vendedordao.eliminar(idEliminar);
                request.setAttribute("vendedor", vendedordao.consultar_vendedor());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/vista_vendedor/actualizar.jsp";
            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("vendedor", vendedordao.consultarById_vendedor(idActual));

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        vendedorb.setNombre_v(request.getParameter("txtnombre"));
        vendedorb.setDireccion_v(request.getParameter("txtdireccion"));

        if (ID == null || ID.isEmpty()) {
            try {
                vendedordao.insertar(vendedorb);
                if (respuesta) {
                    msg = "registro guardado";
                } else {
                    msg = "registro no guardado";
                }
                ruta = "/Vistas/vista_vendedor/mostrar.jsp";
                request.setAttribute("vendedor", vendedordao.consultar_vendedor());
            } catch (Exception e) {
            }
        } else {
            try {

                vendedorb.setCodigo_vendedor(Integer.parseInt(ID));
                vendedordao.actualizar(vendedorb);
                if (respuesta) {
                    msg = "registro actualizado";
                } else {
                    msg = "registro no actualizado";
                }
                ruta = "/Vistas/vista_vendedor/mostrar.jsp";
                request.setAttribute("vendedor", vendedordao.consultar_vendedor());

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
