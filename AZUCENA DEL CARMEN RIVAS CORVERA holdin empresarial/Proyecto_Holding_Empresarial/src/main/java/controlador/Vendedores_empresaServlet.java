/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.EmpresaDao;
import dao.VendedorDao;
import dao.Vendedores_EmpresaDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.EmpresaBean;
import modelo.VendedorBean;
import modelo.Vendedores_empresaBean;

/**
 *
 * @author Azucena
 */
@WebServlet(name = "Vendedores_empresaServlet", urlPatterns = {"/Vendedores_empresaServlet"})
public class Vendedores_empresaServlet extends HttpServlet {

    Conexion conn = new Conexion();

    Vendedores_EmpresaDao vendedoresdao = new Vendedores_EmpresaDao(conn);
    VendedorDao vendedordao = new VendedorDao(conn);
    EmpresaDao empresadao = new EmpresaDao(conn);

    Vendedores_empresaBean vendedoresb = new Vendedores_empresaBean(0);
    VendedorBean vendedorb = new VendedorBean(0);
    EmpresaBean empresab = new EmpresaBean(0);

    String msg;
    boolean respuesta;
    SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("vendedor_empresa")) {
            ruta = "/Vistas/vista_vendedor_empresa/mostrar.jsp";

            try {
                request.setAttribute("vendedores", vendedoresdao.consultar_Vendedores_empresa());

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/vista_vendedor_empresa/insertar.jsp";
            try {
                request.setAttribute("vendedor", vendedordao.consultar_vendedor());
                request.setAttribute("empresas", empresadao.consultar_empresa());
            } catch (Exception e) {
            }
        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/vista_vendedor_empresa/actualizar.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("vendedores", vendedoresdao.consultarById_vendedores_empresa(idActual));
                request.setAttribute("vendedor", vendedordao.consultar_vendedor());
                request.setAttribute("empresas", empresadao.consultar_empresa());
            } catch (Exception e) {
            }

        }else if(accion.equalsIgnoreCase("eliminar")){
        ruta = "/Vistas/vista_vendedor_empresa/mostrar.jsp";
            try {
                int idEliminar=Integer.parseInt(request.getParameter("id"));
                vendedoresdao.eliminar(idEliminar);
                 request.setAttribute("vendedores", vendedoresdao.consultar_Vendedores_empresa());
                
            } catch (Exception e) {
            }
        
        }else if (accion.equalsIgnoreCase("ver_vendedores")) {
            ruta = "/Vistas/vista_vendedor_empresa/lista_vendedores.jsp";
            try {
                request.setAttribute("vendedores", vendedoresdao.Ver_Vendedores_empresa());
            } catch (Exception e) {
            }

            

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
//        Date fecha = format.parse(request.getParameter("txtfecha"));

        String ID = request.getParameter("txtID");
        
        vendedoresb.setFecha_contratacion(request.getParameter("txtfecha"));
        vendedorb.setCodigo_vendedor(Integer.parseInt(request.getParameter("cmbvendedor")));
        vendedoresb.setVendedor(vendedorb);
        empresab.setCodigo_empresa(Integer.parseInt(request.getParameter("cmbempresa")));
        vendedoresb.setEmpresa(empresab);

        if (ID == null || ID.isEmpty()) {
            try {
                vendedoresdao.insertar(vendedoresb);

                if (respuesta) {
                    msg = "registro guardado";
                } else {
                    msg = "registro no guardado";
                }
                ruta = "/Vistas/vista_vendedor_empresa/mostrar.jsp";
                request.setAttribute("vendedores", vendedoresdao.consultar_Vendedores_empresa());
            } catch (Exception e) {
            }
        } else {
            try {

                vendedoresb.setCodigo_vendedor_empresa(Integer.parseInt(ID));
                vendedoresdao.actualizar(vendedoresb);
                if (respuesta) {
                    msg = "registro actualizado";
                } else {
                    msg = "registro no actualizado";
                }
                ruta = "/Vistas/vista_vendedor_empresa/mostrar.jsp";
                request.setAttribute("vendedores", vendedoresdao.consultar_Vendedores_empresa());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }
}
