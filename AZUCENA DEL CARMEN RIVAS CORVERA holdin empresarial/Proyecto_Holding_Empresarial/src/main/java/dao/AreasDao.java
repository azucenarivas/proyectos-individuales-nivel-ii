/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.AreasBean;

/**
 *
 * @author azucena.rivasusam
 */
public class AreasDao {

    Conexion conn;

    public AreasDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(AreasBean areab) {

        String sql = "insert into areas(nombre_area,descripcion_area)values(?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            //ps.setInt(1, areab.getCodigo_areas());
            ps.setString(1, areab.getNombre_area());
            ps.setString(2, areab.getDescripcion_area());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println("insertar "+e);
            return false;
        }

    }

    public List<AreasBean> consultar_areas() {

        String sql = "select *from areas ";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            System.out.println(ps);
            AreasBean areab;
            ResultSet rs = ps.executeQuery();
            List<AreasBean> lista = new LinkedList<>();
            while (rs.next()) {
                areab = new AreasBean(rs.getInt("codigo_areas"));
                areab.setNombre_area(rs.getString("nombre_area"));
                areab.setDescripcion_area(rs.getString("descripcion_area"));
                lista.add(areab);

            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<AreasBean> consultarById_areas(int codigo) {
        String sql = "select *from areas where codigo_areas=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            AreasBean areab;
            List<AreasBean> lista = new LinkedList<>();

            while (rs.next()) {
                areab = new AreasBean(rs.getInt("codigo_areas"));
                areab.setNombre_area(rs.getString("nombre_area"));
                areab.setDescripcion_area(rs.getString("descripcion_area"));
                lista.add(areab);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean actualizar(AreasBean areab) {

        String sql = "update areas set nombre_area=?,descripcion_area=? where codigo_areas=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, areab.getNombre_area());
            ps.setString(2, areab.getDescripcion_area());
            ps.setInt(3, areab.getCodigo_areas());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public boolean eliminar(int codigo_areas) {

        String sql = "delete from areas where codigo_areas=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_areas);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

}
