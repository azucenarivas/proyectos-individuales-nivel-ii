/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.AreasBean;
import modelo.Areas_cubiertasBean;
import modelo.EmpresaBean;

/**
 *
 * @author azucena.rivasusam
 */
public class Areas_cubiertasDao {

    Conexion conn;

    public Areas_cubiertasDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertarAreas_cubiertas(Areas_cubiertasBean areab) {

        String sql = "insert into areas_cubiertas(codigo_empresa,codigo_areas)values(?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            //ps.setInt(1,areab.getCodigo_areas_cubiertas());
            ps.setInt(1, areab.getEmpresa().getCodigo_empresa());
            ps.setInt(2, areab.getAreas().getCodigo_areas());
            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public List<Areas_cubiertasBean> consultarById_areaCubiertas(int codigo) {
        String sql = "select *from areas_cubiertas where codigo_areas_cubiertas=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            Areas_cubiertasBean areascubiertasb;
            List<Areas_cubiertasBean> lista = new LinkedList<>();

            while (rs.next()) {
                areascubiertasb = new Areas_cubiertasBean(rs.getInt("codigo_areas_cubiertas"));
                areascubiertasb.setCodigo_empresa(rs.getInt("codigo_empresa"));
                areascubiertasb.setCodigo_areas(rs.getInt("codigo_areas"));
                lista.add(areascubiertasb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean actualizar_Areas_cubiertas(Areas_cubiertasBean areab) {

        String sql = "update areas_cubiertas set codigo_empresa=?,codigo_areas=? where codigo_areas_cubiertas=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, areab.getEmpresa().getCodigo_empresa());
            ps.setInt(2, areab.getAreas().getCodigo_areas());
            ps.setInt(3, areab.getCodigo_areas_cubiertas());
            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public List<Areas_cubiertasBean> Consultar_Areas_cubiertas() {

        String sql = "select a.codigo_areas_cubiertas,e.nombre_e,c.nombre_area\n"
                + "from areas_cubiertas a\n"
                + "inner join empresa e on e.codigo_empresa=a.codigo_empresa\n"
                + "inner join areas c on c.codigo_areas=a.codigo_areas";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Areas_cubiertasBean area_cubiertasb;
            ResultSet rs = ps.executeQuery();
            List<Areas_cubiertasBean> lista = new LinkedList<>();
            while (rs.next()) {
                area_cubiertasb = new Areas_cubiertasBean(rs.getInt("codigo_areas_cubiertas"));
                area_cubiertasb.setNombre_e(rs.getString("nombre_e"));
                area_cubiertasb.setNombre_area(rs.getString("nombre_area"));
                lista.add(area_cubiertasb);

            }
            return lista;

        } catch (Exception e) {
            return null;
        }
    }

    public List<Areas_cubiertasBean> Ver_todas_Areas_cubiertas() {

        String sql = "select ar.codigo_areas_cubiertas,e.nombre_e,p.nombre_p,s.ciudad,are.nombre_area,are.descripcion_area,e.numero_vendedores,e.fecha_entrada_holdind\n"
                + "from areas_cubiertas ar\n"
                + "inner join empresa e on e.codigo_empresa=ar.codigo_empresa\n"
                + "inner join pais p on p.codigo_pais=e.codigo_pais\n"
                + "inner join sede s on s.codigo_sede=e.codigo_sede\n"
                + "inner join areas are on are.codigo_areas=ar.codigo_areas";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Areas_cubiertasBean area_cubiertas;
            ResultSet rs = ps.executeQuery();
            List<Areas_cubiertasBean> lista = new LinkedList<>();
            while (rs.next()) {
                area_cubiertas = new Areas_cubiertasBean(rs.getInt("codigo_areas_cubiertas"));
                area_cubiertas.setNombre_e(rs.getString("nombre_e"));
                area_cubiertas.setNombre_p(rs.getString("nombre_p"));
                area_cubiertas.setCiudad(rs.getString("ciudad"));
                area_cubiertas.setNombre_area(rs.getString("nombre_area"));
                area_cubiertas.setDescripcion_area(rs.getString("descripcion_area"));
                area_cubiertas.setNumero_vendedores(rs.getInt("numero_vendedores"));
                area_cubiertas.setFecha_entrada_holdind(rs.getDate("fecha_entrada_holdind"));
                lista.add(area_cubiertas);

            }
            return lista;

        } catch (Exception e) {
            return null;
        }
    }

    public List<Areas_cubiertasBean> Ver_Areas_cubiertas() {

        String sql = "select ac.codigo_areas_cubiertas,e.nombre_e,a.nombre_area,a.descripcion_area\n"
                + "from areas_cubiertas ac\n"
                + "inner join empresa e on e.codigo_empresa=ac.codigo_empresa\n"
                + "inner join areas a on a.codigo_areas=ac.codigo_areas";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Areas_cubiertasBean area_cubiertas;
            ResultSet rs = ps.executeQuery();
            List<Areas_cubiertasBean> lista = new LinkedList<>();
            while (rs.next()) {
                area_cubiertas = new Areas_cubiertasBean(rs.getInt("codigo_areas_cubiertas"));
                area_cubiertas.setNombre_e(rs.getString("nombre_e"));
                area_cubiertas.setNombre_area(rs.getString("nombre_area"));
                area_cubiertas.setDescripcion_area(rs.getString("descripcion_area"));
                lista.add(area_cubiertas);

            }
            return lista;

        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar_ares_cubiertas(int codigo_areas_cubiertas) {

        String sql = "delete from areas_cubiertas where codigo_areas_cubiertas=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_areas_cubiertas);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

}
