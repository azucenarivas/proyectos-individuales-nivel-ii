/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.AsesorBean;

/**
 *
 * @author azucena.rivasusam
 */
public class AsesorDao {

    Conexion conn;

    public AsesorDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(AsesorBean asesob) {

        String sql = "insert into asesor(nombre_a,direccion_a,titulacion)values(?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            //ps.setInt(1, asesob.getCodigo_asesor());
            ps.setString(1, asesob.getNombre_a());
            ps.setString(2, asesob.getDireccion_a());
            ps.setString(3, asesob.getTitulacion());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;

        }

    }

    
     public List<AsesorBean> consultarById_asesor(int codigo) {
        String sql = "select *from asesor where codigo_asesor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            AsesorBean asesorb;
            List<AsesorBean> lista = new LinkedList<>();

            while (rs.next()) {
                asesorb = new AsesorBean(rs.getInt("codigo_asesor"));
                asesorb.setNombre_a(rs.getString("nombre_a"));
                asesorb.setDireccion_a(rs.getString("direccion_a"));
                asesorb.setTitulacion(rs.getString("titulacion"));
                lista.add(asesorb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }
    public boolean actualizar(AsesorBean asesob) {

        String sql = "update asesor set nombre_a=?,direccion_a=?,titulacion=? where codigo_asesor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, asesob.getNombre_a());
            ps.setString(2, asesob.getDireccion_a());
            ps.setString(3, asesob.getTitulacion());
            ps.setInt(4, asesob.getCodigo_asesor());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;

        }

    }

    public List<AsesorBean> consultar_asesor() {

        String sql = "select *from asesor";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            AsesorBean aseb;
            ResultSet rt = ps.executeQuery();
            List<AsesorBean> lista = new LinkedList<>();
            while (rt.next()) {
                aseb = new AsesorBean(rt.getInt("codigo_asesor"));
                aseb.setNombre_a(rt.getString("nombre_a"));
                aseb.setDireccion_a(rt.getString("direccion_a"));
                aseb.setTitulacion(rt.getString("titulacion"));
                lista.add(aseb);

            }

            return lista;

        } catch (Exception e) {
            return null;
        }

    }

    public boolean eliminar(int codigo_asesor) {

        String sql = "delete from asesor  where codigo_asesor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_asesor);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

}
