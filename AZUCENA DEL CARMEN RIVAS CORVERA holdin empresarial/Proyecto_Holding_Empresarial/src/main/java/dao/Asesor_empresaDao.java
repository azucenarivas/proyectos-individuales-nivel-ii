/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Asesor_empresaBean;

/**
 *
 * @author Azucena
 */
public class Asesor_empresaDao {

    Conexion conn;

    public Asesor_empresaDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(Asesor_empresaBean asesorb) {

        String sql = "insert into asesor_empresa(codigo_empresa,codigo_asesor)values(?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, asesorb.getEmpresa().getCodigo_empresa());
            ps.setInt(2, asesorb.getAsesor().getCodigo_asesor());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;

        }

    }

    public List<Asesor_empresaBean> consultarById_Asesor_empresa(int codigo) {
        String sql = "select *from asesor_empresa where codigo_asesor_empresa=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            Asesor_empresaBean asesorb;
            List<Asesor_empresaBean> lista = new LinkedList<>();

            while (rs.next()) {
                asesorb = new Asesor_empresaBean(rs.getInt("codigo_asesor_empresa"));
                asesorb.setCodigo_empresa(rs.getInt("codigo_empresa"));
                asesorb.setCodigo_asesor(rs.getInt("codigo_asesor"));
                lista.add(asesorb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean actualizar(Asesor_empresaBean asesorb) {

        String sql = "update asesor_empresa set codigo_empresa=?,codigo_asesor=? where codigo_asesor_empresa=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, asesorb.getEmpresa().getCodigo_empresa());
            ps.setInt(2, asesorb.getAsesor().getCodigo_asesor());
            ps.setInt(3, asesorb.getCodigo_asesor_empresa());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;

        }

    }

    public List<Asesor_empresaBean> ConsultarAsesorEmpresa() {

        String sql = "select ae.codigo_asesor_empresa,em.nombre_e,a.nombre_a\n"
                + "from asesor_empresa ae\n"
                + "inner join empresa em on em.codigo_empresa=ae.codigo_empresa\n"
                + "inner join asesor a on a.codigo_asesor=ae.codigo_asesor";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Asesor_empresaBean asesorb;

            ResultSet rs = ps.executeQuery();
            List<Asesor_empresaBean> lista = new LinkedList<>();

            while (rs.next()) {
                asesorb = new Asesor_empresaBean(rs.getInt("codigo_asesor_empresa"));
                asesorb.setNombre_e(rs.getString("nombre_e"));
                asesorb.setNombre_a(rs.getString("nombre_a"));
                lista.add(asesorb);

            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean eliminar(int codigo_asesor_empresa) {

        String sql = "delete from asesor_empresa where codigo_asesor_empresa=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_asesor_empresa);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

}
