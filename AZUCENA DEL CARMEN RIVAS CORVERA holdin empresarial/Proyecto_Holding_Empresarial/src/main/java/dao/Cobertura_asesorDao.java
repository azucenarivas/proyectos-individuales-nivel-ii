/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Cobertura_asesorBean;

/**
 *
 * @author azucena.rivasusam
 */
public class Cobertura_asesorDao {

    Conexion conn;

    public Cobertura_asesorDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(Cobertura_asesorBean coverb) {

        String sql = "insert into cobertura_asesor(codigo_areas,codigo_asesor)values(?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            //ps.setInt(1, asesob.getCodigo_asesor());
            ps.setInt(1, coverb.getAreas().getCodigo_areas());
            ps.setInt(2, coverb.getAsesor().getCodigo_asesor());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;

        }

    }

    public List<Cobertura_asesorBean> consultarById_covertura_asesor(int codigo) {
        String sql = "select *from cobertura_asesor where codigo_conbertura_asesor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            Cobertura_asesorBean cobertura_asesorb;
            List<Cobertura_asesorBean> lista = new LinkedList<>();

            while (rs.next()) {
                cobertura_asesorb = new Cobertura_asesorBean(rs.getInt("codigo_conbertura_asesor"));
                cobertura_asesorb.setCodigo_areas(rs.getInt("codigo_areas"));
                cobertura_asesorb.setCodigo_asesor(rs.getInt("codigo_asesor"));
                lista.add(cobertura_asesorb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean actualizar(Cobertura_asesorBean coverb) {

        String sql = "update cobertura_asesor set codigo_areas=?,codigo_asesor=? where codigo_conbertura_asesor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, coverb.getAreas().getCodigo_areas());
            ps.setInt(2, coverb.getAsesor().getCodigo_asesor());
            ps.setInt(3, coverb.getCodigo_conbertura_asesor());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;

        }

    }

    public List<Cobertura_asesorBean> ConsultarCobertura_asesor() {

        String sql = "select ca.codigo_conbertura_asesor,c.nombre_area,a.nombre_a\n"
                + "from cobertura_asesor ca\n"
                + "inner join areas c on c.codigo_areas=ca.codigo_areas\n"
                + "inner join asesor a on a.codigo_asesor=ca.codigo_asesor";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Cobertura_asesorBean cab;

            ResultSet rs = ps.executeQuery();
            List<Cobertura_asesorBean> lista = new LinkedList<>();

            while (rs.next()) {
                cab = new Cobertura_asesorBean(rs.getInt("codigo_conbertura_asesor"));
                cab.setNombre_area(rs.getString("nombre_area"));
                cab.setNombre_a(rs.getString("nombre_a"));
                lista.add(cab);

            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public List<Cobertura_asesorBean> VerCobertura_asesor() {

        String sql = "select co.codigo_conbertura_asesor,ase.nombre_a,a.nombre_area,a.descripcion_area\n"
                + "from cobertura_asesor co\n"
                + "inner join areas a on a.codigo_areas=co.codigo_areas\n"
                + "inner join asesor ase on ase.codigo_asesor=co.codigo_asesor";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Cobertura_asesorBean cab;

            ResultSet rs = ps.executeQuery();
            List<Cobertura_asesorBean> lista = new LinkedList<>();

            while (rs.next()) {
                cab = new Cobertura_asesorBean(rs.getInt("codigo_conbertura_asesor"));
                cab.setNombre_area(rs.getString("nombre_area"));
                cab.setNombre_a(rs.getString("nombre_a"));
                 cab.setDescripcion_area(rs.getString("descripcion_area"));
                lista.add(cab);

            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean eliminar(int codigo_conbertura_asesor) {

        String sql = "delete from cobertura_asesor where codigo_conbertura_asesor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_conbertura_asesor);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

}
