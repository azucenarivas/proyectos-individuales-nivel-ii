/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import modelo.EmpresaBean;

/**
 *
 * @author azucena.rivasusam
 */
public class EmpresaDao {

    Conexion conn;

    public EmpresaDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(EmpresaBean empreb) {

        String sql = "insert into empresa(nombre_e,fecha_entrada_holdind,facturacion_anual,numero_vendedores,codigo_sede,codigo_pais)values(?,?,?,?,?,?)";
       // SimpleDateFormat formato = new SimpleDateFormat("yy-MM-dd");
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            // ps.setInt(1, empreb.getCodigo_empresa());
            ps.setString(1, empreb.getNombre_e());

            ps.setDate(2, empreb.getFecha_entrada_holdind());
            //ps.setString(2, empreb.getFecha_entrada_holdind());
            ps.setString(3, empreb.getFacturacion_anual());
            ps.setInt(4, empreb.getNumero_vendedores());
            ps.setInt(5, empreb.getSede().getCodigo_sede());
            ps.setInt(6, empreb.getPais().getCodigo_pais());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public List<EmpresaBean> consultarById_empresa(int codigo) {
        String sql = "select *from empresa where codigo_empresa=?";
        SimpleDateFormat formato = new SimpleDateFormat("yy-MM-dd");
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            EmpresaBean empresab;
            List<EmpresaBean> lista = new LinkedList<>();

            while (rs.next()) {
                empresab = new EmpresaBean(rs.getInt("codigo_empresa"));
                empresab.setNombre_e(rs.getString("nombre_e"));
                // empresab.setFecha_entrada_holdind(rs.getString("fecha_entrada_holdind"));
                //ps.setString(2, formato.format(empreb.getFecha_entrada_holdind()));
                empresab.setFecha_entrada_holdind(rs.getDate("fecha_entrada_holdind"));
                empresab.setFacturacion_anual(rs.getString("facturacion_anual"));
                empresab.setNumero_vendedores(rs.getInt("numero_vendedores"));
                empresab.setCodigo_sede(rs.getInt("codigo_sede"));
                empresab.setCodigo_pais(rs.getInt("codigo_pais"));
                lista.add(empresab);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean actualizar(EmpresaBean empreb) {

        String sql = "update empresa set nombre_e=?,fecha_entrada_holdind=?,facturacion_anual=?,numero_vendedores=?,codigo_sede=?,codigo_pais=? where codigo_empresa=?";
        SimpleDateFormat formato = new SimpleDateFormat("yy-MM-dd");
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, empreb.getNombre_e());
            // ps.setString(2, empreb.getFecha_entrada_holdind());
            ps.setString(2, formato.format(empreb.getFecha_entrada_holdind()));
            ps.setString(3, empreb.getFacturacion_anual());
            ps.setInt(4, empreb.getNumero_vendedores());
            ps.setInt(6, empreb.getSede().getCodigo_sede());
            ps.setInt(5, empreb.getPais().getCodigo_pais());
            ps.setInt(7, empreb.getCodigo_empresa());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public List<EmpresaBean> consultar_empresa() {

        String sql = "select e.codigo_empresa,e.nombre_e,e.fecha_entrada_holdind,e.facturacion_anual,e.numero_vendedores,p.nombre_p,s.ciudad\n"
                + "from empresa e\n"
                + "inner join sede s on s.codigo_sede=e.codigo_sede\n"
                + "inner join pais p on p.codigo_pais=e.codigo_pais";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            //System.out.println(ps);
            EmpresaBean empreb;
            ResultSet rt = ps.executeQuery();
            // System.out.println(rt);
            List<EmpresaBean> lista = new LinkedList<>();
            while (rt.next()) {
                empreb = new EmpresaBean(rt.getInt("codigo_empresa"));
                empreb.setNombre_e(rt.getString("nombre_e"));
                // empreb.setFecha_entrada_holdind(rt.getString("fecha_entrada_holdind"));
                empreb.setFecha_entrada_holdind(rt.getDate("fecha_entrada_holdind"));
                empreb.setFacturacion_anual(rt.getString("facturacion_anual"));
                empreb.setNumero_vendedores(rt.getInt("numero_vendedores"));
                empreb.setCiudad(rt.getString("ciudad"));
                empreb.setNombre_p(rt.getString("nombre_p"));
                //System.out.println(empreb);
                lista.add(empreb);

            }

            return lista;

        } catch (Exception e) {
            return null;
        }

    }

    public boolean eliminar(int codigo_empresa) {

        String sql = "delete from empresa  where codigo_empresa=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_empresa);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

}
