/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.PaisBean;

/**
 *
 * @author azucena.rivasusam
 */
public class PaisDao {

    Conexion conn;

    public PaisDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(PaisBean paisb) {

        String sql = "insert into pais(nombre_p,PIB,numero_habitantes,capital)values(?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            // ps.setInt(1, paisb.getCodigo_pais());
            ps.setString(1, paisb.getNombre_p());
            ps.setDouble(2, paisb.getPIB());
            ps.setInt(3, paisb.getNumero_habitantes());
            ps.setString(4, paisb.getCapital());
            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public List<PaisBean> consultarById_pais(int codigo) {
        String sql = "select *from pais where codigo_pais=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            PaisBean paisb;
            List<PaisBean> lista = new LinkedList<>();

            while (rs.next()) {
                paisb = new PaisBean(rs.getInt("codigo_pais"));
                paisb.setNombre_p(rs.getString("nombre_p"));
                paisb.setPIB(rs.getDouble("PIB"));
                paisb.setNumero_habitantes(rs.getInt("numero_habitantes"));
                paisb.setCapital(rs.getString("capital"));
                lista.add(paisb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean actualizar(PaisBean paisb) {

        String sql = "update pais set nombre_p=?,PIB=?,numero_habitantes=?,capital=? where codigo_pais=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, paisb.getNombre_p());
            ps.setDouble(2, paisb.getPIB());
            ps.setInt(3, paisb.getNumero_habitantes());
            ps.setString(4, paisb.getCapital());
            ps.setInt(5, paisb.getCodigo_pais());
            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public List<PaisBean> consultar_pais() {

        String sql = "select *from pais";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            PaisBean paib;
            ResultSet rt = ps.executeQuery();
            List<PaisBean> lista = new LinkedList<>();
            while (rt.next()) {
                paib = new PaisBean(rt.getInt("codigo_pais"));
                paib.setNombre_p(rt.getString("nombre_p"));
                paib.setPIB(rt.getDouble("PIB"));
                paib.setNumero_habitantes(rt.getInt("numero_habitantes"));
                paib.setCapital(rt.getString("capital"));
                lista.add(paib);

            }

            return lista;

        } catch (Exception e) {
            return null;
        }

    }

    public boolean eliminar(int codigo_pais) {

        String sql = "delete from pais  where codigo_pais=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_pais);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }
}
