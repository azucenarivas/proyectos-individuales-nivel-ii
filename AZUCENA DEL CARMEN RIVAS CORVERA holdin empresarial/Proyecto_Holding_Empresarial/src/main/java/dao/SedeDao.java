/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.SedeBean;

/**
 *
 * @author azucena.rivasusam
 */
public class SedeDao {

    Conexion conn;

    public SedeDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(SedeBean sedeb) {

        String sql = "insert into sede(ciudad) values(?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            // ps.setInt(1, sedeb.getCodigo_sede());
            ps.setString(1, sedeb.getCiudad());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    
     public List<SedeBean> consultarById_sedes(int codigo) {
        String sql = "select *from sede where codigo_sede=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            SedeBean sedeb;
            List<SedeBean> lista = new LinkedList<>();

            while (rs.next()) {
                sedeb = new SedeBean(rs.getInt("codigo_sede"));
                sedeb.setCiudad(rs.getString("ciudad"));
                lista.add(sedeb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }
     
    public boolean actualizar(SedeBean sedeb) {

        String sql = "update sede set ciudad=? where codigo_sede=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, sedeb.getCiudad());
            ps.setInt(2, sedeb.getCodigo_sede());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public List<SedeBean> consultar_sede() {

        String sql = "select *from sede";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            SedeBean sedeb;
            ResultSet rt = ps.executeQuery();
            List<SedeBean> lista = new LinkedList<>();
            while (rt.next()) {
                sedeb = new SedeBean(rt.getInt("codigo_sede"));
                sedeb.setCiudad(rt.getString("ciudad"));
                //sedeb.setNombre_p(rt.getString("nombre_p"));
                lista.add(sedeb);
            }
            return lista;

        } catch (Exception e) {
            return null;
        }

    }

    public boolean eliminar(int codigo_sede) {

        String sql = "delete from sede  where codigo_sede=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_sede);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

}
