/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.UsuarioBean;

/**
 *
 * @author azucena.rivasusam
 */
public class UsuarioDao {

    Conexion conn;

    public UsuarioDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean autentificacion(String usuario, String pass) throws Exception {
        ResultSet rt = null;
        String sql = "select *from usuario where usuario=? and pass=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, usuario);
            ps.setString(2, pass);
            rt = ps.executeQuery();
            if (rt.absolute(1)) {
                return true;
            }

        } catch (Exception e) {
            System.err.println("Error" + e);

        }
        return false;

    }

    public boolean insertar(String nombre_u, String apellido_u, String direccion_u, String telefono_u, String usuario, String pass) {

        String sql = "insert into usuario(nombre_u,apellido_u,direccion_u,telefono_u,usuario,pass)values(?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            //ps.setInt(1, usuab.getCodigo_usuario());
            ps.setString(1, nombre_u);
            ps.setString(2, apellido_u);
            ps.setString(3, direccion_u);
            ps.setString(4, telefono_u);
            ps.setString(5, usuario);
            ps.setString(6, pass);
            ps.executeUpdate();
            if (ps.executeUpdate() == 1) {
                return true;
            }
        } catch (Exception e) {
            System.err.println("Error" + e);
        }

        return false;
    }

    public boolean actualizar(UsuarioBean usuab) {

        String sql = "update usuario set nombre_u=?,apellido_u=?,direccion_u=?,telefono_u=?,usuario=?,pass=? where codigo_usuario=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, usuab.getNombre_u());
            ps.setString(2, usuab.getApellido_u());
            ps.setString(3, usuab.getDireccion_u());
            ps.setString(4, usuab.getTelefono_u());
            ps.setString(5, usuab.getUsuario());
            ps.setString(6, usuab.getPass());
            ps.setInt(7, usuab.getCodigo_usuario());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public List<UsuarioBean> consultar_usuarios() {

        String sql = "select *from usuario";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            UsuarioBean usub;
            ResultSet rs = ps.executeQuery();
            List<UsuarioBean> lista = new LinkedList<>();
            while (rs.next()) {
                usub = new UsuarioBean(rs.getInt("codigo_usuario"));
                usub.setNombre_u(rs.getString("nombre_u"));
                usub.setApellido_u(rs.getString("apellido_u"));
                usub.setDireccion_u(rs.getString("direccion_u"));
                usub.setTelefono_u(rs.getString("telefono_u"));
                usub.setUsuario(rs.getString("usuario"));
                usub.setApellido_u(rs.getString("pass"));
                lista.add(usub);

            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int codigo_usuario) {

        String sql = "delete from usuario  where codigo_usuario=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_usuario);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

}
