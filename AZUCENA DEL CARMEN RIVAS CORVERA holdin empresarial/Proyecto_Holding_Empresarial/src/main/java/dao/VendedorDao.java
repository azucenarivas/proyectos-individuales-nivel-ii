/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.VendedorBean;

/**
 *
 * @author azucena.rivasusam
 */
public class VendedorDao {

    Conexion conn;

    public VendedorDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(VendedorBean venb) {

        String sql = "insert into vendedor(nombre_v,direccion_v)values(?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            //ps.setInt(1, venb.getCodigo_vendedor());
            ps.setString(1, venb.getNombre_v());
            ps.setString(2, venb.getDireccion_v());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public List<VendedorBean> consultarById_vendedor(int codigo) {
        String sql = "select *from vendedor where codigo_vendedor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            VendedorBean vendedorb;
            List<VendedorBean> lista = new LinkedList<>();

            while (rs.next()) {
                vendedorb = new VendedorBean(rs.getInt("codigo_vendedor"));
                vendedorb.setNombre_v(rs.getString("nombre_v"));
                vendedorb.setDireccion_v(rs.getString("direccion_v"));
                lista.add(vendedorb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean actualizar(VendedorBean venb) {

        String sql = "update vendedor set nombre_v=?,direccion_v=? where codigo_vendedor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, venb.getNombre_v());
            ps.setString(2, venb.getDireccion_v());
            ps.setInt(3, venb.getCodigo_vendedor());
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

    public List<VendedorBean> consultar_vendedor() {

        String sql = "select *from vendedor";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            VendedorBean venb;
            ResultSet rt = ps.executeQuery();
            List<VendedorBean> lista = new LinkedList<>();
            while (rt.next()) {
                venb = new VendedorBean(rt.getInt("codigo_vendedor"));
                venb.setNombre_v(rt.getString("nombre_v"));
                venb.setDireccion_v(rt.getString("direccion_v"));
                lista.add(venb);

            }

            return lista;

        } catch (Exception e) {
            return null;
        }

    }

    public boolean eliminar(int codigo_vendedor) {

        String sql = "delete from vendedor  where codigo_vendedor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, codigo_vendedor);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

}
