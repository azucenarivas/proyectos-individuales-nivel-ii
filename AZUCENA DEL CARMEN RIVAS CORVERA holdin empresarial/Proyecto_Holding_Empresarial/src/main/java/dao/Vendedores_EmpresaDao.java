/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import modelo.Vendedores_empresaBean;

/**
 *
 * @author azucena.rivasusam
 */
public class Vendedores_EmpresaDao {
    
    Conexion conn;
    
    public Vendedores_EmpresaDao(Conexion conn) {
        this.conn = conn;
    }
    
    public boolean insertar(Vendedores_empresaBean vendeb) {
        
        String sql = "insert into vendedores_empresa(fecha_contratacion,codigo_vendedor,codigo_empresa)values(?,?,?)";
        // SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd");
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            //ps.setInt(1, vendeb.getCodigo_vendedor_empresa());
            // ps.setString(1, format.format(vendeb.getFecha_contratacion()));
            ps.setString(1, vendeb.getFecha_contratacion());
            ps.setInt(2, vendeb.getVendedor().getCodigo_vendedor());
            ps.setInt(3, vendeb.getEmpresa().getCodigo_empresa());
            ps.executeUpdate();
            
            return true;
            
        } catch (Exception e) {
            return false;
        }
    }
    
    public List<Vendedores_empresaBean> consultarById_vendedores_empresa(int codigo) {
        String sql = "select *from vendedores_empresa where codigo_vendedor_empresa=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            Vendedores_empresaBean vendedoresb;
            List<Vendedores_empresaBean> lista = new LinkedList<>();
            
            while (rs.next()) {
                vendedoresb = new Vendedores_empresaBean(rs.getInt("codigo_vendedor_empresa"));
                vendedoresb.setFecha_contratacion(rs.getString("fecha_contratacion"));
                vendedoresb.setCodigo_vendedor(rs.getInt("codigo_vendedor"));
                vendedoresb.setCodigo_empresa(rs.getInt("codigo_empresa"));
                lista.add(vendedoresb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
        
    }
    
    public boolean actualizar(Vendedores_empresaBean vendeb) {
        
        String sql = "update vendedores_empresa set fecha_contratacion=?,codigo_vendedor=?,codigo_empresa=? where codigo_vendedor_empresa=?";
        // SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            // ps.setString(1, format.format(vendeb.getFecha_contratacion()));
            ps.setString(1, vendeb.getFecha_contratacion());
            ps.setInt(2, vendeb.getVendedor().getCodigo_vendedor());
            ps.setInt(3, vendeb.getEmpresa().getCodigo_empresa());
            ps.setInt(4, vendeb.getCodigo_vendedor_empresa());
            ps.executeUpdate();
            
            return true;
        } catch (Exception e) {
            return false;
        }
        
    }
    
    public List<Vendedores_empresaBean> consultar_Vendedores_empresa() {
        
        String sql = "select ve.codigo_vendedor_empresa,ve.fecha_contratacion,v.nombre_v,e.nombre_e\n"
                + "from vendedores_empresa ve\n"
                + "inner join vendedor v on v.codigo_vendedor=ve.codigo_vendedor\n"
                + "inner join empresa e on e.codigo_empresa=ve.codigo_empresa;";
        
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Vendedores_empresaBean venb;
            ResultSet rs = ps.executeQuery();
            List<Vendedores_empresaBean> lista = new LinkedList<>();
            while (rs.next()) {
                
                venb = new Vendedores_empresaBean(rs.getInt("codigo_vendedor_empresa"));
                venb.setFecha_contratacion(rs.getString("fecha_contratacion"));
                venb.setNombre_v(rs.getString("nombre_v"));
                venb.setNombre_e(rs.getString("nombre_e"));
                lista.add(venb);
                
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
        
    }
    
    public List<Vendedores_empresaBean> Ver_Vendedores_empresa() {
        
        String sql = "select ve.codigo_vendedor_empresa,v.nombre_v,v.direccion_v,e.nombre_e\n"
                + "from vendedores_empresa ve\n"
                + "inner join vendedor v on v.codigo_vendedor=ve.codigo_vendedor\n"
                + "inner join empresa e on e.codigo_empresa=ve.codigo_empresa";
        
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Vendedores_empresaBean venb;
            ResultSet rs = ps.executeQuery();
            List<Vendedores_empresaBean> lista = new LinkedList<>();
            while (rs.next()) {
                
                venb = new Vendedores_empresaBean(rs.getInt("codigo_vendedor_empresa"));
                venb.setNombre_v(rs.getString("nombre_v"));
                venb.setDireccion_v(rs.getString("direccion_v"));
                venb.setNombre_e(rs.getString("nombre_e"));
                lista.add(venb);
                
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
        
    }
    
    public boolean eliminar(int codigo_vendedor_empresa) {
        
        String sql = "delete from vendedores_empresa  where codigo_vendedor_empresa=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_vendedor_empresa);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
        
    }
    
}
