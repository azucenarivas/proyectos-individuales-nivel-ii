/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class AreasBean {

    private int codigo_areas;
    private String nombre_area;
    private String descripcion_area;

    private int codigo_conbertura_asesor;
    
    public AreasBean(int codigo_areas) {
        this.codigo_areas = codigo_areas;
    }

    public int getCodigo_conbertura_asesor() {
        return codigo_conbertura_asesor;
    }

    public void setCodigo_conbertura_asesor(int codigo_conbertura_asesor) {
        this.codigo_conbertura_asesor = codigo_conbertura_asesor;
    }

    
    
    public int getCodigo_areas() {
        return codigo_areas;
    }

    public void setCodigo_areas(int codigo_areas) {
        this.codigo_areas = codigo_areas;
    }

    public String getNombre_area() {
        return nombre_area;
    }

    public void setNombre_area(String nombre_area) {
        this.nombre_area = nombre_area;
    }

    public String getDescripcion_area() {
        return descripcion_area;
    }

    public void setDescripcion_area(String descripcion_area) {
        this.descripcion_area = descripcion_area;
    }

}
