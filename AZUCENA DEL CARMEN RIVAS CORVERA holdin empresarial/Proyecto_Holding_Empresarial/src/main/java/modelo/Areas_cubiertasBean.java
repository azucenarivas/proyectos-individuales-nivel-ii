/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Date;

/**
 *
 * @author azucena.rivasusam
 */
public class Areas_cubiertasBean {

    public int codigo_areas_cubiertas;
    private EmpresaBean empresa;
    private AreasBean areas;

    //variables de impresion
    
    private int codigo_empresa;
    private String nombre_e ;
    
    private int codigo_areas;
    private String nombre_area;
    private String descripcion_area;
    ///variables de impresion para la vista de tdas las areas
    
    private String nombre_p;
    private String ciudad;
    
    private int numero_vendedores;
    private Date fecha_entrada_holdind;
    
    
    public Areas_cubiertasBean(int codigo_areas_cubiertas) {
        this.codigo_areas_cubiertas = codigo_areas_cubiertas;
    }

    public String getNombre_p() {
        return nombre_p;
    }

    public String getDescripcion_area() {
        return descripcion_area;
    }

    public void setDescripcion_area(String descripcion_area) {
        this.descripcion_area = descripcion_area;
    }

    public void setNombre_p(String nombre_p) {
        this.nombre_p = nombre_p;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getNumero_vendedores() {
        return numero_vendedores;
    }

    public void setNumero_vendedores(int numero_vendedores) {
        this.numero_vendedores = numero_vendedores;
    }

    public Date getFecha_entrada_holdind() {
        return fecha_entrada_holdind;
    }

    public void setFecha_entrada_holdind(Date fecha_entrada_holdind) {
        this.fecha_entrada_holdind = fecha_entrada_holdind;
    }

  
   
 
    public int getCodigo_empresa() {
        return codigo_empresa;
    }

    public void setCodigo_empresa(int codigo_empresa) {
        this.codigo_empresa = codigo_empresa;
    }

    public String getNombre_e() {
        return nombre_e;
    }

    public void setNombre_e(String nombre_e) {
        this.nombre_e = nombre_e;
    }

    public int getCodigo_areas() {
        return codigo_areas;
    }

    public void setCodigo_areas(int codigo_areas) {
        this.codigo_areas = codigo_areas;
    }

  

    public String getNombre_area() {
        return nombre_area;
    }

    public void setNombre_area(String nombre_area) {
        this.nombre_area = nombre_area;
    }

    
    
    public int getCodigo_areas_cubiertas() {
        return codigo_areas_cubiertas;
    }

    public void setCodigo_areas_cubiertas(int codigo_areas_cubiertas) {
        this.codigo_areas_cubiertas = codigo_areas_cubiertas;
    }

    public EmpresaBean getEmpresa() {
        return empresa;
    }

    public void setEmpresa(EmpresaBean empresa) {
        this.empresa = empresa;
    }

    public AreasBean getAreas() {
        return areas;
    }

    public void setAreas(AreasBean areas) {
        this.areas = areas;
    }

}
