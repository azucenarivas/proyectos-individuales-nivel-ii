/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class AsesorBean {

    private int codigo_asesor;
    private String nombre_a;
    private String direccion_a;
    private String titulacion;

    private int codigo_conbertura_asesor;

    public AsesorBean(int codigo_asesor) {
        this.codigo_asesor = codigo_asesor;
    }

    public int getCodigo_conbertura_asesor() {
        return codigo_conbertura_asesor;
    }

    public void setCodigo_conbertura_asesor(int codigo_conbertura_asesor) {
        this.codigo_conbertura_asesor = codigo_conbertura_asesor;
    }

    
    public int getCodigo_asesor() {
        return codigo_asesor;
    }

    public void setCodigo_asesor(int codigo_asesor) {
        this.codigo_asesor = codigo_asesor;
    }

    public String getNombre_a() {
        return nombre_a;
    }

    public void setNombre_a(String nombre_a) {
        this.nombre_a = nombre_a;
    }

    public String getDireccion_a() {
        return direccion_a;
    }

    public void setDireccion_a(String direccion_a) {
        this.direccion_a = direccion_a;
    }

    public String getTitulacion() {
        return titulacion;
    }

    public void setTitulacion(String titulacion) {
        this.titulacion = titulacion;
    }

}
