/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Azucena
 */
public class Asesor_empresaBean {

    private int codigo_asesor_empresa;
    private EmpresaBean empresa;
    private AsesorBean asesor;

//variables de impresion
    private int codigo_empresa;
    private String nombre_e;

    private int codigo_asesor;
    private String nombre_a;

    
    public Asesor_empresaBean(int codigo_asesor_empresa) {
        this.codigo_asesor_empresa = codigo_asesor_empresa;
    }
    
    
    public int getCodigo_asesor_empresa() {
        return codigo_asesor_empresa;
    }

    public void setCodigo_asesor_empresa(int codigo_asesor_empresa) {
        this.codigo_asesor_empresa = codigo_asesor_empresa;
    }

    public int getCodigo_empresa() {
        return codigo_empresa;
    }

    public void setCodigo_empresa(int codigo_empresa) {
        this.codigo_empresa = codigo_empresa;
    }

   
    public int getCodigo_asesor() {
        return codigo_asesor;
    }

    public void setCodigo_asesor(int codigo_asesor) {
        this.codigo_asesor = codigo_asesor;
    }

    public String getNombre_e() {
        return nombre_e;
    }

    public void setNombre_e(String nombre_e) {
        this.nombre_e = nombre_e;
    }

    public String getNombre_a() {
        return nombre_a;
    }

    public void setNombre_a(String nombre_a) {
        this.nombre_a = nombre_a;
    }

   

    public EmpresaBean getEmpresa() {
        return empresa;
    }

    public void setEmpresa(EmpresaBean empresa) {
        this.empresa = empresa;
    }

    public AsesorBean getAsesor() {
        return asesor;
    }

    public void setAsesor(AsesorBean asesor) {
        this.asesor = asesor;
    }

    
}
