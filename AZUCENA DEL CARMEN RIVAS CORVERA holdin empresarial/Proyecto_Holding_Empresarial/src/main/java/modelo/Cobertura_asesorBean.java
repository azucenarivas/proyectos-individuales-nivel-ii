/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class Cobertura_asesorBean {
    
    private int codigo_conbertura_asesor;
    private AreasBean areas;
    private AsesorBean asesor;
    
    //variables de impresion
    private int codigo_areas;
    private String nombre_area;
     private int codigo_asesor;
    private String nombre_a;
    
    private String descripcion_area;

      public Cobertura_asesorBean(int codigo_conbertura_asesor) {
        this.codigo_conbertura_asesor = codigo_conbertura_asesor;
    }

    public String getDescripcion_area() {
        return descripcion_area;
    }

    public void setDescripcion_area(String descripcion_area) {
        this.descripcion_area = descripcion_area;
    }

      
      
    public int getCodigo_areas() {
        return codigo_areas;
    }

    public void setCodigo_areas(int codigo_areas) {
        this.codigo_areas = codigo_areas;
    }

  

    public String getNombre_area() {
        return nombre_area;
    }

    public void setNombre_area(String nombre_area) {
        this.nombre_area = nombre_area;
    }

    public int getCodigo_asesor() {
        return codigo_asesor;
    }

    public void setCodigo_asesor(int codigo_asesor) {
        this.codigo_asesor = codigo_asesor;
    }

    public String getNombre_a() {
        return nombre_a;
    }

    public void setNombre_a(String nombre_a) {
        this.nombre_a = nombre_a;
    }
      
      
      
    public int getCodigo_conbertura_asesor() {
        return codigo_conbertura_asesor;
    }

    public void setCodigo_conbertura_asesor(int codigo_conbertura_asesor) {
        this.codigo_conbertura_asesor = codigo_conbertura_asesor;
    }

    public AreasBean getAreas() {
        return areas;
    }

    public void setAreas(AreasBean areas) {
        this.areas = areas;
    }

    public AsesorBean getAsesor() {
        return asesor;
    }

    public void setAsesor(AsesorBean asesor) {
        this.asesor = asesor;
    }
    
    
    
}
