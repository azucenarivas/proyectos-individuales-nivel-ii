/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Date;

/**
 *
 * @author azucena.rivasusam
 */
public class EmpresaBean {

    private int codigo_empresa;
    private String nombre_e;
    private Date fecha_entrada_holdind;
    private String facturacion_anual;
    private int numero_vendedores;
    private SedeBean sede;
    private PaisBean pais;

    // variables de impresion
    private int codigo_pais;
    private String nombre_p;
    private int codigo_sede;
    private String ciudad;

    public EmpresaBean(int codigo_empresa) {
        this.codigo_empresa = codigo_empresa;
    }

    public int getCodigo_sede() {
        return codigo_sede;
    }

    public void setCodigo_sede(int codigo_sede) {
        this.codigo_sede = codigo_sede;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getCodigo_pais() {
        return codigo_pais;
    }

    public SedeBean getSede() {
        return sede;
    }

    public void setSede(SedeBean sede) {
        this.sede = sede;
    }

    public void setCodigo_pais(int codigo_pais) {
        this.codigo_pais = codigo_pais;
    }

    public String getNombre_p() {
        return nombre_p;
    }

    public void setNombre_p(String nombre_p) {
        this.nombre_p = nombre_p;
    }

    public int getCodigo_empresa() {
        return codigo_empresa;
    }

    public void setCodigo_empresa(int codigo_empresa) {
        this.codigo_empresa = codigo_empresa;
    }

    public String getNombre_e() {
        return nombre_e;
    }

    public void setNombre_e(String nombre_e) {
        this.nombre_e = nombre_e;
    }

    public Date getFecha_entrada_holdind() {
        return fecha_entrada_holdind;
    }

    public void setFecha_entrada_holdind(Date fecha_entrada_holdind) {
        this.fecha_entrada_holdind = fecha_entrada_holdind;
    }

  

    public String getFacturacion_anual() {
        return facturacion_anual;
    }

    public void setFacturacion_anual(String facturacion_anual) {
        this.facturacion_anual = facturacion_anual;
    }

    public int getNumero_vendedores() {
        return numero_vendedores;
    }

    public void setNumero_vendedores(int numero_vendedores) {
        this.numero_vendedores = numero_vendedores;
    }

    public PaisBean getPais() {
        return pais;
    }

    public void setPais(PaisBean pais) {
        this.pais = pais;
    }

}
