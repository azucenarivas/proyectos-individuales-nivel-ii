/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class PaisBean {

    private int codigo_pais;
    private String nombre_p;
    private double PIB;
    private int numero_habitantes;
    private String capital;

    public PaisBean(int codigo_pais) {
        this.codigo_pais = codigo_pais;
    }

    public int getCodigo_pais() {
        return codigo_pais;
    }

    public void setCodigo_pais(int codigo_pais) {
        this.codigo_pais = codigo_pais;
    }

    public String getNombre_p() {
        return nombre_p;
    }

    public void setNombre_p(String nombre_p) {
        this.nombre_p = nombre_p;
    }

    public double getPIB() {
        return PIB;
    }

    public void setPIB(double PIB) {
        this.PIB = PIB;
    }

    public int getNumero_habitantes() {
        return numero_habitantes;
    }

    public void setNumero_habitantes(int numero_habitantes) {
        this.numero_habitantes = numero_habitantes;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

}
