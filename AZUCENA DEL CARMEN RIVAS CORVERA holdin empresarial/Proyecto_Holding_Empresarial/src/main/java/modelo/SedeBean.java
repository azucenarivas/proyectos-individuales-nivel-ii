/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class SedeBean {

    private int codigo_sede;
    private String ciudad;


    public SedeBean(int codigo_sede) {
        this.codigo_sede = codigo_sede;
    }

    public int getCodigo_sede() {
        return codigo_sede;
    }

    public void setCodigo_sede(int codigo_sede) {
        this.codigo_sede = codigo_sede;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

}
