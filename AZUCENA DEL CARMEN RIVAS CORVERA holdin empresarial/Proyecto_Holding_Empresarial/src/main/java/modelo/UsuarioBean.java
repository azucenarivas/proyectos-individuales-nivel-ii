/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Azucena
 */
public class UsuarioBean {

    private int codigo_usuario;
    private String nombre_u;
    private String apellido_u;
    private String direccion_u;
    private String telefono_u;
    private String usuario;
    private String pass;

    
    public UsuarioBean(int codigo_usuario) {
        this.codigo_usuario = codigo_usuario;
    }

    public int getCodigo_usuario() {
        return codigo_usuario;
    }

    public void setCodigo_usuario(int codigo_usuario) {
        this.codigo_usuario = codigo_usuario;
    }

    public String getNombre_u() {
        return nombre_u;
    }

    public void setNombre_u(String nombre_u) {
        this.nombre_u = nombre_u;
    }

    public String getApellido_u() {
        return apellido_u;
    }

    public void setApellido_u(String apellido_u) {
        this.apellido_u = apellido_u;
    }

    public String getDireccion_u() {
        return direccion_u;
    }

    public void setDireccion_u(String direccion_u) {
        this.direccion_u = direccion_u;
    }

    public String getTelefono_u() {
        return telefono_u;
    }

    public void setTelefono_u(String telefono_u) {
        this.telefono_u = telefono_u;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    
    
}
