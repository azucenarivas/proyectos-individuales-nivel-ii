/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class VendedorBean {

    private int codigo_vendedor;
    private String nombre_v;
    private String direccion_v;

    public VendedorBean(int codigo_vendedor) {
        this.codigo_vendedor = codigo_vendedor;
    }

    public int getCodigo_vendedor() {
        return codigo_vendedor;
    }

    public void setCodigo_vendedor(int codigo_vendedor) {
        this.codigo_vendedor = codigo_vendedor;
    }

    public String getNombre_v() {
        return nombre_v;
    }

    public void setNombre_v(String nombre_v) {
        this.nombre_v = nombre_v;
    }

    public String getDireccion_v() {
        return direccion_v;
    }

    public void setDireccion_v(String direccion_v) {
        this.direccion_v = direccion_v;
    }

}
