/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class Vendedores_empresaBean {

    private int codigo_vendedor_empresa;
    private String fecha_contratacion;
    private VendedorBean vendedor;
    private EmpresaBean empresa;

    ///variables de impresion
    private int codigo_vendedor;
    ;
   private int codigo_empresa;
    private String nombre_v;
    private String direccion_v;
    private String nombre_e;

    public Vendedores_empresaBean(int codigo_vendedor_empresa) {
        this.codigo_vendedor_empresa = codigo_vendedor_empresa;
    }

    public int getCodigo_vendedor() {
        return codigo_vendedor;
    }

    public String getDireccion_v() {
        return direccion_v;
    }

    public void setDireccion_v(String direccion_v) {
        this.direccion_v = direccion_v;
    }
    
    

    public void setCodigo_vendedor(int codigo_vendedor) {
        this.codigo_vendedor = codigo_vendedor;
    }

    public int getCodigo_empresa() {
        return codigo_empresa;
    }

    public void setCodigo_empresa(int codigo_empresa) {
        this.codigo_empresa = codigo_empresa;
    }

    public String getNombre_v() {
        return nombre_v;
    }

    public void setNombre_v(String nombre_v) {
        this.nombre_v = nombre_v;
    }

    public String getNombre_e() {
        return nombre_e;
    }

    public void setNombre_e(String nombre_e) {
        this.nombre_e = nombre_e;
    }

    public int getCodigo_vendedor_empresa() {
        return codigo_vendedor_empresa;
    }

    public void setCodigo_vendedor_empresa(int codigo_vendedor_empresa) {
        this.codigo_vendedor_empresa = codigo_vendedor_empresa;
    }

    public String getFecha_contratacion() {
        return fecha_contratacion;
    }

    public void setFecha_contratacion(String fecha_contratacion) {
        this.fecha_contratacion = fecha_contratacion;
    }

    public VendedorBean getVendedor() {
        return vendedor;
    }

    public void setVendedor(VendedorBean vendedor) {
        this.vendedor = vendedor;
    }

    public EmpresaBean getEmpresa() {
        return empresa;
    }

    public void setEmpresa(EmpresaBean empresa) {
        this.empresa = empresa;
    }

}
