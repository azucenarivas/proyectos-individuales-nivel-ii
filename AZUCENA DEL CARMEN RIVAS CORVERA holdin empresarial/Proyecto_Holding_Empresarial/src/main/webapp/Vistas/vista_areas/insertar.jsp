<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>

    <body>
        <jsp:include page="/menu_encabezado.jsp"/>




        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        <center> <form class="form-horizontal" method="post" action="AreasServlet">

                                <fieldset>
                                    <legend class="text-center header">

                                        <h3>
                                            <u  class="text-default" class="brown-text"><p>Registro de una Area</p> </u>
                                        </h3>
                                    </legend>

                                    <br>



                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Ingrese el Nombre de la Area:</span>
                                        <div class="col-md-8">
                                            <input id="txtnombre" name="txtnombre" type="text" placeholder="Ingrese un Nombre" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Ingrese la Descripcion de la Area:</span>
                                        <div class="col-md-8">
                                            <input id="txtfecha" name="txtdescripcion" type="text" placeholder="Ingrese una descripcion " class="form-control">
                                        </div>
                                    </div>

                                    <br>

                                    <div class="form-group">
                                     
                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos" class=" btn btn-info btn-lg"/>

                                            </div>
                                        </div>
                                    </div>



                                </fieldset>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


