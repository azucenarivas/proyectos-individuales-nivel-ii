<%-- 
    Document   : lista_areas
    Created on : 11-23-2019, 01:41:28 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="/menu_encabezado.jsp"/>
        <br>
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <div class="col m12" align="center">
                        <h3>
                            <u  class="text-default"><p>Descripción de las Areas Cubiertas </p> </u>
                        </h3>

                    </div>
                </div>
                <br>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead bgcolor="#eceff1">
                        <tr class="info">
                            <th bgcolor="#fafafa" scope="col">Codigo de areas Cubiertas</th>
                            <th scope="col" class="text-BLACK">Nombre de la Empresa</th>
                            <th scope="col" class="text-BLACK">Nombre del Areas</th>
                            <th scope="col" class="text-BLACK">Descripcion de Areas</th>
                            <th scope="col" ></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>

                        
                        <c:forEach items="${areas_cubiertas}" var="a">
                                <tr>
                                <td>${a.codigo_areas_cubiertas}</td>
                                <td>${a.nombre_e}</td>
                                <td>${a.nombre_area}</td>
                                <td>${a.descripcion_area}</td>
                            </tr>

                            </c:forEach>
                            
                       
                    </tbody>
                </table>

            </div>


        </div>
    </body>
</html>
