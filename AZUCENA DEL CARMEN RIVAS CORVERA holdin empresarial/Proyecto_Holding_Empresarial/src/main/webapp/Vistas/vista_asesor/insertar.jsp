<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>

    <body>
        <jsp:include page="/menu_encabezado.jsp"/>




        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        <center> <form class="form-horizontal" method="post" action="AsesorServlet">

                                <fieldset>
                                    <legend class="text-center header">

                                        <h3>
                                            <u  class="text-default" class="brown-text"><p>Registro para un Asesor</p> </u>
                                        </h3>
                                    </legend>

                                    <br>



                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Ingrese el Nombre de la Asesor:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtnombre" name="txtnombre" type="text" placeholder="Ingrese un Nombre" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Ingrese la Direccion del Asesor:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtdireccion" name="txtdireccion" type="text" placeholder="Ingrese una direccion " class="form-control">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Ingrese la Titulacion del Asesor:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtdireccion" name="txttitulacion" type="text" placeholder="Ingrese datos " class="form-control">
                                        </div>
                                    </div>
                                    






                                    <br>

                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <input type="submit" name="btnsave" value="Guardar Datos" class=" btn btn-info btn-lg"/>

                                        </div>
                                    </div>



                                </fieldset>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
