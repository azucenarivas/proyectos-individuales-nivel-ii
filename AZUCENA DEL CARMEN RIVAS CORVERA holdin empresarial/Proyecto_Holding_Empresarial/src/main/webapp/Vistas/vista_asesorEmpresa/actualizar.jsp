<%-- 
    Document   : actualizar
    Created on : 11-25-2019, 01:25:31 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="/menu_encabezado.jsp"/>
        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        <center> <form class="form-horizontal" method="post" action="AsesorEmpresaServlet">
                                <c:forEach items="${asesorempresa}" var="asesore">

                                    <fieldset>
                                        <legend class="text-center header">

                                            <h3>
                                                <u  class="text-default" class="brown-text"><p>Actualizar la Asignacion de Coberturas de Asesores</p> </u>
                                            </h3>
                                        </legend>

                                        <br>

                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Seleccione una Empresa:<i class="fa fa-user bigicon"></i></span>
                                            <div class="col-md-8">
                                                <input type="hidden" name="txtID" value="${asesore.codigo_asesor_empresa}"/>
                                                <select name="cmbempresa" class="form-control">
                                                    <c:forEach items="${empresa}" var="em">
                                                        <c:choose>
                                                            <c:when test="${asesore.codigo_empresa==em.codigo_empresa}">


                                                                <option value="${em.codigo_empresa}" selected=""> ${em.nombre_e}
                                                                </option>
                                                            </c:when>
                                                            <c:otherwise>

                                                                <option value="${em.codigo_empresa}"> ${em.nombre_e}
                                                                </option>
                                                            </c:otherwise>

                                                        </c:choose>


                                                    </c:forEach>

                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Seleccione el Asesor:<i class="fa fa-user bigicon"></i></span>
                                            <div class="col-md-8">

                                                <select name="cmbasesor" class="form-control">

                                                    <c:forEach items="${asesor}" var="a">

                                                        <c:choose>
                                                            <c:when test="${asesore.codigo_asesor==a.codigo_asesor}">


                                                                <option value="${a.codigo_asesor}" selected=""> ${a.nombre_a}
                                                                </option>
                                                            </c:when>
                                                            <c:otherwise>

                                                                <option value="${a.codigo_asesor}"> ${a.nombre_a}
                                                                </option>
                                                            </c:otherwise>
                                                        </c:choose>


                                                    </c:forEach>

                                                </select>
                                            </div>
                                        </div>

                                        <br>
                                        <br>
                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos" class=" btn btn-info btn-lg"/>

                                            </div>
                                        </div>



                                    </fieldset>
                                </c:forEach>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>
