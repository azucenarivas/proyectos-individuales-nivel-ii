<%-- 
    Document   : mostrar
    Created on : 11-25-2019, 11:33:19 AM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
          <jsp:include page="/menu_encabezado.jsp"/>
          <br>
        
        <div class="container">
            <div class="row">
                
                 <div class="col-md-12">
                    <div class="col m12" align="center">
                        <h3>
                            <u  class="text-default"><p>Asignar Asesor  a una Empresa</p> </u>
                        </h3>

                    </div>
                </div>
                <br>
                
                

                <div class="col m12" align="left">

                    <a href="AsesorEmpresaServlet?action=nuevo" class="btn btn-info"><i class="fa fa-pencil-square-o" style="color:#757575; font-size: 20px" style="color:red" > </i>
                     
                        Agregar un Nuevo Registro
                    </a>
                 
                    </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead bgcolor="#eceff1">
                        <tr class="info">
                            <th bgcolor="#fafafa" scope="col">Codigo de Asesor de Empresa</th>
                            <th scope="col" class="text-BLACK">Nombre de la Empresa</th>
                            <th scope="col" class="text-BLACK">Nombre del Asesor</th>
                            <th scope="col" ></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>

                    <c:forEach items="${asesorempresa}" var="a">
                         <tr>
                            <td>${a.codigo_asesor_empresa}</td>
                            <td>${a.nombre_e}</td>
                            <td>${a.nombre_a}</td>
                            <td>
                                <a href="AsesorEmpresaServlet?action=eliminar&id=${a.codigo_asesor_empresa}" class="btn btn-danger"> <i class="fa fa-remove" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i> Eliminar</a>
                            </td>
                            <td>
                                <a href="AsesorEmpresaServlet?action=editar&id=${a.codigo_asesor_empresa}" class="btn btn-info"> <i class="fa  fa-spinner" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i> Actualizar</a>
                            </td>
                        </tr>
                    </c:forEach>
                    
                   

                </table>

            </div>

        </div>

    </body>
</html>
