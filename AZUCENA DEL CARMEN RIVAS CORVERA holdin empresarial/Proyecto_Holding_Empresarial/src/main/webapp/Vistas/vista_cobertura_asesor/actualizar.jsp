<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>

    <body>
        <jsp:include page="/menu_encabezado.jsp"/>

        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        <center> <form class="form-horizontal" method="post" action="Cobertura_asesorServlet">
                                <c:forEach items="${cobertura_asesor}" var="cobertura_asesor">

                                    <fieldset>
                                        <legend class="text-center header">

                                            <h3>
                                                <u  class="text-default" class="brown-text"><p>Actualizar registro de  Asignacion de Coberturas de Asesores</p> </u>
                                            </h3>
                                        </legend>

                                        <br>

                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Seleccione una Area de Cobertura:<i class="fa fa-user bigicon"></i></span>
                                            <div class="col-md-8">
                                                <input type="hidden" name="txtID" value="${cobertura_asesor.codigo_conbertura_asesor}"/>
                                                <select name="cmbarea" class="form-control">
                                                    <c:forEach items="${areas}" var="a">
                                                        <c:choose>
                                                            <c:when test="${a.codigo_areas == cobertura_asesor.codigo_areas}">
                                                                <option value="${a.codigo_areas}" selected=""> ${a.nombre_area}
                                                                </option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="${a.codigo_areas}"> ${a.nombre_area}
                                                                </option>
                                                            </c:otherwise>
                                                        </c:choose>

                                                    </c:forEach>


                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Seleccione el Asesor:<i class="fa fa-user bigicon"></i></span>
                                            <div class="col-md-8">

                                                <select name="cmbasesor" class="form-control">

                                                    <c:forEach items="${asesor}" var="a">

                                                        <c:choose>
                                                            <c:when test="${cobertura_asesor.codigo_asesor==a.codigo_asesor}">


                                                                <option value="${a.codigo_asesor}" selected=""> ${a.nombre_a}
                                                                </option>
                                                            </c:when>
                                                            <c:otherwise>

                                                                <option value="${a.codigo_asesor}"> ${a.nombre_a}
                                                                </option>
                                                            </c:otherwise>
                                                        </c:choose>


                                                    </c:forEach>

                                                </select>
                                            </div>
                                        </div>

                                        <br>
                                        <br>
                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos" class=" btn btn-info btn-lg"/>

                                            </div>
                                        </div>



                                    </fieldset>
                                </c:forEach>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
