<%-- 
    Document   : vista_mostrar
    Created on : 11-13-2019, 02:30:23 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="/menu_encabezado.jsp"/>
        <div class="container">
            <br>
            <div class="row">

                <div class="col-md-12">
                    <div class="col m12" align="center">
                        <h3>
                            <u  class="text-default"><p> Detalle de las Coberturas</p> </u>
                        </h3>

                    </div>
                </div>
                <br>

               
            </div>
 <br>
            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead bgcolor="#eceff1">
                        <tr class="info">
                            <th bgcolor="#fafafa" scope="col">Codigo de Cobertura</th>
                            <th scope="col" class="text-BLACK">Nombre del Asesor</th>
                            <th scope="col" class="text-BLACK">Nombre de la Area</th>
                            <th scope="col" class="text-BLACK">Descripción de la Area</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${cobertura_asesor}" var="c">
                            <tr>
                                <td>${c.codigo_conbertura_asesor}</td>
                                <td>${c.nombre_a}</td>
                                <td>${c.nombre_area}</td>
                                <td>${c.descripcion_area}</td>

                            </tr>

                        </c:forEach>
                    </tbody>
                </table>

            </div>

        </div>


    </body>
</html>
