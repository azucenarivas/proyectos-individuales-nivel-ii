<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

    </head>

    <body>
        <jsp:include page="/menu_encabezado.jsp"/>
        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        <center> <form class="form-horizontal" method="post" action="Vendedores_empresaServlet">

                                <fieldset>
                                    <legend class="text-center header">

                                        <h3>
                                            <u  class="text-default" class="brown-text"><p>Registro de Contratación de Vendedores</p> </u>
                                        </h3>
                                    </legend>

                                    <br>



                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Seleccione Fecha de Contratacion:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtfecha" name="txtfecha" type="date" placeholder="Ingrese un Nombre" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione el Vendedor:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <select name="cmbvendedor" class="form-control">
                                                <c:forEach items="${vendedor}" var="v">

                                                    <option value="${v.codigo_vendedor}" selected=""> ${v.nombre_v}
                                                    </option>
                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione una Empresa:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <select name="cmbempresa" class="form-control">
                                                <c:forEach items="${empresas}" var="em">

                                                    <option value="${em.codigo_empresa}" selected=""> ${em.nombre_e}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>

                                    
                                    <br>

                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <input type="submit" name="btnsave" value="Guardar Datos" class=" btn btn-info btn-lg"/>

                                        </div>
                                    </div>



                                </fieldset>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
