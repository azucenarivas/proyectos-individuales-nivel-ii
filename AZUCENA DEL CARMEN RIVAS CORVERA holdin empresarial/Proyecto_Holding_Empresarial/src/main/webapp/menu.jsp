<%-- 
    Document   : menu
    Created on : 11-05-2019, 11:30:18 AM
    Author     : azucena.rivasusam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>


    </head>

    <body>
        <jsp:include page="menu_encabezado.jsp"/>


        <div class="row">
            <div class="col m12">
                <div class="card black white-text center-align z-depth-5">
                    
                    <FONT FACE="Impact" >
                    <marquee BGCOLOR="#eceff1 "><h2>BIENVENIDOS/AS HOLDING EMPRESARIAL </h2></marquee>
                    </FONT>


                </div>
            </div>

        </div>


        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="https://www.ctisoluciones.com/sites/default/files/inline-images/4-2-0-CSO-empresas-istockphoto-884650780-1024x1024_0.jpg" width="300" height="600" class="d-block w-100" alt="Vendedores">
                </div>
                <div class="carousel-item">
                    <img src="https://elceo.com/wp-content/uploads/2019/10/empresas_publicas_getty.jpg" width="300" height="600" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="https://d500.epimg.net/cincodias/imagenes/2019/03/08/pyme/1552070551_396352_1552070885_noticia_normal_recorte1.jpg" width="300" height="600" class="d-block w-100" alt="...">
                </div>

                <div class="carousel-item">
                    <img src="https://nersasl.com/wp-content/uploads/2016/06/empresas.png" width="200" height="600" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </body>
</html>
