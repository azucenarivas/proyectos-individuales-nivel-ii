<%-- 
    Document   : menu_encabezado
    Created on : 11-18-2019, 04:34:36 PM
    Author     : azucena.rivasusam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>


        <nav class="navbar navbar-expand-lg"  style="background-color:#267A8D;" role="navigation">


            <center> <a class="navbar-brand text-light" href="index.jsp"  >HOLDING EMPRESARIAL </a></center>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Administrar Empresa
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="EmpresaServlet?action=empresa">Registro de Empresa</a>                           
                            <a class="dropdown-item" href="Areas_cubiertasServlet?action=lista_areas">Ver Todas las Areas cubiertas</a>



                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Contratación
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="VendedorServlet?action=vendedor">Registro de Vendedores</a>
                            <a class="dropdown-item" href="Vendedores_empresaServlet?action=vendedor_empresa">Registro de Contratacion de Vendedor</a>
                            <!--<a class="dropdown-item" href="Vendedores_empresaServlet?action=ver_vendedores">Ver todas las Contratacion de Vendedor</a>
                            -->
                        </div>
                    </li>



                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  >
                            Servicios
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="AreasServlet?action=area">Registro de Areas</a>
                            <a class="dropdown-item" href="Areas_cubiertasServlet?action=area_cubiertas">Asignación de una Areas</a>
                            <a class="dropdown-item" href="Areas_cubiertasServlet?action=lista">Ver Todas las Areas cubiertas</a>

                        </div>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Cobertura
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="AsesorServlet?action=asesor">Registro de Asesor</a>
                            <a class="dropdown-item" href="Cobertura_asesorServlet?action=cobertura_asesor">Asignacion de Cobertura para Asesores</a>
                            <a class="dropdown-item" href="AsesorEmpresaServlet?action=asesorEmpresa">Asignacion de asesores a Empresas</a>
                            <a class="dropdown-item" href="Cobertura_asesorServlet?action=listado_cobertura">Listado de todas las Cobertura</a>

                            <!--<a class="dropdown-item" href="Asesor_empresaServlet?action=asesor_empresa">Asignar un Asesor a una Empresa</a>
                            -->

                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Localización

                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="PaisServlet?action=pais">Registro de Pais</a>
                            <a class="dropdown-item" href="SedeServlet?action=sede">Registro de Sede</a>
                            

                        </div>
                    </li>

                </ul>
                <!--<form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>-->
            </div>
        </nav>

    </body>

</html>
