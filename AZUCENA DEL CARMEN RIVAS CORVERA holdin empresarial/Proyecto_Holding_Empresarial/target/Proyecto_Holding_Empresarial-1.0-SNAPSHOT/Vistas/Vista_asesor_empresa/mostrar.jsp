<%-- 
    Document   : vista_mostrar
    Created on : 11-13-2019, 02:30:23 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!--<link href="Resources/css/materialize.css" rel="stylesheet" type="text/css"/>-->
    </head>
    <body>
         <jsp:include page="/menu_encabezado.jsp"/>
        <br>
        
        <div class="container">
            <div class="row">
                
                 <div class="col-md-12">
                    <div class="col m12" align="center">
                        <h3>
                            <u  class="text-default"><p>Asignar Asesor  a una Empresa</p> </u>
                        </h3>

                    </div>
                </div>
                <br>
                
                

                <div class="col m12" align="left">

                    <a href="Asesor_empresaServlet?action=nuevo" class="btn btn-info">
                        Agregar un Nuevo Registro
                    </a>
                    <!--<a href="regresarServlet?action=regresar" class="btn btn-success black-text z-depth-5 " >Regresar al Menu Principal</a>
                -->
                    </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead bgcolor="#eceff1">
                        <tr class="info">
                            <th bgcolor="#fafafa" scope="col">Codigo de Asesor de Empresa</th>
                            <th scope="col" class="text-BLACK">Nombre de la Empresa</th>
                            <th scope="col" class="text-BLACK">Nombre del Asesor</th>
                            <th scope="col" ></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>

                    <c:forEach items="${asesor_empresa}" var="ae">
                        <tr>
                            <td>${ae.codigo_asesor_empresa}</td>
                            <td>${ae.nombre_e}</td>
                            <td>${ae.nombre_a}</td>
                            <td>
                                <a href="Asesor_empresaServlet?action=eliminar&id=${ae.codigo_asesor_empresa}" class="btn btn-danger">Eliminar</a>
                                <a href="Asesor_empresaServlet?action=editar&id=${ae.codigo_asesor_empresa}" class="btn btn-info">Actualizar</a>
                            </td>
                        </tr>

                    </c:forEach>
                   

                </table>

            </div>

        </div>


    </body>
</html>
