<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>

    <body>
        <jsp:include page="/menu_encabezado.jsp"/>
        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        <center> <form class="form-horizontal" method="post" action="AreasServlet">
                                <c:forEach items="${area}" var="area">
                                    <fieldset>
                                        <legend class="text-center header">

                                            <h3>
                                                <u  class="text-default" class="brown-text"><p>Actualizar el registro de una Area</p> </u>
                                            </h3>
                                        </legend>

                                        <br>



                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-left">Nombre de la Area:<i class="fa fa-user bigicon"></i></span>
                                            <div class="col-md-8">
                                                <input type="hidden" name="txtID" value="${area.codigo_areas}"/>
                                                <input id="txtnombre" name="txtnombre" type="text" placeholder="Ingrese un Nombre" class="form-control" value="${area.nombre_area}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Descripcion de la Area:<i class="fa fa-user bigicon"></i></span>
                                            <div class="col-md-8">
                                                <input id="txtfecha" name="txtdescripcion" type="text" placeholder="Ingrese una descripcion " class="form-control" value="${area.descripcion_area}">
                                            </div>
                                        </div>






                                        <br>

                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos" class=" btn btn-info btn-lg"/>

                                            </div>
                                        </div>



                                    </fieldset>
                                </c:forEach>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
