<%-- 
    Document   : vista_mostrar
    Created on : 11-13-2019, 02:30:23 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
           <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <jsp:include page="/menu_encabezado.jsp"/>
        <br>

        <div class="container">
            <div class="row">


                <div class="col-md-12">
                    <div class="col m12" align="center">
                        <h3>
                            <u  class="text-default"><p>Asesores Registrados </p> </u>
                        </h3>

                    </div>
                </div>
                <br>

                <div class="col m6" align="left">

                    <a href="AsesorServlet?action=nuevo" class="btn btn-info"><i class="fa fa-pencil-square-o" style="color:#757575; font-size: 20px" style="color:red" > </i>
                        Agregar un Nuevo Registro
                    </a>
                    <!--<a href="regresarServlet?action=regresar" class="btn btn-success black-text z-depth-5 " >Regresar al Menu Principal</a>-->
                </div>
            </div>


            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead bgcolor="#eceff1">
                        <tr class="info">
                            <th bgcolor="#fafafa" scope="col">Codigo del Asesor</th>
                            <th scope="col" class="text-BLACK">Nombre del Asesor</th>
                            <th scope="col" class="text-BLACK">Direccion del Asesor</th>
                            <th scope="col" class="text-BLACK">Titulacion del Asesor</th>
                            <th scope="col" ></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${asesor}" var="a">
                            <tr>
                                <td>${a.codigo_asesor}</td>
                                <td>${a.nombre_a}</td>
                                <td>${a.direccion_a}</td>
                                <td>${a.titulacion}</td>
                                <td>
                                    <a href="AsesorServlet?action=eliminar&id=${a.codigo_asesor}"  class="btn btn-danger"> <i class="fa fa-remove" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i>Eliminar</a>
                                </td>
                                <td>
                                    <a href="AsesorServlet?action=editar&id=${a.codigo_asesor}" class="btn btn-info"> <i class="fa  fa-spinner" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i> Actualizar</a>
                                </td>
                            </tr>

                        </c:forEach>
                    </tbody>
                </table>

            </div>

        </div>


    </body>
</html>
