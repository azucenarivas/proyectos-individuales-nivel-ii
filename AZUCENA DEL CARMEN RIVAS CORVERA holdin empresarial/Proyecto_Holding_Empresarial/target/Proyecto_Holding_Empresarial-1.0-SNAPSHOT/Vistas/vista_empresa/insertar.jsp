<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- <link href="Resources/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="Resources/Bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>-->
    </head>

    <body>
        <jsp:include page="/menu_encabezado.jsp"/>




        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        <center> <form class="form-horizontal" method="post" action="EmpresaServlet">

                                <fieldset>
                                    <legend class="text-center header">

                                        <h3>
                                            <u  class="text-default" class="brown-text"><p>Registro de Nueva Empresa</p> </u>
                                        </h3>
                                    </legend>

                                    <br>
                                    


                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Nombre de la Empresa:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtnombre" name="txtnombre" type="text" placeholder="Ingrese un Nombre" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Fecha Entrada al Holding:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtfecha" name="txtfecha" type="date" placeholder="Ingrese una Fecha" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Facturacion Anual:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtfacturacion" name="txtfacturacion" type="text"  placeholder="Ingrese un dato" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Numeros de Vendedores:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtvendedores" name="txtvendedores" type="number"  placeholder="Ingrese un dato" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione una Ciuadad:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">

                                            <select name="cmbsede" class="form-control">

                                                <c:forEach items="${sede}" var="s">

                                                    <option value="${s.codigo_sede}" selected=""> ${s.ciudad}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione una Ciuadad:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">

                                            <select name="cmbpais" class="form-control">
                                                <c:forEach items="${pais}" var="p">

                                                    <option value="${p.codigo_pais}" selected=""> ${p.nombre_p}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <input type="submit" name="btnsave" value="Guardar Datos" class=" btn btn-info btn-lg"/>

                                        </div>
                                    </div>



                                </fieldset>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
