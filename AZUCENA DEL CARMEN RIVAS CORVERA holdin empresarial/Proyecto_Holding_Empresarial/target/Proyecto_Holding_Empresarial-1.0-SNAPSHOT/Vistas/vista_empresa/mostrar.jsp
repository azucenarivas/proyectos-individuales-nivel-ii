<%--
    Document   : vista_mostrar
    Created on : 11-13-2019, 02:30:23 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <jsp:include page="/menu_encabezado.jsp"/>

        <div class="container">
            <br>
            <div class="row">

                <div class="col-md-12">
                    <div class="col m-12" align="center">
                        <h3>
                            <u  class="text-default"><p>Empresas Registradas</p> </u>
                        </h3>

                    </div>
                </div>
                <br>
                <div class="col m6" align="left">
                    <br>
                    <a href="EmpresaServlet?action=nuevo" class="btn btn-info" ><i class="fa fa-pencil-square-o" style="color:#757575; font-size: 20px" style="color:red" >
                        </i>  
                        Agregar un Nuevo Registro
                    </a>


                    <!--
                   <a href="regresarServlet?action=regresar" class="btn btn-success black-text z-depth-5 " >Regresar al Menu Principal</a>-->
                </div>


            </div>

            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead bgcolor="#eceff1">
                        <tr class="info">
                            <th bgcolor="#fafafa" scope="col">Codigo</th>
                            <th scope="col" class="text-BLACK">Empresa</th>
                            <th scope="col" class="text-BLACK">F.Inscripción</th>
                            <th scope="col" class="text-BLACK">Facturacion</th>
                            <th scope="col"  class="text-BLACK">#Vendedores</th>
                            <th scope="col"  class="text-BLACK">Ciudad(Sede)</th>
                            <th scope="col"  class="text-BLACK">Pais</th>
                            <th scope="col" ></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${empresas}" var="e">
                            <tr>
                                <td>${e.codigo_empresa}</td>
                                <td style="width:150px">${e.nombre_e}</td>
                                <td>${e.fecha_entrada_holdind}</td>
                                <td>${e.facturacion_anual}</td>
                                <td>${e.numero_vendedores}</td>
                                <td style="width:150px">${e.ciudad}</td>
                                <td>${e.nombre_p}</td>
                                <td style="width:150px"> 
                                    <a href="EmpresaServlet?action=eliminar&id=${e.codigo_empresa}" class="btn btn-danger"><i class="fa fa-remove" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i> Eliminar</a>

                                </td>

                                <td style="width:150px"> 
                                    <a href="EmpresaServlet?action=actualizar&id=${e.codigo_empresa}" class="btn btn-info"><i class="fa  fa-spinner" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i> Actualizar</a>
                                </td>
                            </tr>

                        </c:forEach>
                    </tbody>
            </div>
        </table>


    </div>

</div>


</body>
</html>
