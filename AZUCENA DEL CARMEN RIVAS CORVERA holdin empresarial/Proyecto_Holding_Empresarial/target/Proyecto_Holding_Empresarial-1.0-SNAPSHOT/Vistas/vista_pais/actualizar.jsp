<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>

    <jsp:include page="/menu_encabezado.jsp"/>

    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm"> 
                    <center><form class="form-horizontal" method="post" action="PaisServlet">
                            <c:forEach items="${pais}" var="pa">

                                <fieldset>
                                    <legend class="text-center header">

                                        <h3>
                                            <u  class="text-default" class="brown-text"><p>Actualización del Registro de un Pais</p> </u>
                                        </h3>
                                    </legend>

                                    <br>


                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Ingrese el Nombre del Pais:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input type="hidden" name="txtID" value="${pa.codigo_pais}"/>
                                            <input id="txtnombre" name="txtnombre_p" type="text" placeholder="Ingrese un Nombre" class="form-control" value="${pa.nombre_p}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Ingrese el PIB del Pais:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtnombre" name="txtPIB" type="text" placeholder="Ingrese un pais" class="form-control" value="${pa.PIB}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Ingrese el numero de Habitantes:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtnombre" name="txthabitantes" type="text" placeholder="Ingrese un Numero" class="form-control" value="${pa.numero_habitantes}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Ingrese la Capital del Pais:<i class="fa fa-user bigicon"></i></span>
                                        <div class="col-md-8">
                                            <input id="txtnombre" name="txtcapital" type="text" placeholder="Ingrese la capital" class="form-control" value="${pa.capital}">
                                        </div>
                                    </div>


                                    <br>

                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <input type="submit" name="btnsave" value="Guardar Datos" class=" btn btn-info btn-lg"/>

                                        </div>
                                    </div>



                                </fieldset>

                            </c:forEach>
                        </form>

                    </center>
                </div>
            </div>
        </div>
    </div>
</html>
