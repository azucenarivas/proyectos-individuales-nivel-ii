<%-- 
    Document   : vista_mostrar
    Created on : 11-13-2019, 02:30:23 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
           <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <jsp:include page="/menu_encabezado.jsp"/>
        <br>
        <div class="container">
            <div class="row">


                <div class="col-md-12">
                    <div class="col m12" align="center">
                        <h3>
                            <u  class="text-default"><p>Vendedores contratados</p> </u>
                        </h3>

                    </div>
                </div>
                <br>

                <div class="col m12" align="left">

                    <a href="Vendedores_empresaServlet?action=nuevo" class="btn btn-info" ><i class="fa fa-pencil-square-o" style="color:#757575; font-size: 20px" style="color:red" > </i>
                        Agregar un Nuevo Registro
                    </a>
                    <!--<a href="regresarServlet?action=regresar" class="btn btn-success black-text z-depth-5 " >Regresar al Menu Principal</a>-->
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead bgcolor="#eceff1">
                        <tr class="info">
                            <th bgcolor="#fafafa" scope="col">Codigo del Vendedor_Empresa</th>
                            <th scope="col" class="text-BLACK">Fecha de Contratacion</th>
                            <th scope="col" class="text-BLACK">Nombre del Vendedor</th>
                            <th scope="col" class="text-BLACK">Empresa</th>
                            <th scope="col" ></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>

                    <c:forEach items="${vendedores}" var="v">
                        <tr>
                            <td>${v.codigo_vendedor_empresa}</td>
                            <td>${v.fecha_contratacion}</td>
                            <td>${v.nombre_v}</td>
                            <td>${v.nombre_e}</td>
                            <td>
                                <a href="Vendedores_empresaServlet?action=eliminar&id=${v.codigo_vendedor_empresa}" class="btn btn-danger"> <i class="fa fa-remove" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i>Eliminar</a>
                            </td>
                            <td>
                                <a href="Vendedores_empresaServlet?action=editar&id=${v.codigo_vendedor_empresa}" class="btn btn-info"> <i class="fa  fa-spinner" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i> Actualizar</a>
                            </td>
                        </tr>

                    </c:forEach>

                </table>

            </div>

        </div>


    </body>
</html>
