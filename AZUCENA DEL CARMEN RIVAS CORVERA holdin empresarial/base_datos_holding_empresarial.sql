create database if not exists holding_empresarial;
use holding_empresarial;

create table if not exists vendedor(
codigo_vendedor int not null primary key auto_increment,
nombre_v varchar(100) not null,
direccion_v varchar(100) not null
)engine InnoDB;

create table if not exists pais(
codigo_pais int not null primary key auto_increment,
nombre_p varchar(100) not null,
PIB decimal(10,2) not null,
numero_habitantes int not null,
capital varchar(100) not null
)engine InnoDB;

create table if not exists sede(
codigo_sede int not null primary key auto_increment,
ciudad varchar(100) not null
)engine InnoDB;

create table if not exists empresa(
codigo_empresa int not null primary key auto_increment,
nombre_e varchar(100) not null,
fecha_entrada_holdind date not null,
facturacion_anual varchar(100) not null,
numero_vendedores int,
codigo_sede int not null,
codigo_pais int not null,
constraint fk_sede7 foreign key(codigo_sede) references sede(codigo_sede) on delete cascade on update cascade,
constraint fk_pais7 foreign key(codigo_pais) references pais(codigo_pais) on delete cascade on update cascade
)engine InnoDB;


create table if not exists vendedores_empresa(
codigo_vendedor_empresa int not null primary key auto_increment,
fecha_contratacion date not null,
codigo_vendedor int not null,
codigo_empresa int not null ,
constraint fk_vendedor foreign key(codigo_vendedor) references vendedor(codigo_vendedor) on delete cascade on update cascade,
constraint fk_empresa foreign key(codigo_empresa) references empresa(codigo_empresa) on delete cascade on update cascade
)engine InnoDB;

create table if not exists asesor(
codigo_asesor int not null primary key auto_increment,
nombre_a varchar(100) not null,
direccion_a varchar(100) not null,
titulacion varchar(100) not null
)engine InnoDB;


create table if not exists areas(
codigo_areas int not null primary key auto_increment,
nombre_area varchar(100) not null,
descripcion_area varchar(100) not null
)engine InnoDB;


create table if not exists areas_cubiertas(
codigo_areas_cubiertas int not null primary key auto_increment,
codigo_empresa int not null,
codigo_areas int not null,
constraint fk_empresa3 foreign key(codigo_empresa) references empresa(codigo_empresa) on delete cascade on update cascade,
constraint fk_areas foreign key(codigo_areas) references areas(codigo_areas) on delete cascade on update cascade
)engine InnoDB;

create table if not exists asesor_empresa(
codigo_asesor_empresa int not null primary key auto_increment,
codigo_empresa int not null,
codigo_asesor int not null ,
constraint fk_empresa7 foreign key(codigo_empresa) references empresa(codigo_empresa) on delete cascade on update cascade,
constraint fk_asesor7 foreign key(codigo_asesor) references asesor(codigo_asesor) on delete cascade on update cascade
)engine InnoDB;

create table if not exists cobertura_asesor(
codigo_conbertura_asesor int not null primary key auto_increment,
codigo_areas int not null,
codigo_asesor int not null,
constraint fk_areas3 foreign key(codigo_areas) references areas(codigo_areas) on delete cascade on update cascade,
constraint fk_asesor foreign key(codigo_asesor) references asesor(codigo_asesor) on delete cascade on update cascade
)engine InnoDB;


insert into vendedor(nombre_v,direccion_v)values('Azucena Rivas','San Salvador'),('Elmer Flores','San Vicente'),('Daniel Rivas','San Salvador');
insert into vendedor(nombre_v,direccion_v)values('Cristhian','San Salvador');
insert into sede(ciudad)values('Cojutepeque'),('Ciudad de Honduras'),('Managua');
insert into pais(nombre_p,PIB,numero_habitantes,capital)values('El Salvador','24.81','100000000','San Salvador'),('Honduras','34.81','100000000','Santa Ana'),('Nicaragua','24.81','100000000','San Miguel');
insert into empresa(nombre_e,fecha_entrada_holdind,facturacion_anual,numero_vendedores,codigo_sede,codigo_pais)values('Coca cola','2019-11-11','m00102','100',1,1),('Pepsi','2019-11-11','v00102','200',2,2),('Salvacola','2019-11-11','p00102','300',3,3);
insert into vendedores_empresa(fecha_contratacion,codigo_vendedor,codigo_empresa)values('2019-11-12',1,1),('2017-11-12',2,2),('2016-11-12',3,3);
insert into asesor(nombre_a,direccion_a,titulacion)values('Juan Cruz','San salvador','Ventas'),('Ana Lopez','Santa Ana','Mercadotecnia'),('Santiago Paz','Chile','Diseñador Grafico');
insert into areas(nombre_area,descripcion_area)values('Produccion 1','Zona 1'),('Produccion 2','Zona 2'),('Produccion 3','Zona 3');
insert into areas_cubiertas(codigo_empresa,codigo_areas)values(1,1),(2,2),(3,3);
insert into cobertura_asesor(codigo_areas,codigo_asesor)values(1,1),(2,2),(3,3);
insert into asesor_empresa(codigo_empresa,codigo_asesor)values(1,1),(2,2),(3,3);



select *from vendedor;

select *from empresa ;

select  count(*) from empresa where codigo_pais = 1;


/*inner join para empresa*/
select e.codigo_empresa,e.nombre_e,e.fecha_entrada_holdind,e.facturacion_anual,e.numero_vendedores,s.ciudad,p.nombre_p
from empresa e
inner join sede s on s.codigo_sede=e.codigo_sede
inner join pais p on p.codigo_pais=e.codigo_pais;


/*listado de tdas las empresas*/
select ar.codigo_areas_cubiertas,e.nombre_e,p.nombre_p,s.ciudad,are.nombre_area,are.descripcion_area,e.numero_vendedores,e.fecha_entrada_holdind
from areas_cubiertas ar
inner join empresa e on e.codigo_empresa=ar.codigo_empresa
inner join pais p on p.codigo_pais=e.codigo_pais
inner join sede s on s.codigo_sede=e.codigo_sede
inner join areas are on are.codigo_areas=ar.codigo_areas;


/*listado de todos los vendedores de la empresa*/
select ve.codigo_vendedor_empresa,v.nombre_v,v.direccion_v,e.nombre_e
from vendedores_empresa ve
inner join vendedor v on v.codigo_vendedor=ve.codigo_vendedor
inner join empresa e on e.codigo_empresa=ve.codigo_empresa;



/*listado de todas areas cubiertas*/
select ac.codigo_areas_cubiertas,e.nombre_e,a.nombre_area,a.descripcion_area
from areas_cubiertas ac
inner join empresa e on e.codigo_empresa=ac.codigo_empresa
inner join areas a on a.codigo_areas=ac.codigo_areas;


/*Listado de todas las coberturas de todos los asesores*/
select co.codigo_conbertura_asesor,ase.nombre_a,a.nombre_area,a.descripcion_area
from cobertura_asesor co
inner join areas a on a.codigo_areas=co.codigo_areas
inner join asesor ase on ase.codigo_asesor=co.codigo_asesor;

select asesor.codigo_asesor,asesor.nombre_a AS Nombre_Empleado,cobertura_asesor.codigo_conbertura_asesor,cobertura_asesor.codigo_areas,cobertura_asesor.codigo_asesor,areas.codigo_areas,areas.nombre_area,areas_cubiertas.codigo_areas_cubiertas,areas_cubiertas.codigo_empresa,areas_cubiertas.codigo_areas,empresa.codigo_empresa,empresa.nombre_e FROM asesor,cobertura_asesor,areas,areas_cubiertas,empresa
WHERE asesor.codigo_asesor = 1 AND cobertura_asesor.codigo_conbertura_asesor = 1 AND cobertura_asesor.codigo_areas = 1 AND cobertura_asesor.codigo_asesor = 1 AND areas.codigo_areas = 1 AND areas_cubiertas.codigo_areas_cubiertas = 1 AND areas_cubiertas.codigo_empresa = 1 AND areas_cubiertas.codigo_areas = 1 AND empresa.codigo_empresa = 1;

/*inner join para vendedrores_empresa*/
select ve.codigo_vendedor_empresa,ve.fecha_contratacion,v.nombre_v,e.nombre_e
from vendedores_empresa ve
inner join vendedor v on v.codigo_vendedor=ve.codigo_vendedor
inner join empresa e on e.codigo_empresa=ve.codigo_empresa;



select *from pais;

select *from usuario;





select *from sede;


select *from asesor;


select *from areas;

/*inner join para areas_cubiertas*/

select a.codigo_areas_cubiertas,e.nombre_e,c.nombre_area
from areas_cubiertas a
inner join empresa e on e.codigo_empresa=a.codigo_empresa
inner join areas c on c.codigo_areas=a.codigo_areas;


/*inner join para coberturas_asesor*/

select ca.codigo_conbertura_asesor,c.nombre_area,a.nombre_a
from cobertura_asesor ca
inner join areas c on c.codigo_areas=ca.codigo_areas
inner join asesor a on a.codigo_asesor=ca.codigo_asesor;


/*inner join para asesor_empresa*/

select ae.codigo_asesor_empresa,em.nombre_e,a.nombre_a
from asesor_empresa ae
inner join empresa em on em.codigo_empresa=ae.codigo_empresa
inner join asesor a on a.codigo_asesor=ae.codigo_asesor;

select *from asesor_empresa;

update asesor_empresa set codigo_empresa=2,codigo_asesor=2 where codigo_asesor_empresa=5;


select *from areas_cubiertas where codigo_areas_cubiertas = 1;