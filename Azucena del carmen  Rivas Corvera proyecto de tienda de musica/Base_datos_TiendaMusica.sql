create database if not exists tienda_musica;
use tienda_musica;



create table if not exists persona(
codigo_persona int not null primary key auto_increment,
nombre varchar(100) not null,
apellido varchar(100) not null,
direccion varchar(100) not null
)engine InnoDB;



create table if not exists cargo(
codigo_cargo int not null primary key auto_increment,
cargo varchar(100) not null
)engine InnoDB;


create table if not exists usuario(
codigo_persona int not null primary key,
codigo_cargo int not null,
usuario varchar(25) not null,
contrasenia varchar(25) not null,
constraint fk_personal foreign key(codigo_persona) references persona(codigo_persona) on delete cascade on update cascade,
constraint fk_cargos foreign key(codigo_cargo) references cargo(codigo_cargo) on delete cascade on update cascade
)engine InnoDB;



create table if not exists promotor(
codigo_promotor int not null primary key auto_increment,
nombre_p varchar(100) not null,
direccion_p varchar(100) not null
)engine InnoDB;

create table if not exists estilo_musical(
codigo_estilo_musical int not null primary key auto_increment,
tipo varchar(100) not null
)engine InnoDB;




create table if not exists pais(
codigo_pais int not null primary key auto_increment,
nombre varchar(100) not null
)engine InnoDB;


create table if not exists grupo(
codigo_grupo int not null primary key auto_increment,
cantidad_personas int not null,
nombre varchar(100) not null
)engine InnoDB;


create table if not exists compañia(
codigo_compañia int not null primary key auto_increment,
nombre_c varchar(100) not null,
direccion_c varchar(100) not null,
codigo_pais int not null,
constraint fk_pais foreign key (codigo_pais) references pais(codigo_pais) on delete cascade on update  cascade
)engine InnoDB;

create table if not exists cd(
codigo_cd int not null primary key auto_increment,
nombre_cd varchar(100) not null,
clave varchar(100) not null,
precio_v decimal(10,2) not null
)engine InnoDB;


create table if not exists coleccion(
codigo_coleccion int not null primary key auto_increment,
codigo_cd int not null,
precio_venta decimal(10,2) not null,
codigo_promotor int not null ,
codigo_compañia int not null,
constraint fk_cd foreign key (codigo_cd) references cd(codigo_cd) on delete cascade on update  cascade,
constraint fk_cpromotor foreign key (codigo_promotor) references promotor(codigo_promotor) on delete cascade on update  cascade,
constraint fk_compañia foreign key (codigo_compañia) references compañia(codigo_compañia) on delete cascade on update  cascade
)engine InnoDB;


create table if not exists ediciones(
codigo_ediciones int not null primary key auto_increment,
fecha_e date not null,
detalle varchar(100),
codigo_compañia int not null,
constraint fk_compañia2 foreign key (codigo_compañia) references compañia(codigo_compañia) on delete cascade on update  cascade
)engine InnoDB;



create table if not exists artista(
codigo_artista int not null primary key auto_increment,
nombre_a varchar(100) not null,
codigo_estilo_musical int not null,
codigo_cd int not null,
codigo_pais int not null,
constraint fk_pais1 foreign key (codigo_pais) references pais(codigo_pais) on delete cascade on update  cascade,
constraint fk_estilo_musical1 foreign key (codigo_estilo_musical) references estilo_musical(codigo_estilo_musical) on delete cascade on update  cascade,
constraint fk_cd1 foreign key (codigo_cd) references cd(codigo_cd) on delete cascade on update  cascade
)engine InnoDB;


create table if not exists pistas(
codigo_pistas int not null primary key auto_increment,
numero_pista int not null,
titulo varchar(100) not null,
duracion varchar(100) not null,
codigo_cd int not null,
constraint fk_cd2 foreign key (codigo_cd) references cd(codigo_cd) on delete cascade on update  cascade
)engine InnoDB;


create table if not exists grupo_artista(
codigo_grupo_artista int not null primary key auto_increment,
codigo_artista int not null,
codigo_grupo int not null,
constraint fk_artista1 foreign key (codigo_artista) references artista(codigo_artista) on delete cascade on update  cascade,
constraint fk_grupo1 foreign key (codigo_grupo) references grupo(codigo_grupo) on delete cascade on update  cascade
)engine InnoDB;


insert into promotor(nombre_p,direccion_p)values('Juan Daniel Rivas','SAn Vicente'),('Juana de La Cruz','SAnta Ana'),('Ana del Carmen Cruz','SAn Salvador');
insert into estilo_musical(tipo)values('Salsa'),('Rock'),('Flamenco');
insert into grupo(cantidad_personas,nombre)values('2','Jesse y Joy'),('3','Jonas Brothers'),('4','Camila');
insert into pais(nombre)values('España'),('Inglaterra'),('Suiza');
insert into compañia(nombre_c,direccion_c,codigo_pais)values(' Los Estable','Calle 27 al poniente de Madrid',1),('Los unicos','Calle 10 al norte de Manjat',2),('Los numeros 1','Calle central al sur',3);
insert into cd(nombre_cd,clave,precio_v)values('MOmentos especiales','001','5'),('Mi luna mi sol','002','15'),('una estrella eres tu','003','25');
insert into coleccion(codigo_cd,codigo_promotor,codigo_compañia,precio_venta)values(1,1,1,20.50),(2,2,2,30.00),(3,3,3,25.99);
insert into ediciones(fecha_e,detalle,codigo_compañia)values('2019-11-11','compra',1),('2019-10-11','cesion',2),('2019-12-09','compra',3);
insert into pistas(numero_pista,titulo,duracion,codigo_cd)values(1,'Los Clasicos','3:00 minutos',1),(2,'Los Fantasmas','3:90 minutos',2),(3,'Los novatos','5:00 minutos',3);
insert into artista(nombre_a,codigo_estilo_musical,codigo_cd,codigo_pais)values('Ana Cristel Lopez',1,1,1),('Fany Isabella Flores',2,2,2),('Kelvin Adonay Aguilar',3,3,3);
insert into grupo_artista(codigo_artista,codigo_grupo)values(1,1),(2,2),(3,3);
insert into persona(nombre,apellido,direccion) values('Azucena','Rivas','San Salvador'),('Ana','Cruz','San Vicente');
insert into cargo(cargo) values('Administrador'),('Usuario');
insert into usuario(codigo_persona,codigo_cargo,usuario,contrasenia) values(1,1,'azucena','123456'),(2,2,'ana','12345');

select *from promotor;
select *from persona;
select *from cargo;
select *from usuario;
select *from cd;
select *from artista;

/*Inner join para la compañia*/

select c.codigo_compañia,c.nombre_c,c.direccion_c,p.nombre
from compañia c
inner join pais p on p.codigo_pais=c.codigo_pais;


/*Inner join para colecciones*/

select c.codigo_coleccion,d.nombre_cd,c.precio_venta,p.nombre_p,co.nombre_c
from coleccion c
inner join cd d on d.codigo_cd=c.codigo_cd
inner join promotor p on p.codigo_promotor=c.codigo_promotor
inner join compañia co on co.codigo_compañia=c.codigo_compañia;

select *from ediciones;

select *from pais;

select *from estilo_musical;

/*Inner join para ediciones*/
select e.codigo_ediciones,e.fecha_e,e.detalle,c.nombre_c,c.direccion_c
from ediciones e
inner join compañia c on c.codigo_compañia=e.codigo_compañia;

/*Inner join para artistas*/

select a.codigo_artista,a.nombre_a,e.tipo,c.nombre_cd,pa.nombre
from artista a
inner join estilo_musical e on e.codigo_estilo_musical=a.codigo_estilo_musical
inner join cd  c on c.codigo_cd=a.codigo_cd
inner join pais pa on pa.codigo_pais=a.codigo_pais;


select *from pistas;

select *from grupo;





/*Inner join para pistas*/

select p.codigo_pistas,p.numero_pista,p.titulo,p.duracion,c.nombre_cd
from pistas p
inner join cd c on c.codigo_cd=p.codigo_cd;



/*Inner join para grupo_artistas*/

select g.codigo_grupo_artista,a.nombre_a,gru.nombre
from grupo_artista g
inner join artista a on a.codigo_artista=g.codigo_artista
inner join grupo gru on gru.codigo_grupo=g.codigo_grupo;


select *from grupo_artista where codigo_grupo_artista=2;
update grupo_artista set codigo_artista=1,codigo_grupo=2 where codigo_grupo_artista=2;