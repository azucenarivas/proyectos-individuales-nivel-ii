/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.ArtistaDao;
import dao.CdDao;
import dao.Estilo_musicalDao;
import dao.PaisDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ArtistaBean;
import modelo.CdBean;
import modelo.Estilo_MusicalBean;
import modelo.PaisBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "ArtistaServlet", urlPatterns = {"/ArtistaServlet"})
public class ArtistaServlet extends HttpServlet {

    Conexion conn = new Conexion();

    ArtistaDao artistadao = new ArtistaDao(conn);
    Estilo_musicalDao estilodao = new Estilo_musicalDao(conn);
    CdDao cddao = new CdDao(conn);
    PaisDao paisdao = new PaisDao(conn);

    ArtistaBean artistabean = new ArtistaBean(0);
    Estilo_MusicalBean estilobean = new Estilo_MusicalBean(0);
    CdBean cdbean = new CdBean(0);
    PaisBean paisbean = new PaisBean(0);

    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("artista")) {
            ruta = "/Vistas/Vista_Artista/mostrar_artista.jsp";

            try {
                request.setAttribute("artista", artistadao.Consultar_Artista());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Artista/insertar_artista.jsp";
            try {
                request.setAttribute("estilo", estilodao.Consultar_EstiloMusical());
                request.setAttribute("cd", cddao.Consultar_Cd());
                request.setAttribute("pais", paisdao.Consultar_Pais());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Artista/actualizar_artista.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("artista", artistadao.consultarById_Artista(idActual));
                request.setAttribute("estilo", estilodao.Consultar_EstiloMusical());
                request.setAttribute("cd", cddao.Consultar_Cd());
                request.setAttribute("pais", paisdao.Consultar_Pais());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Artista/mostrar_artista.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("artista", artistadao.Eliminar(idEliminar));
                request.setAttribute("artista", artistadao.Consultar_Artista());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        artistabean.setNombre_a(request.getParameter("txtnombre_a"));
        estilobean.setCodigo_estilo_musical(Integer.parseInt(request.getParameter("cmbtipo")));
        artistabean.setEstilo_musical(estilobean);
        cdbean.setCodigo_cd(Integer.parseInt(request.getParameter("cmbcd")));
        artistabean.setCd(cdbean);
        paisbean.setCodigo_pais(Integer.parseInt(request.getParameter("cmbpais")));
        artistabean.setPais(paisbean);

        if (ID == null || ID.isEmpty()) {
            try {
                artistadao.insertar(artistabean);
                ruta = "/Vistas/Vista_Artista/mostrar_artista.jsp";
                request.setAttribute("artista", artistadao.Consultar_Artista());
            } catch (Exception e) {
            }
        } else {
            try {

                artistabean.setCodigo_artista(Integer.parseInt(ID));
                artistadao.actualizar(artistabean);
                ruta = "/Vistas/Vista_Artista/mostrar_artista.jsp";
                request.setAttribute("artista", artistadao.Consultar_Artista());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
