/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.CdDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CdBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "CdServlet", urlPatterns = {"/CdServlet"})
public class CdServlet extends HttpServlet {

    Conexion conn = new Conexion();
    CdDao cddao = new CdDao(conn);
    CdBean cdbean = new CdBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("cd")) {
            ruta = "/Vistas/Vista_Cd/mostrar_cd.jsp";

            try {
                request.setAttribute("cd", cddao.Consultar_Cd());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Cd/insertar_cd.jsp";

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Cd/actualizar_cd.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("cd", cddao.consultarById_Cd(idActual));
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Cd/mostrar_cd.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("cd", cddao.Eliminar(idEliminar));
                request.setAttribute("cd", cddao.Consultar_Cd());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");

        cdbean.setNombre_cd(request.getParameter("txtnombre_cd"));
        cdbean.setClave(request.getParameter("txtclave_cd"));
        cdbean.setPrecio_v(Double.parseDouble(request.getParameter("txtprecio_cd")));
         

        if (ID == null || ID.isEmpty()) {
            try {
                cddao.insertar(cdbean);
                ruta = "/Vistas/Vista_Cd/mostrar_cd.jsp";
                request.setAttribute("cd", cddao.Consultar_Cd());
            } catch (Exception e) {
            }
        } else {
            try {

                cdbean.setCodigo_cd(Integer.parseInt(ID));
                cddao.actualizar(cdbean);
                ruta = "/Vistas/Vista_Cd/mostrar_cd.jsp";
                request.setAttribute("cd", cddao.Consultar_Cd());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
