/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.CdDao;
import dao.ColeccionDao;
import dao.CompaniaDao;
import dao.PistasDao;
import dao.PromotorDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CdBean;
import modelo.ColeccionBean;
import modelo.CompaniaBean;
import modelo.PromotorBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "ColeccionServlet", urlPatterns = {"/ColeccionServlet"})
public class ColeccionServlet extends HttpServlet {

    Conexion conn = new Conexion();

    ColeccionDao colecciondao = new ColeccionDao(conn);
    CdDao cddao = new CdDao(conn);
    PromotorDao promotordao = new PromotorDao(conn);
    CompaniaDao companiadao = new CompaniaDao(conn);

    ColeccionBean coleccionbean = new ColeccionBean(0);
    CdBean cdbean = new CdBean(0);
    PromotorBean promotorbean = new PromotorBean(0);
    CompaniaBean companiabean = new CompaniaBean(0);

    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("coleccion")) {

            ruta = "/Vistas/Vista_Coleccion/mostrar_coleccion.jsp";

            try {
                request.setAttribute("coleccion", colecciondao.Consultar_Coleccion());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Coleccion/insertar_coleccion.jsp";
            try {

                request.setAttribute("cd", cddao.Consultar_Cd());
                request.setAttribute("promotor", promotordao.Consultar_Promotores());
                request.setAttribute("compania", companiadao.Consultar_Compañia());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Coleccion/actualizar_coleccion.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("coleccion", colecciondao.consultarById_Coleccion(idActual));
                request.setAttribute("cd", cddao.Consultar_Cd());
                request.setAttribute("promotor", promotordao.Consultar_Promotores());
                request.setAttribute("compania", companiadao.Consultar_Compañia());

            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Coleccion/mostrar_coleccion.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("coleccion", colecciondao.Eliminar(idEliminar));
                request.setAttribute("coleccion", colecciondao.Consultar_Coleccion());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rt = request.getRequestDispatcher(ruta);
        rt.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";

        String ID = request.getParameter("txtID");
        cdbean.setCodigo_cd(Integer.parseInt(request.getParameter("cmbcd")));
        coleccionbean.setCd(cdbean);
        coleccionbean.setPrecio_venta(Double.parseDouble(request.getParameter("txtprecio")));
        promotorbean.setCodigo_promotor(Integer.parseInt(request.getParameter("cmbpromotor")));
        coleccionbean.setPromotor(promotorbean);
        companiabean.setCodigo_compañia(Integer.parseInt(request.getParameter("cmbcompania")));
        coleccionbean.setCompañia(companiabean);

        if (ID == null || ID.isEmpty()) {
            try {
                colecciondao.insertar(coleccionbean);
                ruta = "/Vistas/Vista_Coleccion/mostrar_coleccion.jsp";
                request.setAttribute("coleccion", colecciondao.Consultar_Coleccion());
            } catch (Exception e) {
            }
        } else {
            try {

               coleccionbean.setCodigo_coleccion(Integer.parseInt(ID));
              colecciondao.actualizar(coleccionbean);
                ruta = "/Vistas/Vista_Coleccion/mostrar_coleccion.jsp";
                request.setAttribute("coleccion", colecciondao.Consultar_Coleccion());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }
}
