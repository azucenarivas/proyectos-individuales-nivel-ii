/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.ArtistaDao;
import dao.CompaniaDao;
import dao.PaisDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CompaniaBean;
import modelo.PaisBean;

@WebServlet(name = "CompaniaServlet", urlPatterns = {"/CompaniaServlet"})
public class CompaniaServlet extends HttpServlet {

    Conexion conn = new Conexion();

    CompaniaDao companiadao = new CompaniaDao(conn);
    PaisDao paisdao = new PaisDao(conn);

    CompaniaBean companiabean = new CompaniaBean(0);
    PaisBean paisbean = new PaisBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("compania")) {
            ruta = "/Vistas/Vista_Compania/mostrar_compania.jsp";

            try {
                request.setAttribute("compania", companiadao.Consultar_Compañia());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Compania/insertar_compania.jsp";
            try {
                request.setAttribute("pais", paisdao.Consultar_Pais());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Compania/actualizar_compania.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("compania", companiadao.consultarById_Compañia(idActual));
                request.setAttribute("pais", paisdao.Consultar_Pais());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Compania/mostrar_compania.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("compania", companiadao.Eliminar(idEliminar));
                request.setAttribute("compania", companiadao.Consultar_Compañia());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");

        companiabean.setNombre_c(request.getParameter("txtnombre_c"));
        companiabean.setDireccion_c(request.getParameter("txtdireccion"));
        paisbean.setCodigo_pais(Integer.parseInt(request.getParameter("cmbpais")));
        companiabean.setPais(paisbean);

        if (ID == null || ID.isEmpty()) {
            try {
                companiadao.insertar(companiabean);
                ruta = "/Vistas/Vista_Compania/mostrar_compania.jsp";
                request.setAttribute("compania", companiadao.Consultar_Compañia());
            } catch (Exception e) {
            }
        } else {
            try {

                companiabean.setCodigo_compañia(Integer.parseInt(ID));
                companiadao.actualizar(companiabean);
                ruta = "/Vistas/Vista_Compania/mostrar_compania.jsp";
                request.setAttribute("compania", companiadao.Consultar_Compañia());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
