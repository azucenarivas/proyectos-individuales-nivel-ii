/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.CompaniaDao;
import dao.EdicionesDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CompaniaBean;
import modelo.EdicionesBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "EdicionesServlet", urlPatterns = {"/EdicionesServlet"})
public class EdicionesServlet extends HttpServlet {

    Conexion conn = new Conexion();

    EdicionesDao ediciondao = new EdicionesDao(conn);
    CompaniaDao companiadao = new CompaniaDao(conn);

    EdicionesBean edicionbean = new EdicionesBean(0);
    CompaniaBean companiabean = new CompaniaBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("edicion")) {
            ruta = "/Vistas/Vista_Ediciones/mostrar_edicion.jsp";

            try {
                request.setAttribute("edicion", ediciondao.Consultar_Ediciones());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Ediciones/insertar_edicion.jsp";
            try {
                request.setAttribute("compania", companiadao.Consultar_Compañia());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Ediciones/actualizar_edicion.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("edicion", ediciondao.consultarById_Ediciones(idActual));
                request.setAttribute("compania", companiadao.Consultar_Compañia());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Ediciones/mostrar_edicion.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("edicion", ediciondao.Eliminar(idEliminar));
                request.setAttribute("edicion", ediciondao.Consultar_Ediciones());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        Date fechaE = Date.valueOf(request.getParameter("txtfecha"));
        edicionbean.setFecha_e(fechaE);
        edicionbean.setDetalle(request.getParameter("txtdetalle"));
        companiabean.setCodigo_compañia(Integer.parseInt(request.getParameter("cmbcompania")));
        edicionbean.setCompañia(companiabean);

        if (ID == null || ID.isEmpty()) {
            try {
                ediciondao.insertar(edicionbean);
                ruta = "/Vistas/Vista_Ediciones/mostrar_edicion.jsp";
                request.setAttribute("edicion", ediciondao.Consultar_Ediciones());
            } catch (Exception e) {
            }
        } else {
            try {

            edicionbean.setCodigo_ediciones(Integer.parseInt(ID));
            ediciondao.actualizar(edicionbean);
                ruta = "/Vistas/Vista_Ediciones/mostrar_edicion.jsp";
                request.setAttribute("edicion", ediciondao.Consultar_Ediciones());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }
}
