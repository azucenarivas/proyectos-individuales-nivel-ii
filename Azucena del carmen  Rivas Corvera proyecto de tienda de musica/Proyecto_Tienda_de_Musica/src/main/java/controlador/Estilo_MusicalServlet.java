/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.Estilo_musicalDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Estilo_MusicalBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "Estilo_MusicalServlet", urlPatterns = {"/Estilo_MusicalServlet"})
public class Estilo_MusicalServlet extends HttpServlet {

    Conexion conn = new Conexion();
    Estilo_musicalDao estilo_musicadao = new Estilo_musicalDao(conn);
    Estilo_MusicalBean estilo_musicabean = new Estilo_MusicalBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("estilo_musical")) {
            ruta = "/Vistas/Vista_Estilo_Musical/mostrar_musical.jsp";

            try {
                request.setAttribute("estilo_musical", estilo_musicadao.Consultar_EstiloMusical());
            } catch (Exception e) {
            }
        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Estilo_Musical/insertar_estiloMusical.jsp";

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Estilo_Musical/actualizar_estiloMusical.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("estilo_musical", estilo_musicadao.consultarById_Musical(idActual));
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Estilo_Musical/mostrar_musical.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("estilo_musical", estilo_musicadao.Eliminar(idEliminar));
                request.setAttribute("estilo_musical", estilo_musicadao.Consultar_EstiloMusical());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        estilo_musicabean.setTipo(request.getParameter("txttipo"));

        if (ID == null || ID.isEmpty()) {
            try {
               estilo_musicadao.insertar(estilo_musicabean);
                ruta = "/Vistas/Vista_Estilo_Musical/mostrar_musical.jsp";
                 request.setAttribute("estilo_musical", estilo_musicadao.Consultar_EstiloMusical());
            } catch (Exception e) {
            }
        } else {
            try {

               estilo_musicabean.setCodigo_estilo_musical(Integer.parseInt(ID));
                estilo_musicadao.actualizar(estilo_musicabean);

                ruta = "/Vistas/Vista_Estilo_Musical/mostrar_musical.jsp";
                 request.setAttribute("estilo_musical", estilo_musicadao.Consultar_EstiloMusical());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
