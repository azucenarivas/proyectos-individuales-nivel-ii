/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.Estilo_musicalDao;
import dao.GrupoDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.GrupoBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "GrupoServlet", urlPatterns = {"/GrupoServlet"})
public class GrupoServlet extends HttpServlet {

    Conexion conn = new Conexion();
    GrupoDao grupodao = new GrupoDao(conn);
    GrupoBean grupobean = new GrupoBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("grupo")) {
            ruta = "/Vistas/Vista_Grupo/mostrar_grupo.jsp";

            try {
                request.setAttribute("grupo", grupodao.Consultar_Grupo());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Grupo/insertar_grupo.jsp";

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Grupo/actualizar_grupo.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("grupo", grupodao.consultarById_Grupo(idActual));
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Grupo/mostrar_grupo.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("grupo", grupodao.Eliminar(idEliminar));
                request.setAttribute("grupo", grupodao.Consultar_Grupo());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");
        grupobean.setCantidad_personas(Integer.parseInt(request.getParameter("txtcantidad")));
        grupobean.setNombre(request.getParameter("txtnombre"));

        if (ID == null || ID.isEmpty()) {
            try {
                grupodao.insertar(grupobean);
                ruta = "/Vistas/Vista_Grupo/mostrar_grupo.jsp";
                request.setAttribute("grupo", grupodao.Consultar_Grupo());
            } catch (Exception e) {
            }
        } else {
            try {

                grupobean.setCodigo_grupo(Integer.parseInt(ID));
                grupodao.actualizar(grupobean);
                ruta = "/Vistas/Vista_Grupo/mostrar_grupo.jsp";
                request.setAttribute("grupo", grupodao.Consultar_Grupo());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
