/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.ArtistaDao;
import dao.GrupoDao;
import dao.Grupo_ArtistaDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ArtistaBean;
import modelo.GrupoBean;
import modelo.Grupo_ArtistasBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "Grupo_ArtistaServlet", urlPatterns = {"/Grupo_ArtistaServlet"})
public class Grupo_ArtistaServlet extends HttpServlet {

    Conexion conn = new Conexion();

    Grupo_ArtistaDao grupo_artisdao = new Grupo_ArtistaDao(conn);
    GrupoDao grupodao = new GrupoDao(conn);
    ArtistaDao artistadao = new ArtistaDao(conn);

    Grupo_ArtistasBean grupo_artisbean = new Grupo_ArtistasBean(0);
    GrupoBean grupobean = new GrupoBean(0);
    ArtistaBean artistabean = new ArtistaBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("grupo_artista")) {
            ruta = "/Vistas/Vista_Grupo_Artista/mostrar_grupoArtista.jsp";

            try {
                request.setAttribute("grupo_artista", grupo_artisdao.Consultar_GRupoArtista());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Grupo_Artista/insertar_grupoArtista.jsp";
            try {
                request.setAttribute("artista", artistadao.Consultar_Artista());
                request.setAttribute("grupo", grupodao.Consultar_Grupo());

            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Grupo_Artista/actualizar_grupoArtista.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("grupo_artista", grupo_artisdao.consultarById_Artista(idActual));
                request.setAttribute("artista", artistadao.Consultar_Artista());
                request.setAttribute("grupo", grupodao.Consultar_Grupo());

            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Grupo_Artista/mostrar_grupoArtista.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("grupo_artista", grupo_artisdao.Eliminar(idEliminar));
                request.setAttribute("grupo_artista", grupo_artisdao.Consultar_GRupoArtista());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");

        artistabean.setCodigo_artista(Integer.parseInt(request.getParameter("cmbartista")));
        grupo_artisbean.setArtista(artistabean);
        grupobean.setCodigo_grupo(Integer.parseInt(request.getParameter("cmbgrupo")));
        grupo_artisbean.setGrupo(grupobean);

        if (ID == null || ID.isEmpty()) {
            try {
                grupo_artisdao.insertar(grupo_artisbean);
                ruta = "/Vistas/Vista_Grupo_Artista/mostrar_grupoArtista.jsp";
                request.setAttribute("grupo_artista", grupo_artisdao.Consultar_GRupoArtista());

            } catch (Exception e) {
            }
        } else {
            try {

                grupo_artisbean.setCodigo_grupo_artista(Integer.parseInt(ID));
                grupo_artisdao.actualizar(grupo_artisbean);
                ruta = "/Vistas/Vista_Grupo_Artista/mostrar_grupoArtista.jsp";
                request.setAttribute("grupo_artista", grupo_artisdao.Consultar_GRupoArtista());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
