/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.PaisDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.PaisBean;

@WebServlet(name = "PaisServlet", urlPatterns = {"/PaisServlet"})
public class PaisServlet extends HttpServlet {

    Conexion conn = new Conexion();
    PaisDao paisdao = new PaisDao(conn);
    PaisBean paisbean = new PaisBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("pais")) {
            ruta = "/Vistas/Vista_Pais/mostrar_pais.jsp";

            try {
                request.setAttribute("pais", paisdao.Consultar_Pais());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Pais/insertar_pais.jsp";

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Pais/actualizar_pais.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("pais", paisdao.consultarById_Pais(idActual));
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Pais/mostrar_pais.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("pais", paisdao.Eliminar(idEliminar));
                request.setAttribute("pais", paisdao.Consultar_Pais());
            } catch (Exception e) {
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";

        String ID = request.getParameter("txtID");

        paisbean.setNombre(request.getParameter("txtnombre"));

        if (ID == null || ID.isEmpty()) {
            try {
                paisdao.insertar(paisbean);
                ruta = "/Vistas/Vista_Pais/mostrar_pais.jsp";
                request.setAttribute("pais", paisdao.Consultar_Pais());
            } catch (Exception e) {
            }
        } else {
            try {

                paisbean.setCodigo_pais(Integer.parseInt(ID));
                paisdao.actualizar(paisbean);

                ruta = "/Vistas/Vista_Pais/mostrar_pais.jsp";
                request.setAttribute("pais", paisdao.Consultar_Pais());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }
}
