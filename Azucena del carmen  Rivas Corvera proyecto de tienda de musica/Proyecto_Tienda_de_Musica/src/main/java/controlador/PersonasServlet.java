/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.CargoDao;
import dao.PersonaDao;
import dao.usuariosDao1_copia;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CargosBean;
import modelo.PersonasBean;
import modelo.UsuariosBean;

@WebServlet(name = "PersonasServlet", urlPatterns = {"/PersonasServlet"})
public class PersonasServlet extends HttpServlet {

    Conexion conn = new Conexion();
    
    PersonaDao personadao = new PersonaDao(conn);
    usuariosDao1_copia usuariodao = new usuariosDao1_copia(conn);
    CargoDao cargodao = new CargoDao(conn);

    PersonasBean personabean = new PersonasBean(0);
    UsuariosBean usuariobean = new UsuariosBean(0);
    CargosBean cargosbean = new CargosBean(0);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("inicio")) {
            ruta = "/Vistas/Vista_Persona/registrar_usuarios.jsp";

        }
        RequestDispatcher rt = request.getRequestDispatcher(ruta);
        rt.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";
        String accion = request.getParameter("action");
        personabean.setNombre(request.getParameter("nombre"));
        personabean.setApellido(request.getParameter("apellido"));
        personabean.setDireccion(request.getParameter("direccion"));

        usuariobean.setUsuario(request.getParameter("usuario"));
        usuariobean.setContrasenia(request.getParameter("contrasenia"));

        try {
            personadao.insertar(personabean);
            usuariodao.insertarUsuario(usuariobean);
            ruta = "/Vistas/Vista_Persona/mostrar_usuarios.jsp";
            request.setAttribute("persona", personadao.Consultar_Personas());

        } catch (Exception e) {
        }
        RequestDispatcher rt = request.getRequestDispatcher(ruta);
        rt.forward(request, response);

    }

}
