/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.CdDao;
import dao.Grupo_ArtistaDao;
import dao.PistasDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CdBean;
import modelo.PistaBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "PistaServlet", urlPatterns = {"/PistaServlet"})
public class PistasServlet extends HttpServlet {

    Conexion conn = new Conexion();

    PistasDao pistadao = new PistasDao(conn);
    CdDao cddao = new CdDao(conn);

    PistaBean pistabean = new PistaBean(0);
    CdBean cdbean = new CdBean(0);

    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("pistas")) {

            ruta = "/Vistas/Vista_Pistas/mostrar_pista.jsp";

            try {
                request.setAttribute("pista", pistadao.Consultar_Pistas());
            } catch (Exception e) {
                e.printStackTrace();
            }
        
        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Pistas/insertar_pista.jsp";
            try {
                request.setAttribute("cd", cddao.Consultar_Cd());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Pistas/actualizar_pista.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("pista", pistadao.consultarById_Pista(idActual));
                request.setAttribute("cd", cddao.Consultar_Cd());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("eliminar")) {
            ruta = "/Vistas/Vista_Pistas/mostrar_pista.jsp";
            try {
                int idEliminar = Integer.parseInt(request.getParameter("id"));
                request.setAttribute("pista", pistadao.Eliminar(idEliminar));
                request.setAttribute("pista", pistadao.Consultar_Pistas());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rt = request.getRequestDispatcher(ruta);
        rt.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");

        pistabean.setNumero_pista(Integer.parseInt(request.getParameter("txtnumero")));
        pistabean.setTitulo(request.getParameter("txttitulo"));
        pistabean.setDuracion(request.getParameter("txtduracion"));
        cdbean.setCodigo_cd(Integer.parseInt(request.getParameter("cmbcd")));
        pistabean.setCd(cdbean);

        if (ID == null || ID.isEmpty()) {
            try {
                pistadao.insertar(pistabean);
                ruta = "/Vistas/Vista_Pistas/mostrar_pista.jsp";
                request.setAttribute("pista", pistadao.Consultar_Pistas());
            } catch (Exception e) {
            }
        } else {
            try {

                pistabean.setCodigo_pistas(Integer.parseInt(ID));
                pistadao.actualizar(pistabean);

                ruta = "/Vistas/Vista_Pistas/mostrar_pista.jsp";
                request.setAttribute("pista", pistadao.Consultar_Pistas());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
