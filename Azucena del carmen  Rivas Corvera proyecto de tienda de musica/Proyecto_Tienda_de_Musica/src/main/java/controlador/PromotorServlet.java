/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.PromotorDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.PromotorBean;

@WebServlet(name = "PromotorServlet", urlPatterns = {"/PromotorServlet"})
public class PromotorServlet extends HttpServlet {

    Conexion conn = new Conexion();
    PromotorDao promotordao = new PromotorDao(conn);
    PromotorBean promotorbean = new PromotorBean(0);
    String msg;
    boolean respuesta;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";
        String accion = request.getParameter("action");

        if (accion.equalsIgnoreCase("promotor")) {
            ruta = "/Vistas/Vista_Promotor/mostrar_promotor.jsp";

            try {
                request.setAttribute("promotor", promotordao.Consultar_Promotores());
            } catch (Exception e) {
            }

        } else if (accion.equalsIgnoreCase("nuevo")) {
            ruta = "/Vistas/Vista_Promotor/insertar_promotor.jsp";

        } else if (accion.equalsIgnoreCase("editar")) {
            ruta = "/Vistas/Vista_Promotor/actualizar_promotor.jsp";

            try {
                int idActual = Integer.parseInt(request.getParameter("id"));
               request.setAttribute("promotor", promotordao.consultarById_Promotor(idActual));
            } catch (Exception e) {
            }

        }else if(accion.equalsIgnoreCase("eliminar")){
               ruta = "/Vistas/Vista_Promotor/mostrar_promotor.jsp";
               try {
                int idEliminar=Integer.parseInt(request.getParameter("id"));
                request.setAttribute("promotor", promotordao.Eliminar(idEliminar));
                request.setAttribute("promotor", promotordao.Consultar_Promotores());
            } catch (Exception e) {
            }
        
        }if (accion.equalsIgnoreCase("promotores")) {
            ruta = "/Vistas/Vista_usuario/mostrar_promotor.jsp";

            try {
                request.setAttribute("promotor", promotordao.Consultar_Promotores());
            } catch (Exception e) {
            }

        }

        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ruta = "";

        String ID = request.getParameter("txtID");

        promotorbean.setNombre_p(request.getParameter("txtnombre"));
        promotorbean.setDireccion_p(request.getParameter("txtdireccion"));

        if (ID == null || ID.isEmpty()) {
            try {
                promotordao.insertar(promotorbean);
                ruta = "/Vistas/Vista_Promotor/mostrar_promotor.jsp";
                request.setAttribute("promotor", promotordao.Consultar_Promotores());
            } catch (Exception e) {
            }
        } else {
            try {

                promotorbean.setCodigo_promotor(Integer.parseInt(ID));
                promotordao.actualizar(promotorbean);

                ruta = "/Vistas/Vista_Promotor/mostrar_promotor.jsp";
                request.setAttribute("promotor", promotordao.Consultar_Promotores());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);
    }

}
