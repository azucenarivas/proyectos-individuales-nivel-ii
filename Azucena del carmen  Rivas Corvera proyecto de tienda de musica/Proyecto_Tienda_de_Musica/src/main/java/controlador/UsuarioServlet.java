/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.CargoDao;
import dao.PersonaDao;
import dao.usuariosDao1_copia;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CargosBean;
import modelo.PersonasBean;
import modelo.UsuariosBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "UsuarioServlet", urlPatterns = {"/UsuarioServlet"})
public class UsuarioServlet extends HttpServlet {

    Conexion conn = new Conexion();
    PersonaDao personadao = new PersonaDao(conn);
    usuariosDao1_copia usuariodao = new usuariosDao1_copia(conn);
    CargoDao cargodao = new CargoDao(conn);

    PersonasBean personabean = new PersonasBean(0);
    UsuariosBean usuariobean = new UsuariosBean(0);
    CargosBean cargosbean = new CargosBean(0);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta="";
        String action=request.getParameter("action");
        
        if(action.equalsIgnoreCase("usuario")){
            ruta="/Vistas/Vista_Usuario2/vista_usuario2.jsp";
            try {
                
                request.setAttribute("llenausuario", usuariodao);
            } catch (Exception e) {
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";
        String accion = request.getParameter("action");

        usuariobean.setCodigo_persona(Integer.parseInt(request.getParameter("codigo_persona")));
        cargosbean.setCodigo_cargo(Integer.parseInt(request.getParameter("codigo_cargo")));
        usuariobean.setCargos(cargosbean);
        usuariobean.setUsuario(request.getParameter("usuario"));
        usuariobean.setContrasenia(request.getParameter("contrasenia"));

        usuariodao.insertarUsuario(usuariobean);
        ruta = "/Vistas/Vista_Persona/mostrar_usuarios.jsp";
        request.setAttribute("persona", personadao.Consultar_Personas());

    }

}
