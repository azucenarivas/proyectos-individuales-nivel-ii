/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.ArtistaDao;
import dao.CdDao;
import dao.PistasDao;
import dao.PromotorDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.PistaBean;

/**
 *
 * @author azucena.rivasusam
 */
@WebServlet(name = "Vista_UsuarioServlet", urlPatterns = {"/Vista_UsuarioServlet"})
public class Vista_UsuarioServlet extends HttpServlet {

    Conexion conn = new Conexion();
    PromotorDao promotordao = new PromotorDao(conn);
    PistasDao pistadao = new PistasDao(conn);
    ArtistaDao artista = new ArtistaDao(conn);
    CdDao cddao = new CdDao(conn);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = "";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("promotores")) {
            ruta = "Vistas/Vistas_usuario/mostrar_promotor.jsp";
            try {
                request.setAttribute("promotor", promotordao.Consultar_Promotores());
            } catch (Exception e) {
            }

        } else if (action.equalsIgnoreCase("pistas2")) {

            ruta = "/Vistas/Vistas_usuario/mostrar_pista_usuario.jsp";

            try {
                request.setAttribute("pista", pistadao.Consultar_Pistas());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (action.equalsIgnoreCase("artista")) {

            ruta = "/Vistas/Vistas_usuario/mostrar_grupoArtista.jsp";

            try {
                request.setAttribute("artista", artista.Consultar_Artista());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (action.equalsIgnoreCase("cd")) {

            ruta = "/Vistas/Vistas_usuario/mostrar_cd_usuario.jsp";

            try {
                request.setAttribute("cd", artista.Consultar_Artista());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        RequestDispatcher rd = request.getRequestDispatcher(ruta);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
