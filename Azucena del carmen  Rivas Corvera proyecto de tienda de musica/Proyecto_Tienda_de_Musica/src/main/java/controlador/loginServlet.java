package controlador;

import conexion.Conexion;
import dao.usuariosDao1_copia;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "loginServlet", urlPatterns = {"/loginServlet"})
public class loginServlet extends HttpServlet {

    Conexion conn = new Conexion();
    RequestDispatcher rd;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession actual = request.getSession(true);
        actual.setAttribute("ingreso", null);
        rd = request.getRequestDispatcher("/inicio_sesion.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String usuario = request.getParameter("txtUsuario");
        String contrasenia = request.getParameter("txtContra");

        usuariosDao1_copia u = new usuariosDao1_copia(conn);

        try {
            if (u.validarLogin(usuario, contrasenia)) {
                HttpSession actual = request.getSession(true);
                actual.setAttribute("ingreso", usuario);
                switch (u.roles(usuario, contrasenia)) {
                    case 1:
                        rd = request.getRequestDispatcher("/menu_admin.jsp");
                        rd.forward(request, response);
                        break;
                    case 2:
                        rd = request.getRequestDispatcher("/menu_usuario.jsp");
                        rd.forward(request, response);
                        break;
                }
            } else {
                response.sendRedirect("index.jsp");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
