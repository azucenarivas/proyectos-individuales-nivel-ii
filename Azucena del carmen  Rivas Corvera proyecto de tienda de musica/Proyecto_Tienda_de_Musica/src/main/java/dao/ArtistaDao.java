/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.ArtistaBean;

/**
 *
 * @author azucena.rivasusam
 */
public class ArtistaDao {

    Conexion conn;

    public ArtistaDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(ArtistaBean artistab) {

        String sql = "insert into artista(nombre_a,codigo_estilo_musical,codigo_cd,codigo_pais)values(?,?,?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, artistab.getNombre_a());
            ps.setInt(2, artistab.getEstilo_musical().getCodigo_estilo_musical());
            ps.setInt(3, artistab.getCd().getCodigo_cd());
            ps.setInt(4, artistab.getPais().getCodigo_pais());

            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<ArtistaBean> Consultar_Artista() {

        String sql = "select a.codigo_artista,a.nombre_a,e.tipo,c.nombre_cd,pa.nombre\n"
                + "from artista a\n"
                + "inner join estilo_musical e on e.codigo_estilo_musical=a.codigo_estilo_musical\n"
                + "inner join cd  c on c.codigo_cd=a.codigo_cd\n"
                + "inner join pais pa on pa.codigo_pais=a.codigo_pais order by a.codigo_artista ";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ArtistaBean artistab;
            ResultSet rs = ps.executeQuery();
            List<ArtistaBean> lista = new LinkedList<>();
            while (rs.next()) {
                artistab = new ArtistaBean(rs.getInt("codigo_artista"));
                artistab.setNombre_a(rs.getString("nombre_a"));
                artistab.setTipo(rs.getString("tipo"));
                artistab.setNombre_cd(rs.getString("nombre_cd"));
                artistab.setNombre(rs.getString("nombre"));
                lista.add(artistab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

    public List<ArtistaBean> consultarById_Artista(int codigo) {

        String sql = "select *from artista where codigo_artista=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            ArtistaBean artistab;
            List<ArtistaBean> lista = new LinkedList<>();
            while (rs.next()) {
                artistab = new ArtistaBean(rs.getInt("codigo_artista"));
                artistab.setNombre_a(rs.getString("nombre_a"));
                artistab.setCodigo_estilo_musical(rs.getInt("codigo_estilo_musical"));
                artistab.setCodigo_cd(rs.getInt("codigo_cd"));
                artistab.setCodigo_pais(rs.getInt("codigo_pais"));

                lista.add(artistab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(ArtistaBean artistab) {

        String sql = "update artista set nombre_a=?,codigo_estilo_musical=?,codigo_cd=?,codigo_pais=? where codigo_artista=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, artistab.getNombre_a());
            ps.setInt(2, artistab.getEstilo_musical().getCodigo_estilo_musical());
            ps.setInt(3, artistab.getCd().getCodigo_cd());
            ps.setInt(4, artistab.getPais().getCodigo_pais());
            ps.setInt(5, artistab.getCodigo_artista());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public boolean Eliminar(int codigo_artista) {
        String sql = "delete from artista where codigo_artista=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_artista);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

}
