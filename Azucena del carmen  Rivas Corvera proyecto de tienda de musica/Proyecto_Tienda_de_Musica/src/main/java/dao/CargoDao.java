package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.CargosBean;

public class CargoDao {

    Conexion conn;

    public CargoDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(CargosBean cargob) {

        String sql = "insert into cargo(cargo) values(?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, cargob.getCargo());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<CargosBean> Consultar_Cargos() {

        String sql = "select *from cargo";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            CargosBean cargob;
            ResultSet rs = ps.executeQuery();
            List<CargosBean> lista = new LinkedList<>();
            while (rs.next()) {
                cargob = new CargosBean(rs.getInt("codigo_cargo"));
                cargob.setCargo(rs.getString("cargo"));
                lista.add(cargob);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

    public List<CargosBean> consultarById_Cargos(int codigo) {

        String sql = "select *from cargo where codigo_cargo=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            CargosBean cargob;
            List<CargosBean> lista = new LinkedList<>();
            while (rs.next()) {
                cargob = new CargosBean(rs.getInt("codigo_cargo"));
                cargob.setCargo(rs.getString("cargo"));
                lista.add(cargob);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(CargosBean cargob) {

        String sql = "update cargo set cargo=? where codigo_cargo=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, cargob.getCargo());
            ps.setInt(2, cargob.getCodigo_cargo());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println(" " + e);
            return false;
        }

    }

    public boolean Eliminar(int codigo_cargo) {
        String sql = "delete from cargo where codigo_cargo=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_cargo);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

}
