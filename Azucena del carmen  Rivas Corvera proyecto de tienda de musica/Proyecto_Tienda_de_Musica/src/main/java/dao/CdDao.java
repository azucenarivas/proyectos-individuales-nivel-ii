/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.CdBean;

/**
 *
 * @author azucena.rivasusam
 */
public class CdDao {

    Conexion conn;

    public CdDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(CdBean cdb) {

        String sql = "insert into cd(nombre_cd,clave,precio_v)values(?,?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, cdb.getNombre_cd());
            ps.setString(2, cdb.getClave());
            ps.setDouble(3, cdb.getPrecio_v());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<CdBean> Consultar_Cd() {

        String sql = "select *from cd";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            CdBean cdb;
            ResultSet rs = ps.executeQuery();
            List<CdBean> lista = new LinkedList<>();
            while (rs.next()) {
                cdb = new CdBean(rs.getInt("codigo_cd"));
                cdb.setNombre_cd(rs.getString("nombre_cd"));
                cdb.setClave(rs.getString("clave"));
                cdb.setPrecio_v(rs.getDouble("precio_v"));
                lista.add(cdb);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

    public List<CdBean> consultarById_Cd(int codigo) {

        String sql = "select *from cd where codigo_cd=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            CdBean cdb;
            List<CdBean> lista = new LinkedList<>();
            while (rs.next()) {
                cdb = new CdBean(rs.getInt("codigo_cd"));
                cdb.setNombre_cd(rs.getString("nombre_cd"));
                cdb.setClave(rs.getString("clave"));
                cdb.setPrecio_v(rs.getDouble("precio_v"));
                lista.add(cdb);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(CdBean cdb) {

        String sql = "update cd set nombre_cd=?,clave=?,precio_v=? where codigo_cd=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, cdb.getNombre_cd());
            ps.setString(2, cdb.getClave());
            ps.setDouble(3, cdb.getPrecio_v());
            ps.setInt(4, cdb.getCodigo_cd());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println(" " + e);
            return false;
        }

    }

    public boolean Eliminar(int codigo_cd) {
        String sql = "delete from cd where codigo_cd=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_cd);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

}
