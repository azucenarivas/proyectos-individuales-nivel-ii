/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.ColeccionBean;

public class ColeccionDao {

    Conexion conn;

    public ColeccionDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(ColeccionBean coleccionb) {

        String sql = "insert into coleccion(codigo_cd,precio_venta,codigo_promotor,codigo_compañia)values(?,?,?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, coleccionb.getCd().getCodigo_cd());
            ps.setDouble(2, coleccionb.getPrecio_venta());
            ps.setInt(3, coleccionb.getPromotor().getCodigo_promotor());
            ps.setInt(4, coleccionb.getCompañia().getCodigo_compañia());

            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<ColeccionBean> Consultar_Coleccion() {

        String sql = "select c.codigo_coleccion,d.nombre_cd,c.precio_venta,p.nombre_p,co.nombre_c\n"
                + "from coleccion c\n"
                + "inner join cd d on d.codigo_cd=c.codigo_cd\n"
                + "inner join promotor p on p.codigo_promotor=c.codigo_promotor\n"
                + "inner join compañia co on co.codigo_compañia=c.codigo_compañia order by c.codigo_coleccion";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ColeccionBean coleccionb;
            ResultSet rs = ps.executeQuery();
            List<ColeccionBean> lista = new LinkedList<>();
            while (rs.next()) {
                coleccionb = new ColeccionBean(rs.getInt("codigo_coleccion"));
                coleccionb.setNombre_cd(rs.getString("nombre_cd"));
                coleccionb.setPrecio_venta(rs.getDouble("precio_venta"));
                coleccionb.setNombre_p(rs.getString("nombre_p"));
                coleccionb.setNombre_c(rs.getString("nombre_c"));
                lista.add(coleccionb);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

    public List<ColeccionBean> consultarById_Coleccion(int codigo) {

        String sql = "select *from coleccion where codigo_coleccion=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            ColeccionBean coleccionb;
            List<ColeccionBean> lista = new LinkedList<>();
            while (rs.next()) {
                coleccionb = new ColeccionBean(rs.getInt("codigo_coleccion"));
                coleccionb.setCodigo_cd(rs.getInt("codigo_cd"));
                coleccionb.setPrecio_venta(rs.getDouble("precio_venta"));
                coleccionb.setCodigo_promotor(rs.getInt("codigo_promotor"));
                coleccionb.setCodigo_compañia(rs.getInt("codigo_compañia"));

                lista.add(coleccionb);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(ColeccionBean coleccionb) {

        String sql = "update coleccion set codigo_cd=?,precio_venta=?,codigo_promotor=?,codigo_compañia=? where codigo_coleccion=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, coleccionb.getCd().getCodigo_cd());
            ps.setDouble(2, coleccionb.getPrecio_venta());
            ps.setInt(3, coleccionb.getPromotor().getCodigo_promotor());
            ps.setInt(4, coleccionb.getCompañia().getCodigo_compañia());
            ps.setInt(5, coleccionb.getCodigo_coleccion());

            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public boolean Eliminar(int codigo_coleccion) {
        String sql = "delete from coleccion where codigo_coleccion=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_coleccion);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

}
