/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.CompaniaBean;

/**
 *
 * @author azucena.rivasusam
 */
public class CompaniaDao {
    Conexion conn;

    public CompaniaDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(CompaniaBean companiab) {

        String sql = "insert into compañia(nombre_c,direccion_c,codigo_pais)values(?,?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, companiab.getNombre_c());
            ps.setString(2, companiab.getDireccion_c());
            ps.setInt(3, companiab.getPais().getCodigo_pais());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<CompaniaBean> Consultar_Compañia() {

        String sql = "select c.codigo_compañia,c.nombre_c,c.direccion_c,p.nombre\n"
                + "from compañia c\n"
                + "inner join pais p on p.codigo_pais=c.codigo_pais order by c.codigo_compañia";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            CompaniaBean companiab;
            ResultSet rs = ps.executeQuery();
            List<CompaniaBean> lista = new LinkedList<>();
            while (rs.next()) {
                companiab = new CompaniaBean(rs.getInt("codigo_compañia"));
                companiab.setNombre_c(rs.getString("nombre_c"));
                companiab.setDireccion_c(rs.getString("direccion_c"));
                companiab.setNombre(rs.getString("nombre"));
                lista.add(companiab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

    public List<CompaniaBean> consultarById_Compañia(int codigo) {

        String sql = "select *from compañia where codigo_compañia=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            CompaniaBean companiab;
            List<CompaniaBean> lista = new LinkedList<>();
            while (rs.next()) {
                companiab = new CompaniaBean(rs.getInt("codigo_compañia"));
                companiab.setNombre_c(rs.getString("nombre_c"));
                companiab.setDireccion_c(rs.getString("direccion_c"));
                companiab.setCodigo_pais(rs.getInt("codigo_pais"));
                lista.add(companiab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(CompaniaBean companiab) {

        String sql = "update compañia set nombre_c=?,direccion_c=?,codigo_pais=? where codigo_compañia=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, companiab.getNombre_c());
            ps.setString(2, companiab.getDireccion_c());
            ps.setInt(3, companiab.getPais().getCodigo_pais());
            ps.setInt(4, companiab.getCodigo_compañia());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public boolean Eliminar(int codigo_compañia) {
        String sql = "delete from compañia where codigo_compañia=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_compañia);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

}
