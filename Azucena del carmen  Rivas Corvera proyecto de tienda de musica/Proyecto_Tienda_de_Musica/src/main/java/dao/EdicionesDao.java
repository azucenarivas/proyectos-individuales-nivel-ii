/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.EdicionesBean;

/**
 *
 * @author azucena.rivasusam
 */
public class EdicionesDao {

    Conexion conn;

    public EdicionesDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(EdicionesBean edicionesb) {

        String sql = "insert into ediciones(fecha_e,detalle,codigo_compañia)values(?,?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setDate(1, edicionesb.getFecha_e());
            ps.setString(2, edicionesb.getDetalle());
            ps.setInt(3, edicionesb.getCompañia().getCodigo_compañia());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<EdicionesBean> Consultar_Ediciones() {

        String sql = "select e.codigo_ediciones,e.fecha_e,e.detalle,c.nombre_c,c.direccion_c\n"
                + "from ediciones e\n"
                + "inner join compañia c on c.codigo_compañia=e.codigo_compañia order by e.codigo_ediciones ";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            EdicionesBean edicionesb;
            ResultSet rs = ps.executeQuery();
            List<EdicionesBean> lista = new LinkedList<>();
            while (rs.next()) {
                edicionesb = new EdicionesBean(rs.getInt("codigo_ediciones"));
                edicionesb.setFecha_e(rs.getDate("fecha_e"));
                edicionesb.setDetalle(rs.getString("detalle"));
                edicionesb.setNombre_c(rs.getString("nombre_c"));
                edicionesb.setDireccion_c(rs.getString("direccion_c"));
                lista.add(edicionesb);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

    public List<EdicionesBean> consultarById_Ediciones(int codigo) {

        String sql = "select *from ediciones where codigo_ediciones=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            EdicionesBean edicionesb;
            List<EdicionesBean> lista = new LinkedList<>();
            while (rs.next()) {
                edicionesb = new EdicionesBean(rs.getInt("codigo_ediciones"));
                edicionesb.setFecha_e(rs.getDate("fecha_e"));
                edicionesb.setDetalle(rs.getString("detalle"));
                edicionesb.setCodigo_compañia(rs.getInt("codigo_compañia"));
                lista.add(edicionesb);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(EdicionesBean edicionesb) {

        String sql = "update ediciones set fecha_e=?,detalle=?,codigo_compañia=? where codigo_ediciones=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setDate(1, edicionesb.getFecha_e());
            ps.setString(2, edicionesb.getDetalle());
            ps.setInt(3, edicionesb.getCompañia().getCodigo_compañia());
            ps.setInt(4, edicionesb.getCodigo_ediciones());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public boolean Eliminar(int codigo_ediciones) {
        String sql = "delete from ediciones where codigo_ediciones=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_ediciones);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

}
