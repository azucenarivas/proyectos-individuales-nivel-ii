/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Estilo_MusicalBean;


/**
 *
 * @author azucena.rivasusam
 */
public class Estilo_musicalDao {
    
        Conexion conn;

    public Estilo_musicalDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(Estilo_MusicalBean musicab) {

        String sql = "insert into estilo_musical(tipo)values(?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, musicab.getTipo());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<Estilo_MusicalBean> Consultar_EstiloMusical() {

        String sql = "select *from estilo_musical";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Estilo_MusicalBean musicab;
            ResultSet rs = ps.executeQuery();
            List<Estilo_MusicalBean> lista = new LinkedList<>();
            while (rs.next()) {
                musicab = new Estilo_MusicalBean(rs.getInt("codigo_estilo_musical"));
                musicab.setTipo(rs.getString("tipo"));
                lista.add(musicab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

 
    public List<Estilo_MusicalBean> consultarById_Musical(int codigo) {

        String sql = "select *from estilo_musical where codigo_estilo_musical=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            Estilo_MusicalBean musicab;
            List<Estilo_MusicalBean> lista = new LinkedList<>();
            while (rs.next()) {
                musicab = new Estilo_MusicalBean(rs.getInt("codigo_estilo_musical"));
                musicab.setTipo(rs.getString("tipo")); 
                lista.add(musicab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(Estilo_MusicalBean musicab) {

        String sql = "update estilo_musical set tipo=? where codigo_estilo_musical=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, musicab.getTipo());
            ps.setInt(2, musicab.getCodigo_estilo_musical());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println(" " + e);
            return false;
        }

    }
    
       public boolean Eliminar(int codigo_promotor) {
        String sql = "delete from estilo_musical where codigo_estilo_musical=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_promotor);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }


    
}
