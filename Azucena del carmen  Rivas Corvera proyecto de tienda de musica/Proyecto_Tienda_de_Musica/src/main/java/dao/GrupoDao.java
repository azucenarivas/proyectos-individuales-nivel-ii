/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.GrupoBean;


/**
 *
 * @author azucena.rivasusam
 */
public class GrupoDao {
         Conexion conn;

    public GrupoDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(GrupoBean grupob) {

        String sql = "insert into grupo(cantidad_personas,nombre)values(?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, grupob.getCantidad_personas());
            ps.setString(2, grupob.getNombre());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<GrupoBean> Consultar_Grupo() {

        String sql = "select *from grupo";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            GrupoBean grupob;
            ResultSet rs = ps.executeQuery();
            List<GrupoBean> lista = new LinkedList<>();
            while (rs.next()) {
                grupob = new GrupoBean(rs.getInt("codigo_grupo"));
                grupob.setCantidad_personas(rs.getInt("cantidad_personas"));
                grupob.setNombre(rs.getString("nombre"));
                lista.add(grupob);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

 
    public List<GrupoBean> consultarById_Grupo(int codigo) {

        String sql = "select *from grupo where codigo_grupo=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            GrupoBean grupob;
            List<GrupoBean> lista = new LinkedList<>();
            while (rs.next()) {
                grupob = new GrupoBean(rs.getInt("codigo_grupo"));
                grupob.setCantidad_personas(rs.getInt("cantidad_personas"));
                grupob.setNombre(rs.getString("nombre")); 
                lista.add(grupob);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(GrupoBean grupob) {

        String sql = "update grupo set cantidad_personas=?,nombre=? where codigo_grupo=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, grupob.getCantidad_personas());
            ps.setString(2, grupob.getNombre());
            ps.setInt(3, grupob.getCodigo_grupo());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println(" " + e);
            return false;
        }

    }
    
       public boolean Eliminar(int codigo_grupo) {
        String sql = "delete from grupo where codigo_grupo=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_grupo);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

    
}
