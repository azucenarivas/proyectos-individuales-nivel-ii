/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Grupo_ArtistasBean;

/**
 *
 * @author azucena.rivasusam
 */
public class Grupo_ArtistaDao {

    Conexion conn;

    public Grupo_ArtistaDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(Grupo_ArtistasBean grupob) {

        String sql = "insert into grupo_artista(codigo_artista,codigo_grupo)values(?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, grupob.getArtista().getCodigo_artista());
            ps.setInt(2, grupob.getGrupo().getCodigo_grupo());

            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<Grupo_ArtistasBean> Consultar_GRupoArtista() {

        String sql = "select g.codigo_grupo_artista,a.nombre_a,gru.nombre\n"
                + "from grupo_artista g\n"
                + "inner join artista a on a.codigo_artista=g.codigo_artista\n"
                + "inner join grupo gru on gru.codigo_grupo=g.codigo_grupo order by g.codigo_grupo_artista";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Grupo_ArtistasBean grupob;
            ResultSet rs = ps.executeQuery();
            List<Grupo_ArtistasBean> lista = new LinkedList<>();
            while (rs.next()) {
                grupob = new Grupo_ArtistasBean(rs.getInt("codigo_grupo_artista"));
                grupob.setNombre_a(rs.getString("nombre_a"));
                grupob.setNombre(rs.getString("nombre"));
                lista.add(grupob);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

    public List<Grupo_ArtistasBean> consultarById_Artista(int codigo) {

        String sql = "select *from grupo_artista where codigo_grupo_artista=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            Grupo_ArtistasBean grupob;
            List<Grupo_ArtistasBean> lista = new LinkedList<>();
            while (rs.next()) {
                grupob = new Grupo_ArtistasBean(rs.getInt("codigo_grupo_artista"));
                grupob.setCodigo_artista(rs.getInt("codigo_artista"));
                grupob.setCodigo_grupo(rs.getInt("codigo_grupo"));

                lista.add(grupob);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(Grupo_ArtistasBean grupob) {

        String sql = "update grupo_artista set codigo_artista=?,codigo_grupo=? where codigo_grupo_artista=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, grupob.getArtista().getCodigo_artista());
            ps.setInt(2, grupob.getGrupo().getCodigo_grupo());
            ps.setInt(3, grupob.getCodigo_grupo_artista());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public boolean Eliminar(int codigo) {
        
        String sql = "delete from grupo_artista where codigo_grupo_artista=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

}
