/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.PaisBean;

/**
 *
 * @author azucena.rivasusam
 */
public class PaisDao {
     Conexion conn;

    public PaisDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(PaisBean paisb) {

        String sql = "insert into pais(nombre)values(?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, paisb.getNombre());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<PaisBean> Consultar_Pais() {

        String sql = "select *from pais";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            PaisBean paisb;
            ResultSet rs = ps.executeQuery();
            List<PaisBean> lista = new LinkedList<>();
            while (rs.next()) {
                paisb = new PaisBean(rs.getInt("codigo_pais"));
                paisb.setNombre(rs.getString("nombre"));
                lista.add(paisb);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

 
    public List<PaisBean> consultarById_Pais(int codigo) {

        String sql = "select *from pais where codigo_pais=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            PaisBean paisb;
            List<PaisBean> lista = new LinkedList<>();
            while (rs.next()) {
                paisb = new PaisBean(rs.getInt("codigo_pais"));
                paisb.setNombre(rs.getString("nombre")); 
                lista.add
        (paisb);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(PaisBean paisb) {

        String sql = "update pais set nombre=? where codigo_pais=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, paisb.getNombre());
            ps.setInt(2, paisb.getCodigo_pais());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println(" " + e);
            return false;
        }

    }
    
       public boolean Eliminar(int codigo_pais) {
        String sql = "delete from pais where codigo_pais=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_pais);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }


    
    
}
