package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.PersonasBean;

public class PersonaDao {

    Conexion conn;

    public PersonaDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(PersonasBean personab) {

        String sql = "insert into persona(nombre,apellido,direccion) values(?,?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, personab.getNombre());
            ps.setString(2, personab.getApellido());
            ps.setString(3, personab.getDireccion());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<PersonasBean> Consultar_Personas() {

        String sql = "select *from persona";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            PersonasBean personab;
            ResultSet rs = ps.executeQuery();
            List<PersonasBean> lista = new LinkedList<>();
            while (rs.next()) {
                personab = new PersonasBean(rs.getInt("codigo_persona"));
                personab.setNombre(rs.getString("nombre"));
                personab.setApellido(rs.getString("apellido"));
                personab.setDireccion(rs.getString("direccion"));
                lista.add(personab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

    public List<PersonasBean> consultarById_Persona(int codigo) {

        String sql = "select *from persona where codigo_persona=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            PersonasBean personab;
            List<PersonasBean> lista = new LinkedList<>();
            while (rs.next()) {
                personab = new PersonasBean(rs.getInt("codigo_persona"));
                personab.setNombre(rs.getString("nombre"));
                personab.setApellido(rs.getString("apellido"));
                personab.setDireccion(rs.getString("direccion"));
                lista.add(personab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(PersonasBean personab) {

        String sql = "update persona set nombre=?,apellido=?,direccion=? where codigo_persona=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, personab.getNombre());
            ps.setString(2, personab.getApellido());
            ps.setString(3, personab.getDireccion());
            ps.setInt(4, personab.getCodigo_persona());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println(" " + e);
            return false;
        }

    }

    public boolean Eliminar(int codigo_persona) {
        String sql = "delete from persona where codigo_persona?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_persona);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

}
