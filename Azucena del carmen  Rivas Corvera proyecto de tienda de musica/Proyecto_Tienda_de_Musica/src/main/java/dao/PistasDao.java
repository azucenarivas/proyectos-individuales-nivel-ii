/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.EdicionesBean;
import modelo.PistaBean;

/**
 *
 * @author azucena.rivasusam
 */
public class PistasDao {

    Conexion conn;

    public PistasDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(PistaBean pistab) {

        String sql = "insert into pistas(numero_pista,titulo,duracion,codigo_cd)values(?,?,?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, pistab.getNumero_pista());
            ps.setString(2, pistab.getTitulo());
            ps.setString(3, pistab.getDuracion());
            ps.setInt(4, pistab.getCd().getCodigo_cd());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<PistaBean> Consultar_Pistas() {

        String sql = "select p.codigo_pistas,p.numero_pista,p.titulo,p.duracion,c.nombre_cd\n"
                + "from pistas p\n"
                + "inner join cd c on c.codigo_cd=p.codigo_cd";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            PistaBean pistab;
            ResultSet rs = ps.executeQuery();
            List<PistaBean> lista = new LinkedList<>();
            while (rs.next()) {
                pistab = new PistaBean(rs.getInt("codigo_pistas"));
                pistab.setNumero_pista(rs.getInt("numero_pista"));
                pistab.setTitulo(rs.getString("titulo"));
                pistab.setDuracion(rs.getString("duracion"));
                pistab.setNombre_cd(rs.getString("nombre_cd"));
                lista.add(pistab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

    public List<PistaBean> consultarById_Pista(int codigo) {

        String sql = "select *from pistas where codigo_pistas=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            PistaBean pistab;
            List<PistaBean> lista = new LinkedList<>();
            while (rs.next()) {
                pistab = new PistaBean(rs.getInt("codigo_pistas"));
                pistab.setNumero_pista(rs.getInt("numero_pista"));
                pistab.setTitulo(rs.getString("titulo"));
                pistab.setDuracion(rs.getString("duracion"));
                pistab.setCodigo_cd(rs.getInt("codigo_cd"));
                lista.add(pistab);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(PistaBean pistab) {

        String sql = "update pistas set numero_pista=?,titulo=?,duracion=?,codigo_cd=? where codigo_pistas=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, pistab.getNumero_pista());
            ps.setString(2, pistab.getTitulo());
            ps.setString(3, pistab.getDuracion());
            ps.setInt(4, pistab.getCd().getCodigo_cd());
            ps.setInt(5, pistab.getCodigo_pistas());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public boolean Eliminar(int codigo_pistas) {
        String sql = "delete from pistas where codigo_pistas=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_pistas);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }

}
