/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.PromotorBean;

/**
 *
 * @author azucena.rivasusam
 */
public class PromotorDao {

    Conexion conn;

    public PromotorDao(Conexion conn) {
        this.conn = conn;

    }

    public boolean insertar(PromotorBean promob) {

        String sql = "insert into promotor(nombre_p,direccion_p)values(?,?)";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, promob.getNombre_p());
            ps.setString(2, promob.getDireccion_p());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
            return false;
        }

    }

    public List<PromotorBean> Consultar_Promotores() {

        String sql = "select *from promotor;";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            PromotorBean promotorb;
            ResultSet rs = ps.executeQuery();
            List<PromotorBean> lista = new LinkedList<>();
            while (rs.next()) {
                promotorb = new PromotorBean(rs.getInt("codigo_promotor"));
                promotorb.setNombre_p(rs.getString("nombre_p"));
                promotorb.setDireccion_p(rs.getString("direccion_p"));
                lista.add(promotorb);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

 
    public List<PromotorBean> consultarById_Promotor(int codigo) {

        String sql = "select *from promotor where codigo_promotor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            PromotorBean promob;
            List<PromotorBean> lista = new LinkedList<>();
            while (rs.next()) {
                promob = new PromotorBean(rs.getInt("codigo_promotor"));
                promob.setNombre_p(rs.getString("nombre_p"));
                promob.setDireccion_p(rs.getString("direccion_p"));
                lista.add(promob);
            }
            return lista;
        } catch (Exception e) {
            System.err.println(" " + e);
        }
        return null;

    }

    public boolean actualizar(PromotorBean promob) {

        String sql = "update promotor set nombre_p=?,direccion_p=? where codigo_promotor=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, promob.getNombre_p());
            ps.setString(2, promob.getDireccion_p());
            ps.setInt(3, promob.getCodigo_promotor());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println(" " + e);
            return false;
        }

    }
    
       public boolean Eliminar(int codigo_promotor) {
        String sql = "delete from promotor where codigo_promotor=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, codigo_promotor);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return false;
    }


}
