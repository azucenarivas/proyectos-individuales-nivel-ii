package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import modelo.CdBean;
import modelo.UsuariosBean;

public class usuariosDao1_copia extends conexion.Conexion {

    Conexion conn;

    public usuariosDao1_copia(Conexion conn) {
        this.conn = conn;

    }

    public boolean validarLogin(String usuario, String contrasenia){

        try {

            String sql = "select * from usuario where usuario = ? and contrasenia = ?";
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, contrasenia);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            System.err.println("q paSo" + e);
        }

        return false;
    }

    public int roles(String usuario, String contrasenia) throws Exception {

        int rol = 0;

        try {

            String sql = "select * from usuario where usuario = ? and contrasenia = ?";
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, contrasenia);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                rol = rs.getInt("codigo_cargo");
            }
        } catch (Exception e) {
            System.err.println("q pao" + e);

        }

        return rol;
    }

    public boolean insertarUsuario(UsuariosBean usuariob) {
        try {

            String sql = "insert into usuario(codigo_persona,codigo_cargo,usuario,contrasenia) values(?,?,?,?)";
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, usuariob.getCodigo_persona());
            ps.setInt(2, usuariob.getCodigo_cargo());
            ps.setString(3, usuariob.getUsuario());
            ps.setString(4, usuariob.getContrasenia());

           ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("Error" + e);
        }
        return false;

    }
    
    public List<UsuariosBean> Consultar_Clientes() {

        String sql = "select *from usuario";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            UsuariosBean usuariob;
            ResultSet rs = ps.executeQuery();
            List<UsuariosBean> lista = new LinkedList<>();
            while (rs.next()) {
                usuariob = new UsuariosBean(rs.getInt("codigo_persona"));
                usuariob.setNombre(rs.getString("nombre_cd"));
                
                lista.add(usuariob);
            }
            return lista;
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return null;
    }

}
