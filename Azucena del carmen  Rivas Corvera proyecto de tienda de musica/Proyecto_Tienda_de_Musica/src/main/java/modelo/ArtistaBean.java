/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class ArtistaBean {

    private int codigo_artista;
    private String nombre_a;
    private Estilo_MusicalBean estilo_musical;
    private CdBean cd;
    private PaisBean pais;

    ///variables de impresion
    private int codigo_estilo_musical;
    private String tipo;
    private int codigo_cd;
    private String nombre_cd;
    private double Precio;
    private int codigo_coleccion;
    private int  codigo_pais;
    private String nombre;

    public ArtistaBean(int codigo_artista) {
        this.codigo_artista = codigo_artista;
    }

    public int getCodigo_artista() {
        return codigo_artista;
    }

    public void setCodigo_artista(int codigo_artista) {
        this.codigo_artista = codigo_artista;
    }

    public String getNombre_a() {
        return nombre_a;
    }

    public void setNombre_a(String nombre_a) {
        this.nombre_a = nombre_a;
    }

    public String getNombre_cd() {
        return nombre_cd;
    }

    public void setNombre_cd(String nombre_cd) {
        this.nombre_cd = nombre_cd;
    }

    
    public Estilo_MusicalBean getEstilo_musical() {
        return estilo_musical;
    }

    public void setEstilo_musical(Estilo_MusicalBean estilo_musical) {
        this.estilo_musical = estilo_musical;
    }

    public CdBean getCd() {
        return cd;
    }

    public void setCd(CdBean cd) {
        this.cd = cd;
    }

    public PaisBean getPais() {
        return pais;
    }

    public void setPais(PaisBean pais) {
        this.pais = pais;
    }

    public int getCodigo_estilo_musical() {
        return codigo_estilo_musical;
    }

    public void setCodigo_estilo_musical(int codigo_estilo_musical) {
        this.codigo_estilo_musical = codigo_estilo_musical;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCodigo_cd() {
        return codigo_cd;
    }

    public void setCodigo_cd(int codigo_cd) {
        this.codigo_cd = codigo_cd;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }

    public int getCodigo_coleccion() {
        return codigo_coleccion;
    }

    public void setCodigo_coleccion(int codigo_coleccion) {
        this.codigo_coleccion = codigo_coleccion;
    }

    public int getCodigo_pais() {
        return codigo_pais;
    }

    public void setCodigo_pais(int codigo_pais) {
        this.codigo_pais = codigo_pais;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
                    

}
