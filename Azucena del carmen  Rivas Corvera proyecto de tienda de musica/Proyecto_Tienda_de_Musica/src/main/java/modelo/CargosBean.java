package modelo;

public class CargosBean {

    private int codigo_cargo;
    private String cargo;

    public CargosBean(int codigo_cargo) {
        this.codigo_cargo = codigo_cargo;
    }

    public int getCodigo_cargo() {
        return codigo_cargo;
    }

    public void setCodigo_cargo(int codigo_cargo) {
        this.codigo_cargo = codigo_cargo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

   

  

}
