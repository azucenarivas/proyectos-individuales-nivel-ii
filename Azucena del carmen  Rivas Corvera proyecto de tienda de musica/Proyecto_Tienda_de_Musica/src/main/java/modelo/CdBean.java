/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class CdBean {

    private int codigo_cd;
    private String nombre_cd;
    private String clave;
    private double precio_v;

    public CdBean(int codigo_cd) {
        this.codigo_cd = codigo_cd;
    }

    public int getCodigo_cd() {
        return codigo_cd;
    }

    public void setCodigo_cd(int codigo_cd) {
        this.codigo_cd = codigo_cd;
    }

    public String getNombre_cd() {
        return nombre_cd;
    }

    public void setNombre_cd(String nombre_cd) {
        this.nombre_cd = nombre_cd;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public double getPrecio_v() {
        return precio_v;
    }

    public void setPrecio_v(double precio_v) {
        this.precio_v = precio_v;
    }

    
}
