/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class ColeccionBean {

    private int codigo_coleccion;
    private CdBean cd;
    private PromotorBean promotor;
    private CompaniaBean compañia;
    private double precio_venta;

    ///variables de impresion
    private int codigo_promotor;
    private String nombre_p;
    private int codigo_compañia;
    private String nombre_c;
    private int codigo_cd;
    private String nombre_cd;

    public ColeccionBean(int codigo_coleccion) {
        this.codigo_coleccion = codigo_coleccion;
    }

    public int getCodigo_coleccion() {
        return codigo_coleccion;
    }

    public void setCodigo_coleccion(int codigo_coleccion) {
        this.codigo_coleccion = codigo_coleccion;
    }

    public PromotorBean getPromotor() {
        return promotor;
    }

    public void setPromotor(PromotorBean promotor) {
        this.promotor = promotor;
    }

    public CompaniaBean getCompañia() {
        return compañia;
    }

    
    public void setCompañia(CompaniaBean compañia) {
        this.compañia = compañia;
    }

    public double getPrecio_venta() {
        return precio_venta;
    }

    public CdBean getCd() {
        return cd;
    }

    public void setCd(CdBean cd) {
        this.cd = cd;
    }

    public int getCodigo_cd() {
        return codigo_cd;
    }

    public void setCodigo_cd(int codigo_cd) {
        this.codigo_cd = codigo_cd;
    }

    public String getNombre_cd() {
        return nombre_cd;
    }

    public void setNombre_cd(String nombre_cd) {
        this.nombre_cd = nombre_cd;
    }

    
    public void setPrecio_venta(double precio_venta) {
        this.precio_venta = precio_venta;
    }

    public int getCodigo_promotor() {
        return codigo_promotor;
    }

    public void setCodigo_promotor(int codigo_promotor) {
        this.codigo_promotor = codigo_promotor;
    }

    public String getNombre_p() {
        return nombre_p;
    }

    public void setNombre_p(String nombre_p) {
        this.nombre_p = nombre_p;
    }

    public int getCodigo_compañia() {
        return codigo_compañia;
    }

    public void setCodigo_compañia(int codigo_compañia) {
        this.codigo_compañia = codigo_compañia;
    }

    public String getNombre_c() {
        return nombre_c;
    }

    public void setNombre_c(String nombre_c) {
        this.nombre_c = nombre_c;
    }
    
    

}
