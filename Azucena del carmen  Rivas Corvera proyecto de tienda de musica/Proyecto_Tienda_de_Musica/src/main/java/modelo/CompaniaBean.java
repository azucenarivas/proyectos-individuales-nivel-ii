/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class CompaniaBean {

    private int codigo_compañia;
    private String nombre_c;
    private String direccion_c;
    private PaisBean pais;

    ///variables de impresion
    private int codigo_pais;
    private String nombre;

    public CompaniaBean(int codigo_compañia) {
        this.codigo_compañia = codigo_compañia;
    }

    public int getCodigo_compañia() {
        return codigo_compañia;
    }

    public void setCodigo_compañia(int codigo_compañia) {
        this.codigo_compañia = codigo_compañia;
    }

    public String getNombre_c() {
        return nombre_c;
    }

    public void setNombre_c(String nombre_c) {
        this.nombre_c = nombre_c;
    }

    public String getDireccion_c() {
        return direccion_c;
    }

    public void setDireccion_c(String direccion_c) {
        this.direccion_c = direccion_c;
    }

    public PaisBean getPais() {
        return pais;
    }

    public void setPais(PaisBean pais) {
        this.pais = pais;
    }

    public int getCodigo_pais() {
        return codigo_pais;
    }

    public void setCodigo_pais(int codigo_pais) {
        this.codigo_pais = codigo_pais;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
