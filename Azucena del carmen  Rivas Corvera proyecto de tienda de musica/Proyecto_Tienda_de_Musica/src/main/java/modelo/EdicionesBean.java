/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Date;

/**
 *
 * @author azucena.rivasusam
 */
public class EdicionesBean {

    private int codigo_ediciones;
    private Date fecha_e;
    private String detalle;
    private CompaniaBean compañia;

    ////variables de impresion
    private int codigo_compañia;
    private String nombre_c;
    private String direccion_c;

    public EdicionesBean(int codigo_ediciones) {
        this.codigo_ediciones = codigo_ediciones;
    }

    public int getCodigo_ediciones() {
        return codigo_ediciones;
    }

    public void setCodigo_ediciones(int codigo_ediciones) {
        this.codigo_ediciones = codigo_ediciones;
    }

    public String getDetalle() {
        return detalle;
    }

    public String getDireccion_c() {
        return direccion_c;
    }

    public void setDireccion_c(String direccion_c) {
        this.direccion_c = direccion_c;
    }
    

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    
    public Date getFecha_e() {
        return fecha_e;
    }

    public void setFecha_e(Date fecha_e) {
        this.fecha_e = fecha_e;
    }

    public CompaniaBean getCompañia() {
        return compañia;
    }

    public void setCompañia(CompaniaBean compañia) {
        this.compañia = compañia;
    }

   
    public int getCodigo_compañia() {
        return codigo_compañia;
    }

    public void setCodigo_compañia(int codigo_compañia) {
        this.codigo_compañia = codigo_compañia;
    }

    public String getNombre_c() {
        return nombre_c;
    }

    public void setNombre_c(String nombre_c) {
        this.nombre_c = nombre_c;
    }
    
    
    
}
