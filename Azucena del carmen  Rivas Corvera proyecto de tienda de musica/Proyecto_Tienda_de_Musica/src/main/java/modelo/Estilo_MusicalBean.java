/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class Estilo_MusicalBean {

    private int codigo_estilo_musical;
    private String tipo;

    public Estilo_MusicalBean(int codigo_estilo_musical) {
        this.codigo_estilo_musical = codigo_estilo_musical;
    }

    public int getCodigo_estilo_musical() {
        return codigo_estilo_musical;
    }

    public void setCodigo_estilo_musical(int codigo_estilo_musical) {
        this.codigo_estilo_musical = codigo_estilo_musical;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
