/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class Grupo_ArtistasBean {

    private int codigo_grupo_artista;
    private ArtistaBean artista;
    private GrupoBean grupo;

    ////variables de impresion
    private int codigo_artista;
    private String nombre_a;
    private int codigo_grupo;
    private String nombre;

    public Grupo_ArtistasBean(int codigo_grupo_artista) {
        this.codigo_grupo_artista = codigo_grupo_artista;
    }

    public int getCodigo_grupo_artista() {
        return codigo_grupo_artista;
    }

    public void setCodigo_grupo_artista(int codigo_grupo_artista) {
        this.codigo_grupo_artista = codigo_grupo_artista;
    }

    public ArtistaBean getArtista() {
        return artista;
    }

    public void setArtista(ArtistaBean artista) {
        this.artista = artista;
    }

    public GrupoBean getGrupo() {
        return grupo;
    }

    public void setGrupo(GrupoBean grupo) {
        this.grupo = grupo;
    }

    public int getCodigo_artista() {
        return codigo_artista;
    }

    public void setCodigo_artista(int codigo_artista) {
        this.codigo_artista = codigo_artista;
    }

    public String getNombre_a() {
        return nombre_a;
    }

    public void setNombre_a(String nombre_a) {
        this.nombre_a = nombre_a;
    }

    public int getCodigo_grupo() {
        return codigo_grupo;
    }

    public void setCodigo_grupo(int codigo_grupo) {
        this.codigo_grupo = codigo_grupo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
