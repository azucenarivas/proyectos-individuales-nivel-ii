/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class PistaBean {

    private int codigo_pistas;
    private int numero_pista;
    private String titulo;
    private String duracion;
    private CdBean cd;

    ///variables de impresion
    private int codigo_cd;
    private String nombre_cd;

    public PistaBean(int codigo_pistas) {
        this.codigo_pistas = codigo_pistas;
    }

    public int getCodigo_pistas() {
        return codigo_pistas;
    }

    public void setCodigo_pistas(int codigo_pistas) {
        this.codigo_pistas = codigo_pistas;
    }

    public int getNumero_pista() {
        return numero_pista;
    }

    public String getNombre_cd() {
        return nombre_cd;
    }

    public void setNombre_cd(String nombre_cd) {
        this.nombre_cd = nombre_cd;
    }

    
    public void setNumero_pista(int numero_pista) {
        this.numero_pista = numero_pista;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public CdBean getCd() {
        return cd;
    }

    public void setCd(CdBean cd) {
        this.cd = cd;
    }

    public int getCodigo_cd() {
        return codigo_cd;
    }

    public void setCodigo_cd(int codigo_cd) {
        this.codigo_cd = codigo_cd;
    }
    
    
    

}
