/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author azucena.rivasusam
 */
public class PromotorBean {

    private int codigo_promotor;
    private String nombre_p;
    private String direccion_p;

    public PromotorBean(int codigo_promotor) {
        this.codigo_promotor = codigo_promotor;
    }

    public int getCodigo_promotor() {
        return codigo_promotor;
    }

    public void setCodigo_promotor(int codigo_promotor) {
        this.codigo_promotor = codigo_promotor;
    }

    public String getNombre_p() {
        return nombre_p;
    }

    public void setNombre_p(String nombre_p) {
        this.nombre_p = nombre_p;
    }

    public String getDireccion_p() {
        return direccion_p;
    }

    public void setDireccion_p(String direccion_p) {
        this.direccion_p = direccion_p;
    }

}
