package modelo;

public class UsuariosBean {

    private PersonasBean persona;
    private CargosBean cargos;
    private String usuario;
    private String contrasenia;
    
    ////variables de impresion
    
    private int codigo_persona;
    private String nombre;
    
     private int codigo_cargo;
    private String cargo;

    public UsuariosBean(int codigo_persona) {
        this.codigo_persona = codigo_persona;
    }

    public PersonasBean getPersona() {
        return persona;
    }

    public void setPersona(PersonasBean persona) {
        this.persona = persona;
    }

    public CargosBean getCargos() {
        return cargos;
    }

    public void setCargos(CargosBean cargos) {
        this.cargos = cargos;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public int getCodigo_persona() {
        return codigo_persona;
    }

    public void setCodigo_persona(int codigo_persona) {
        this.codigo_persona = codigo_persona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo_cargo() {
        return codigo_cargo;
    }

    public void setCodigo_cargo(int codigo_cargo) {
        this.codigo_cargo = codigo_cargo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    

   

}
