<%-- 
    Document   : mostrar_promotor
    Created on : 01-dic-2019, 19:12:11
    Author     : Azucena
--%>
<%@page session = "true" %>

<%
    HttpSession actual = request.getSession();

    String usuario;

    if (actual.getAttribute("ingreso") != null) {
        usuario = session.getAttribute("ingreso").toString();
    } else {
        response.sendRedirect("inicio_sesion.jsp");
    }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <jsp:include page="/menu_admin.jsp"/>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col m12" align="center">
                        <h3>
                            <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                 Artistas Registrados</FONT></u>
                        </h3>

                    </div>
                    
                </div>
                <br>

  <div class="alert alert-success" role="alert">
                    
                <div class="col m6" align="left">

                    <a href="ArtistaServlet?action=nuevo"  class="btn btn-outline-secondary"><i class="fa fa-pencil-square-o" style="color:#757575; font-size: 20px" style="color:red" > </i>
                        Agregar un Nuevo Registro
                    </a>
                </div>
            </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover">

                    <thead bgcolor="#eceff1" >
                        <tr class="info">
                           
                            <th bgcolor="#eceff1" scope="col" class="text-BLACK">Nombre Artista</th>
                            <th scope="col" class="text-BLACK">Tipo</th>
                            <th scope="col" class="text-BLACK">Nombre Cd</th>
                            <th scope="col" class="text-BLACK">Pais</th>
                            <th scope="col" ></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>


                    <tbody>

                        <c:forEach items="${artista}" var="a">
                            <tr>
                                
                                <td style="color: white !important" >${a.nombre_a}</td>
                                <td style="color: white !important" >${a.tipo}</td>
                                <td style="color: white !important" >${a.nombre_cd}</td>
                                <td style="color: white !important" >${a.nombre}</td>

                                <td>
                                    <a href="ArtistaServlet?action=eliminar&id=${a.codigo_artista}" class="btn btn-outline-danger"><i class="fa fa-remove" style="color:#757575; font-size: 20px" style="color:red" >

                                        </i> Eliminar</a>

                                </td>
                                <td >
                                    <a href="ArtistaServlet?action=editar&id=${a.codigo_artista}"  class="btn btn-outline-success"> <i class="fa  fa-spinner" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i> Actualizar</a>
                                </td>
                            </tr>

                        </c:forEach>
                        </div> 
                    </tbody>
                </table>

            </div>

        </div>


    </body>
</html>
