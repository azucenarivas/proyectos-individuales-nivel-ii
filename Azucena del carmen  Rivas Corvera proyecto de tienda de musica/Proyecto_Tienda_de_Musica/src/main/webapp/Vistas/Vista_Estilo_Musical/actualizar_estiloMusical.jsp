<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
             <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <jsp:include page="/menu_admin.jsp"/>

        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">

                        <center> <form class="form-horizontal" method="post" action="Estilo_MusicalServlet">
                                <c:forEach items="${estilo_musical}" var="estilo">


                                    <fieldset>

                                         <div class="col m12" align="center">
                                            <h3>
                                                <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                                    Actualización del Registro de  Estilo Musical</FONT></u>
                                            </h3>

                                        </div>
                                    <br>



                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-left">Ingrese el Tipo de Musica:</span>
                                            <div class="col-md-4">
                                                  <input type="hidden" name="txtID" value="${estilo.codigo_estilo_musical}"/>
                                                <input id="txtnombre" name="txttipo" type="text" placeholder="Ingrese el nombre del Pais" class="form-control" value="${estilo.tipo}">
                                            </div>
                                        </div>
                                        <br>
                                        <br>


                                        <br>

                                        <div class="form-group">

                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <input type="submit" name="btnsave" value="Guardar Datos"  class="btn btn-success btn-lg"/>

                                                </div>
                                            </div>
                                        </div>



                                    </fieldset>
                                </c:forEach>
                            </form>

                        </center>

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
