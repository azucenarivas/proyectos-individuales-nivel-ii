<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
             <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
       <jsp:include page="/menu_admin.jsp"/>

        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">

                        <center> <form class="form-horizontal" method="post" action="Grupo_ArtistaServlet">
                                <c:forEach items="${grupo_artista}" var="gru">
                                 

                                    <fieldset>
                             
                                          <div class="col m12" align="center">
                                            <h3>
                                                <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                                    Actualización del Registro de Artista Grupo</FONT></u>
                                            </h3>

                                        </div>
                                    <br>

                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Seleccione un Artista:</span>
                                            <div class="col-md-4">
                                                <input type="hidden" name="txtID" value="${gru.codigo_grupo_artista}"/>
                                                <select name="cmbartista" class="form-control">

                                                    <c:forEach items="${artista}" var="ar">
                                                        <c:choose>
                                                            <c:when test="${gru.codigo_artista==ar.codigo_artista}">
                                                                <option value="${ar.codigo_artista}" selected=""> ${ar.nombre_a}
                                                                </option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="${ar.codigo_artista}"> ${ar.nombre_a}
                                                                </option>

                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Seleccione Grupo:</span>
                                            <div class="col-md-4">

                                                <select name="cmbgrupo" class="form-control">
                                                    <c:forEach items="${grupo}" var="g">
                                                        <c:choose>
                                                            <c:when test="${gru.codigo_grupo==g.codigo_grupo}">
                                                                <option value="${g.codigo_grupo}" selected=""> ${g.nombre}
                                                                </option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="${g.codigo_grupo}"> ${g.nombre}
                                                                </option>

                                                            </c:otherwise>
                                                        </c:choose>


                                                    </c:forEach>

                                                </select>
                                            </div>
                                        </div>
                                        <br>       

                                        <div class="form-group">

                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <input type="submit" name="btnsave" value="Guardar Datos"  class="btn btn-success btn-lg"/>

                                                </div>
                                            </div>
                                        </div>



                                    </fieldset>
                                </c:forEach>
                            </form>

                        </center>

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
