<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>

    <body>
        <jsp:include page="/menu_admin.jsp"/>

        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">

                        <center> <form class="form-horizontal" method="post" action="Grupo_ArtistaServlet">

                                <fieldset>

                                    <div class="col-md-12">
                                        <div class="col m12" align="center">
                                            <h3>
                                                <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                                    Asignar Artista a un Grupo</FONT></u>
                                            </h3>

                                        </div>

                                    </div>
                                    <br>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione un Artista:</span>
                                        <div class="col-md-4">

                                            <select name="cmbartista" class="form-control">

                                                <c:forEach items="${artista}" var="a">

                                                    <option value="${a.codigo_artista}" selected=""> ${a.nombre_a}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>
                                    <br>




                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione Grupo:</span>
                                        <div class="col-md-4">

                                            <select name="cmbgrupo" class="form-control">

                                                <c:forEach items="${grupo}" var="g">

                                                    <option value="${g.codigo_grupo}" selected=""> ${g.nombre}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>
                                    <br>       

                                    <div class="form-group">

                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos"  class="btn btn-success btn-lg"/>

                                            </div>
                                        </div>
                                    </div>



                                </fieldset>
                            </form>

                        </center>

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>


