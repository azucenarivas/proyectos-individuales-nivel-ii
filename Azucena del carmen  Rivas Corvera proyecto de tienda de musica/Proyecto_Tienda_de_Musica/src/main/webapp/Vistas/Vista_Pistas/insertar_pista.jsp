<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>

    <body>
       <jsp:include page="/menu_admin.jsp"/>

        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                    
                        <center> <form class="form-horizontal" method="post" action="PistaServlet">

                                <fieldset>
                              
                                        <div class="col-md-12">
                                        <div class="col m12" align="center">
                                            <h3>
                                                <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                                    Registro de Pistas</FONT></u>
                                            </h3>

                                        </div>

                                    </div>
                                    <br>




                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Numero de Canción:</span>
                                        <div class="col-md-4">
                                            <input id="txtnumero" name="txtnumero" type="text" placeholder="Ingrese el numero de la cancion" class="form-control">
                                        </div>
                                    </div>
                                   
                                    <br>
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Nombre de la Canción:</span>
                                        <div class="col-md-4">
                                            <input id="txttitulo" name="txttitulo" type="text" placeholder="Ingrese el nombre de la cancion" class="form-control">
                                        </div>
                                    </div>
                                  
                                    <br>
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Duración de la Cancion (minutos):</span>
                                        <div class="col-md-4">
                                            <input id="txtduracion" name="txtduracion" type="text" placeholder="Ingrese el tiempo que dura la cancion" class="form-control">
                                        </div>
                                    </div>
                                  
                                    <br>
                                    
                                     <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione el CD:</span>
                                        <div class="col-md-4">

                                            <select name="cmbcd" class="form-control">
                                               
                                                <c:forEach items="${cd}" var="c">

                                                    <option value="${c.codigo_cd}" selected=""> ${c.nombre_cd}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    
                                    
                                   

                                    <br>

                                    <div class="form-group">
                                     
                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos"  class="btn btn-success btn-lg"/>

                                            </div>
                                        </div>
                                    </div>



                                </fieldset>
                            </form>
                            
                        </center>
                        
                    </div>
                </div>
            </div>
        </div>
  
    </body>
</html>


