<%@page session = "true" %>
<%
    HttpSession actual = request.getSession();

    actual.setAttribute("ingreso", null);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inicio de Session</title>
        <script src="Resources/js/main.js"></script>
    </head>
    <link href="Resources/css/materialize.css" rel="stylesheet" type="text/css"/>
    <link href="Resources/css/styles.css" rel="stylesheet"/>
    <link href="recursos/css/materialize.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merienda&display=swap" rel="stylesheet">
    <link href="recursos/css/fondo.css" rel="stylesheet" type="text/css"/>

    <body>

        <div class="container">


            <div class="col z-depth-3 card-panel center-block blue-grey-text">
                <div class="container"> 


                    <form method="post" action="loginServlet">

                        <div class="row" align="center">

                            <u><FONT FACE="impact" SIZE=7 COLOR="#0d47a1">
                                Inicio de Sesión</FONT></u>
                        </div>

                        <br>
                        <br>

                        <div class="row">
                            <FONT SIZE=4 COLOR="#1976d2">Nombre de Usuario:</FONT>
                            <input id="usu" type="text" name="txtUsuario" class="validate" required="" value="" placeholder="Ingresa un nombre para tu usuario" focus>

                        </div>


                        <div class="row">

                            <FONT SIZE=4  COLOR="#1976d2">Contraseña:</FONT>
                            <input id="pass" type="password" name="txtContra" class="validate" placeholder="Ingresa una Contraseña" required="">

                        </div>



                        <div class="row">
                            <div class="col m3"></div>
                            <div class="col m2"></div>
                            <div class="col m2">
                                <input type="submit" value="Ingresar" class="btn blue darken-1 white-text z-depth-5">
                            </div>
                        </div>
                    </form>
                    <br></br>
                    <div class="row" align="center" >
                        <FONT FACE="small fonts" SIZE=4 COLOR="black">
                        No tienes una Cuenta?.... <u><a href="PersonasServlet?action=inicio"> Clip para Registrarse </a></u></FONT> 

                    </div>


                </div>
            </div>

    </body>
</body>
</html>
