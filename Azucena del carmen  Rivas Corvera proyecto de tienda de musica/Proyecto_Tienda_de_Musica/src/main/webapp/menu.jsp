<%-- 
    Document   : menu
    Created on : 11-05-2019, 11:30:18 AM
    Author     : azucena.rivasusam
--%>
<%@page session = "true" %>

<%
    HttpSession actual = request.getSession();

    String usuario;

    if (actual.getAttribute("ingreso") != null) {
        usuario = session.getAttribute("ingreso").toString();
    } else {
        response.sendRedirect("inicio_sesion.jsp");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>


    </head>

    <body>
        <jsp:include page="menu_admin.jsp"/>


        <div class="row">
            <div class="col m12">


                <div class="card black white-text center-align z-depth-5">

                    <h3>
                        <center><u><FONT FACE="impact" SIZE=7 COLOR=#0086ad>

                                BIENVENIDOS /AS A LA UNICA TIENDA DE MUSICA  </FONT></u>
                    </h3>
                </div>
            </div>

        </div>




        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">



            <img src="https://psicologiaymente.com/media/xW/va/mj/xWvamjzDqG/estudiar-escuchando-musica/estudiar-escuchando-musica-wide.jpg" width="200" height="600" class="d-block w-100" alt=>
        </div>
        <div class="carousel-item">
            <img src="" width="300" height="600" class="d-block w-100" alt="...">
        </div>





        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</body>
</html>
