<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session = "true" %>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    <style>
        #fondo{
            position: fixed;
            z-index: -100;
            top: 0%;
            left: 0%;
            width: 100%;
            height:  100%;
            opacity: 0.9.1;
            filter: grayscale(0);
        }

        body{

            background-image: url(https://png.pngtree.com/thumb_back/fw800/back_our/20190621/ourmid/pngtree-carnival-music-festival-background-material-image_205558.jpg);
            background-repeat: no-repeat;
            background-position: center;
        }
        tbody {
            background-color: black !important;
            opacity: 80%;
        }
        thead{
            background-color: #007bff !important;
        }
    </style>

    <body>


        <%
            HttpSession actual = request.getSession();

            String usuario;

            if (actual.getAttribute("ingreso") != null) {
                usuario = session.getAttribute("ingreso").toString();
            } else {
                response.sendRedirect("inicio_sesion.jsp");
            }
        %>
        <img src="https://png.pngtree.com/thumb_back/fw800/back_our/20190621/ourmid/pngtree-carnival-music-festival-background-material-image_205558.jpg.jpg" id="fondo"/>

        <nav class="navbar navbar-expand-lg"  style="background-color:#1b5e20;" role="navigation">


            <center> <a class="navbar-brand text-light" href="menu.jsp"  >TIENDA DE MUSICA </a></center>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Grupo Musical
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="GrupoServlet?action=grupo">Agregar un Grupo</a>

                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            CD
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="CdServlet?action=cd">Cd</a>
                            <a class="dropdown-item" href="PistaServlet?action=pistas">Canciones</a>
                            <a class="dropdown-item" href="Estilo_MusicalServlet?action=estilo_musical">Estilo Musical</a>
                            <a class="dropdown-item" href="ColeccionServlet?action=coleccion">Colección</a>


                        </div>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Artistas
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="ArtistaServlet?action=artista">Artista Individual</a>
                            <a class="dropdown-item" href="Grupo_ArtistaServlet?action=grupo_artista">Grupo de Artsitas</a>
                        </div>
                    </li>



                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Compañias
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="CompaniaServlet?action=compania">Compañia</a>
                            <a class="dropdown-item" href="EdicionesServlet?action=edicion"> Ediciones</a>
                        </div>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Promotores
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="PromotorServlet?action=promotor">Registro de Promotor</a>
                        </div>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Pais
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="PaisServlet?action=pais">Registro de Pais</a>

                        </div>
                    </li>



                </ul>


                <form method="link" action="salir.jsp">
                    <div class="col m-12 right-align white-text">
                        <input type="submit" value="Cerrar Sessión" style="color: white" class="btn pink text-accent-1 white-text z-depth-5"/>
                    </div>
                </form>
            </div>
        </nav>

    </body>


</html>
