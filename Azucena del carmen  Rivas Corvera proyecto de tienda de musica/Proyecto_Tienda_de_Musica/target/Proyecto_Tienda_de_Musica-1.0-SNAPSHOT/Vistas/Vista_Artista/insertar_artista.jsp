<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    </head>

    <body>
        <jsp:include page="/menu_admin.jsp"/>

        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">

                        <center> <form class="form-horizontal" method="post" action="ArtistaServlet">

                                <fieldset>

                                    <div class="col-md-12">
                                        <div class="col m12" align="center">
                                            <h3>
                                                <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                                    Registro de Artistas</FONT></u>
                                            </h3>

                                        </div>

                                    </div>
                                    <br>



                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Ingrese el nombre:</span>
                                        <div class="col-md-4">
                                            <input id="txtnombre_a" name="txtnombre_a" type="text" placeholder="Ingrese el nombre del Artista" class="form-control">
                                        </div>
                                    </div>
                                    <br>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione el Tipo de Musica:</span>
                                        <div class="col-md-4">

                                            <select name="cmbtipo" class="form-control">

                                                <c:forEach items="${estilo}" var="e">

                                                    <option value="${e.codigo_estilo_musical}" selected=""> ${e.tipo}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>
                                    <br>       
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione un Cd:</span>
                                        <div class="col-md-4">

                                            <select name="cmbcd" class="form-control">

                                                <c:forEach items="${cd}" var="c">

                                                    <option value="${c.codigo_cd}" selected=""> ${c.nombre_cd}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione el Tipo de Musica:</span>
                                        <div class="col-md-4">

                                            <select name="cmbpais" class="form-control">

                                                <c:forEach items="${pais}" var="p">

                                                    <option value="${p.codigo_pais}" selected=""> ${p.nombre}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>



                                    <br>
                                    <br>
                                    <div class="form-group">

                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos"   class="btn btn-success btn-lg"/>

                                            </div>
                                        </div>
                                    </div>



                                </fieldset>
                            </form>

                        </center>

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>


