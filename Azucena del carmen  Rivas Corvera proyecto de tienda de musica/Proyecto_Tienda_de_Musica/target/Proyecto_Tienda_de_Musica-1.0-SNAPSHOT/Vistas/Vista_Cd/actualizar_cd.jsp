<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
             <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
   <jsp:include page="/menu_admin.jsp"/>

    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">

                    <center> <form class="form-horizontal" method="post" action="CdServlet">
                            <c:forEach items="${cd}" var="cd">
                                <fieldset>
                                     <div class="col-md-12">
                                        <div class="col m12" align="center">
                                            <h3>
                                                <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                                    Actualización del Registro de Grupo</FONT></u>
                                            </h3>

                                        </div>

                                    </div>
                                    <br>
                                    
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Ingrese Nombre:</span>
                                        <div class="col-md-4">
                                            <input type="hidden" name="txtID" value="${cd.codigo_cd}"/>
                                            <input id="txtnombre_cd" name="txtnombre_cd" type="text" placeholder="Ingrese el nombre del CD" class="form-control" value="${cd.nombre_cd}">
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Ingrese una Clave:</span>
                                        <div class="col-md-4">
                                            <input id="txtclave_cd" name="txtclave_cd" type="text" placeholder="Ingrese la clave del CD" class="form-control" value="${cd.clave}">
                                        </div>
                                    </div>

                                    <br>
                                    <br>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Ingrese el Precio:</span>
                                        <div class="col-md-4">
                                            <input id="txtprecio_cd" name="txtprecio_cd" type="text" placeholder="Ingrese el precio de Venta del CD" class="form-control" value="${cd.precio_v}">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">

                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos"  class="btn btn-success btn-lg"/>

                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                            </c:forEach>
                        </form>

                    </center>

                </div>
            </div>
        </div>
    </div>


</body>
</html>
