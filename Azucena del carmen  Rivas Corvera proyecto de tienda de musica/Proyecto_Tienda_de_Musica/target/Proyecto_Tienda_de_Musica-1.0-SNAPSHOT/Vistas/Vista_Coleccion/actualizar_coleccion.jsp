<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>

    <body>
       <jsp:include page="/menu_admin.jsp"/>

        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">

                        <center> <form class="form-horizontal" method="post" action="ColeccionServlet">
                                <c:forEach items="${coleccion}" var="cole">



                                    <fieldset>

                                        <div class="col-md-12">
                                            <div class="col m12" align="center">
                                                <h3>
                                                    <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                                        Actualización del Registro de Colección</FONT></u>
                                                </h3>

                                            </div>

                                        </div>
                                        <br>

                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Seleccione un CD:</span>
                                            <div class="col-md-4">
                                                <input type="hidden" name="txtID" value="${cole.codigo_coleccion}"/>
                                                <select name="cmbcd" class="form-control">

                                                    <c:forEach items="${cd}" var="c">
                                                        <c:choose>
                                                            <c:when test="${cole.codigo_cd==c.codigo_cd}">
                                                                <option value="${c.codigo_cd}" selected=""> ${c.nombre_cd}
                                                                </option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="${c.codigo_cd}"> ${c.nombre_cd}
                                                                </option>
                                                            </c:otherwise>
                                                        </c:choose>



                                                    </c:forEach>

                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-left">Precio de Venta:</span>
                                            <div class="col-md-4">
                                                <input id="txtprecio" name="txtprecio" type="number" placeholder="Ingrese el Precio de Venta" class="form-control" value="${cole.precio_venta}">
                                            </div>
                                        </div>

                                        <br>       

                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Seleccione un Promotor:</span>
                                            <div class="col-md-4">

                                                <select name="cmbpromotor" class="form-control">

                                                    <c:forEach items="${promotor}" var="p">
                                                        <c:choose>
                                                            <c:when test="${cole.codigo_promotor==p.codigo_promotor}">
                                                                <option value="${p.codigo_promotor}" selected=""> ${p.nombre_p}
                                                        </option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="${p.codigo_promotor}"> ${p.nombre_p}
                                                        </option>
                                                            </c:otherwise>
                                                        </c:choose>

                                                        

                                                    </c:forEach>

                                                </select>
                                            </div>
                                        </div>



                                        <br>
                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-2 text-center">Seleccione la Compañia:</span>
                                            <div class="col-md-4">

                                                <select name="cmbcompania" class="form-control">

                                                    <c:forEach items="${compania}" var="co">
                                                        <c:choose>
                                                            <c:when test="${cole.codigo_compañia== co.codigo_compañia}">
                                                                 <option value="${co.codigo_compañia}" selected=""> ${co.nombre_c}
                                                        </option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                 <option value="${co.codigo_compañia}"> ${co.nombre_c}
                                                        </option>
                                                            </c:otherwise>
                                                        </c:choose>

                                                       

                                                    </c:forEach>

                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="form-group">

                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <input type="submit" name="btnsave" value="Guardar Datos"  class="btn btn-success btn-lg"/>

                                                </div>
                                            </div>
                                        </div>



                                    </fieldset>
                                </c:forEach>
                            </form>

                        </center>

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>


