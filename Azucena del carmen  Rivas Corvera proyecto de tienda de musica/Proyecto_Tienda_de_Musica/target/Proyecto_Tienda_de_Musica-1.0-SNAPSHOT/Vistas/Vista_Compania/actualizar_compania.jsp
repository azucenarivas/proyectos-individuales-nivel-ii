<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
             <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <jsp:include page="/menu_admin.jsp"/>

        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">

                        <center> <form class="form-horizontal" method="post" action="CompaniaServlet">
                                <c:forEach items="${compania}" var="compania">
                                    
                               
                                <fieldset>
                                    <div class="col-md-12">
                                        <div class="col m12" align="center">
                                            <h3>
                                                <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                                    Actualización del Registro de Compañias</FONT></u>
                                            </h3>

                                        </div>

                                    </div>
                                    <br>



                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Nombre:</span>
                                        <div class="col-md-4">
                                            <input type="hidden" name="txtID" value="${compania.codigo_compañia}"/>
                                            <input id="txtnombre_c" name="txtnombre_c" type="text" placeholder="Ingrese el nombre de la compañia" class="form-control" value="${compania.nombre_c}">
                                        </div>
                                    </div>

                                    <br>
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Dirección:</span>
                                        <div class="col-md-4">
                                            <input id="txtdireccion" name="txtdireccion" type="text" placeholder="Ingrese la dirección de la compañia" class="form-control" value="${compania.direccion_c}">
                                        </div>
                                    </div>


                                    <br>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione el Pais:</span>
                                        <div class="col-md-4">

                                            <select name="cmbpais" class="form-control">

                                                <c:forEach items="${pais}" var="p">
                                                    <c:choose>
                                                        <c:when test="${compania.codigo_pais==p.codigo_pais}">
                                                             <option value="${p.codigo_pais}" selected=""> ${p.nombre}
                                                    </option>
                                                        </c:when>
                                                        <c:otherwise>
                                                             <option value="${p.codigo_pais}"> ${p.nombre}
                                                    </option>
                                                        </c:otherwise>
                                                    </c:choose>

                                                   

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>




                                    <br>

                                    <div class="form-group">

                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos"  class="btn btn-success btn-lg"/>

                                            </div>
                                        </div>
                                    </div>



                                </fieldset>
                                     </c:forEach>
                            </form>

                        </center>

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
