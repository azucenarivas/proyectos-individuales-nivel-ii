<%-- 
    Document   : mostrar_promotor
    Created on : 01-dic-2019, 19:12:11
    Author     : Azucena
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="/menu_encabezado.jsp"/>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col m12" align="center">
                        <h3>
                            <u  class="text-default"><p>Listado de Compañias </p> </u>
                        </h3>

                    </div>
                </div>
                <br>


                <div class="col m6" align="left">

                    <a href="CompañiaServlet?action=nuevo" class="btn btn-info"><i class="fa fa-pencil-square-o" style="color:#757575; font-size: 20px" style="color:red" > </i>
                        Agregar un Nuevo Registro
                    </a>
                </div>
            </div>


            <div class="table-responsive">
                <table class="table table-hover ">

                    <thead bgcolor="#eceff1">
                        <tr class="info">
                            <th bgcolor="#fafafa" scope="col">Codigo de la Compañia</th>
                            <th scope="col" class="text-BLACK">Compañia</th>
                            <th scope="col" class="text-BLACK">Dirección</th>
                            <th scope="col" class="text-BLACK">Pais</th>
                            <th scope="col" ></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>


                    <tbody>

                        <c:forEach items="${compania}" var="c">
                            <tr>
                                <td>${c.codigo_compañia}</td>
                                <td>${c.nombre_c}</td>
                                <td>${c.direccion_c}</td>
                                <td>${c.nombre}</td>

                                <td>
                                    <a href="CompañiaServlet?action=eliminar&id=${c.codigo_compañia}" class="btn btn-danger"><i class="fa fa-remove" style="color:#757575; font-size: 20px" style="color:red" >

                                        </i> Eliminar</a>

                                </td>
                                <td>
                                    <a href="CompañiaServlet?action=editar&id=${c.codigo_compañia}" class="btn btn-info"> <i class="fa  fa-spinner" style="color:#757575; font-size: 20px" style="color:red" >
                                        </i> Actualizar</a>
                                </td>
                            </tr>

                        </c:forEach>
                        </body>
                        </html>
