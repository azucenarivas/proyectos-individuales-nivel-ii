<%-- 
    Document   : vista_insertar
    Created on : 11-13-2019, 02:30:36 PM
    Author     : azucena.rivasusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>

    <body>
        <jsp:include page="/menu_admin.jsp"/>

        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">

                        <center> <form class="form-horizontal" method="post" action="EdicionesServlet">

                                <fieldset>
                                 

                                    <div class="col-md-12">
                                        <div class="col m12" align="center">
                                            <h3>
                                                <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                                    Registro de Ediciones</FONT></u>
                                            </h3>

                                        </div>

                                    </div>
                                    <br>



                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Fecha Entrada:</span>
                                        <div class="col-md-4">
                                            <input id="txtfecha" name="txtfecha" type="Date" placeholder="Ingrese el nombre de la compañia" class="form-control">
                                        </div>
                                    </div>

                                    <br>
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-left">Detalle</span>
                                        <div class="col-md-4">
                                            <input id="txtdetalle" name="txtdetalle" type="text" placeholder="Ingrese el detalle de la compañia" class="form-control">
                                        </div>
                                    </div>

                                    <br>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center">Seleccione la Compañia:</span>
                                        <div class="col-md-4">

                                            <select name="cmbcompania" class="form-control">

                                                <c:forEach items="${compania}" var="co">

                                                    <option value="${co.codigo_compañia}" selected=""> ${co.nombre_c}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </div>
                                    </div>
                                    <br>



                                    <div class="form-group">

                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" name="btnsave" value="Guardar Datos"  class="btn btn-success btn-lg"/>

                                            </div>
                                        </div>
                                    </div>



                                </fieldset>
                            </form>

                        </center>

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>


