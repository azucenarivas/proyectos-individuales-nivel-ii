<%-- 
    Document   : registrar_usuarios
    Created on : 03-oct-2019, 17:53:32
    Author     : Azucena
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrarme</title>
    </head>
    <link href="Resources/css/materialize.css" rel="stylesheet" type="text/css"/>
    <style>
        body{
            background: url(Resources/img/adv.jpg) repeat 0 0;
        }
        .row {
            margin-bottom: 0px
        }
        p {
            color: #000000;
            font-size: 20px;
            font-weight: bold;
            text-align: center;

        }



        img{
            display:block;
            margin:auto; 
            ;
        }

    </style>

    <body>
        <div class="container  z-depth-5">

            <form method="post" action="PersonasServlet" >

                <div class="row">
                    <div class="col m12">
                        <div class="blue-grey-text center-align z-depth-5">
                            <h3>Registro de Usuarios</h3> <br>

                        </div>
                       
                    </div>

                </div>
                <br>

                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3"><p>Ingrese su Nombres:</p></div>
                    <div class="col m6">  
                        <input type="text" name="nombre" required="" /> <br> 
                    </div>
                </div>

                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3"><p>Ingrese su Apellido:</p></div>
                    <div class="col m6">
                        <input type="text" name="apellido" required=""/> <br>

                    </div>
                </div>

                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3"><p>Ingrese su Direccion:</p></div>
                    <div class="col m6">
                        <input type="text" name="direccion" required=""/> <br>

                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3"><p>Ingrese su Usuario:</p></div>
                    <div class="col m6">
                        <input type="text" name="usuario" required=""/> <br>

                    </div>
                </div>
                <br>
                 <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3"><p>Ingrese su Contraseña:</p></div>
                    <div class="col m6">
                        <input type="password" name="contrasenia" required=""/> <br>

                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m2"></div>
                    <div class="col m2">
                        <input type="submit" name="btnSave" value="Registrarme" class="btn blue darken-1 white-text z-depth-5">
                    </div>
                </div>
                <br>
                <br>

            </form>


        </div>




    </body>
</html> 
