<%-- 
    Document   : mostrar_promotor
    Created on : 01-dic-2019, 19:12:11
    Author     : Azucena
--%>

<%@page session = "true" %>

<%
    HttpSession actual = request.getSession();

    String usuario;

    if (actual.getAttribute("ingreso") != null) {
        usuario = session.getAttribute("ingreso").toString();
    } else {
        response.sendRedirect("inicio_sesion.jsp");
    }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="Resources/iconos/iconos/iconos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <jsp:include page="/menu_usuario.jsp"/>
        <br>
        <div class="container">
            <div class="row">


                <div class="col-md-12">
                    <div class="col m12" align="center">
                        <h3>
                            <u><FONT FACE="impact" SIZE=6 COLOR="#707869">
                                Listado de Canciones con su CD </FONT></u>
                        </h3>

                    </div>

                </div>
                <br>

            </div>

  <br>
            <div class="table-responsive">
                <table class="table table-hover ">

                    <thead bgcolor="#eceff1">
                        <tr class="info">

                            <th  bgcolor="#fafafa" scope="col" class="text-BLACK">Numero de canción</th>
                            <th scope="col" class="text-BLACK">Nombre de La Canción</th>
                            <th scope="col" class="text-BLACK">Tiempo de Duración</th>
                            <th scope="col" class="text-BLACK">Pertenede al CD</th>
                            <th scope="col" ></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>


                    <tbody>

                        <c:forEach items="${pista}" var="p">
                            <tr>

                                <td style="color: black !important">${p.numero_pista}</td>
                                <td style="color: black !important">${p.titulo}</td>
                                <td style="color: black !important">${p.duracion}</td>
                                <td style="color: black !important">${p.nombre_cd}</td>

                            </tr>

                        </c:forEach>
                        </div> 
                    </tbody>
                </table>

            </div>

        </div>


    </body>
</html>