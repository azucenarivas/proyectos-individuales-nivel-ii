<%-- 
    Document   : vista_registro_usuario
    Created on : 27-sep-2019, 15:03:17
    Author     : Azucena
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <link href="Resources/css/materialize.css" rel="stylesheet" type="text/css"/>
    <body>
        <div class="container">
            <div class="row">
                <div class="col m12">
                    <div class="card black-text  center-align z-depth-3">
                        <h3>Registro de  Usuarios</h3>

                    </div>
                </div> 
                <a href="control_regisUsua?action=nuevo" class="btn orange black-text z-depth-5">Agregar Nuevo Usuario</a>
            </div>
            <div class="col m12">
                <table class="responsive-table">
                    <tr class="card black-text z-depth-3">
                        <th>Codigo</th>
                        <th>Nombre Completo</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>fecha_nacimiento</th>
                        <th>Nacionalidad </th>
                        <th>Nombre_usuario</th>
                        <th>Contraseña</th>

                    </tr>
                    <c:forEach items="${llenausur}" var="u">
                        <tr>
                            <td>${u.id_registro_usuario}</td>
                            <td>${u.nombre} ${u.apellido}</td>
                            <td>${u.direccion}</td>
                            <td>${u.telefono}</td>
                            <td>${u.fecha_nacimiento}</td>
                            <td>${u.nacionalidad}</td>
                            <td>${u.nombre_usuario}</td>
                            <td>${u.contrasenia}</td>
                        </tr>

                    </c:forEach>

                </table>

            </div>

        </div>
    </body>
</html>

