

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    HttpSession actual = request.getSession(false);
    if (actual != null) {
        actual.invalidate();
    }
    response.sendRedirect("index.jsp");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cerrar Sessión</title>
    </head>
    <body>
    </body>
</html>
